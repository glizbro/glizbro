import validator from "validator";

const formSchema = {
	//step 1
	firstName: [isEmpty, isLength({min:2, max:20}), isAlpha ],
	lastName: [isEmpty, isLength({min:2, max:20}), isAlpha],
	age: [isEmpty],
	email: [isEmpty, isLength({min:3, max:30}) ],
	password: [isEmpty, isLength({min:3, max:20}), passReg],
	// repeatPassword: [isEmpty, ],
	//step 2
	country: [isEmpty,],
	city: [isEmpty, isAlpha, isLength({min:2, max:30})],
	gender: [isEmpty],
	status: [isEmpty],
	religion: [isEmpty],
	// step 3
	tall: [isEmpty],
	weight: [isEmpty],
	smoking: [isEmpty],
	alcohol: [isEmpty],
	language: [isEmpty],
	// step 4
	pets: [isEmpty],
	character: [isEmpty],
	education: [isEmpty],
	relationshipPurpose: [isEmpty],
   transport: [isEmpty],
   interests: [isEmpty],
};

const validateForm = (field, value) => {
	if (!formSchema[field]) {
		// field not match signup form schema
		console.error(`${field}  not match signup form schema`);
		return;
	}
	let result;
	for (var test of formSchema[field]) {
		result = test(field, value);
	}
	return result;
};

export default validateForm;

// value contaion only english characters   
function isAlpha( field, value ) {
   const result = validator.isAlpha( value );
   if( result ) return false;
   return `${field} must contaion only english characters`;
}

// validate length
function isLength({ min, max }) {
   let error = false;
	return (field, value) => {
      const length = value.toString().length;

		if (min && (length < min)) 
			error = `${field} length must min ${min} characters`;
		else if (max && length > max)
         error = `${field} length must max ${max} characters`;
      else
         error = false;

		return error;
	};
}

function passReg( field, value ) {
   const res = validator.matches(value, /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/);
   if(res) return false;
   return 'pasword pattern wrong'
}

function isEmpty(field, value) {
	// for array
	if (value && value.length && value.length > 0) return false;
	// for string
	if (!value) return false;
	// for number
   if (typeof value === "number") return false;
   // for boolean
   if(typeof variable === "boolean") return false;

	return `${field} is required`;
}

// function isNotEmail(field, value) {
// 	if (validator.isEmail(value)) return false;

// 	return `${field} not valid email`;
// }
