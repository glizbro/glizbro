import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

class WithUser extends React.Component {
	render() {
		const { user } = this.props;
		const Children = React.cloneElement(this.props.children);
		const shouldUserExist = this.props.shouldUserExist;
		const userExists = Object.keys(user.data).length > 0;
		if (shouldUserExist) {
			if (userExists) return Children;
			return <Redirect to={this.props.urlToRedirect} />;
		} else {
			// user not shouldUserExist
			if (!userExists) return Children;
			return <Redirect to={this.props.urlToRedirect} />;
		}
	}
}

const mapStateToProps = state => {
	return {
		user: state.user,
	};
};

export default connect(
	mapStateToProps,
	null,
)(WithUser);
