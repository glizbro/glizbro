//time for chat
const getCurrentTime = () => {
	var date = new Date;


	var seconds = date.getSeconds();
	var minutes = date.getMinutes();
	var hour = date.getHours();
	var ampm = hour >= 12 ? 'PM' : 'AM';
	return hour + ':' + minutes + ' ' + ampm;
}


export default getCurrentTime;