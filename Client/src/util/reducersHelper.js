// insert item to array by index
export function insertItem(array, action) {
	return [...array.slice(0, action.index), action.item, ...array.slice(action.index)];
}

//  remove item from array by index
export function removeItem(array, action) {
	return [...array.slice(0, action.index), ...array.slice(action.index + 1)];
}
