
module.exports = {
   age: 'date',
   weight: 'number',
   tall: 'number',
   country: 'string',
   gender: 'string',
   status: 'string',
   transport: 'string',
   religion: 'string',
   relationshipPurpose: 'string',
   education: 'string',
   language: 'string',
   interests: 'array',
   character: 'array',
   pets: 'array',
   alcohol: 'boolean',
   smoking: 'boolean',
}