import {
	SEND_MESSAGE,
	RECEIVE_MESSAGE,
	// GET_ALL_MESSAGES,
	GET_ALL_MESSAGES_SUCCESS,
} from './actions';

const initState = {
	messages: [],
};

export default (state = initState, action) => {
	const { messages } = state;
	const { type, payload } = action;

	switch (type) {
		case SEND_MESSAGE:
			return { ...state, messages: [...messages, payload] };
		case RECEIVE_MESSAGE:
			return { ...state, messages: [...messages, payload] };
		case GET_ALL_MESSAGES_SUCCESS:
			return { ...state, messages: action.messages };

		default:
			return state;
	}
};
