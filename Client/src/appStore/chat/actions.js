
//imports
// sockets
import { ioEvents, ioSendEvent } from '../../sockets';
//utils
import getCurrentTime from '../../util/time';

//api
import api from '../../api';

const fetchSuccess = (res) => ({
	type: GET_ALL_MESSAGES_SUCCESS,
	messages: res,
});

const fetchFailed = error => ({
	type: GET_ALL_MESSAGES_FAILED,
	error: error,
});


// action creators
export const sendMessage = content => (dispatch, getState) => {
	let payload = {
		me: true,
		content: content,
		time: getCurrentTime()
	}

	ioSendEvent(ioEvents.SEND_MESSAGE, payload);
	dispatch({
		type: SEND_MESSAGE,
		payload: payload,
	});

};

export const receiveMessage = payload => {

	return {
		type: RECEIVE_MESSAGE,
		payload: {
			me: false,
			content: payload.content,
			time: getCurrentTime()
		},
	};
};



export const getAllMessages = userId => (dispatch, getState) => {
	const { chat } = getState();
	ioSendEvent(ioEvents.GET_ALL_MESSAGES, userId);
	
	dispatch({
		type: GET_ALL_MESSAGES,
		userId: userId,
	});

	return api
			.chat()
			.then(res => {
				dispatch(fetchSuccess(res.data));
			})
			.catch(err => {
				dispatch(fetchFailed(err.message));
			});
};

export const clickSave = name => {
	return (dispatch, getState) => {
		const { form } = getState();
		return api
			.profile({
				method: 'post',
				url: '/save',
				data: { fields: { public: form.profileConfiguration }},
			})
			.then(res => {
				dispatch(fetchSuccess(res));
			})
			.catch(err => {
				dispatch(fetchFailed(err.message));
			});
	};
};

//exports

export const SEND_MESSAGE = 'SEND_MESSAGE';
export const RECEIVE_MESSAGE = 'RECEIVE_MESSAGE';
export const GET_ALL_MESSAGES = 'GET_ALL_MESSAGES';
export const GET_ALL_MESSAGES_SUCCESS = 'GET_ALL_MESSAGES_SUCCESS';
export const GET_ALL_MESSAGES_FAILED = 'GET_ALL_MESSAGES_FAILED';

