// async actions flow for fetch
// 1) dispatch fetch atcion
//  1.1) fetch action dispatch request_action for update UI until waiting for respnse
//  1.2) make async call
// 2) on success => dispatch success action
// 3) on error => dispatch error action

import api from '../../api';


// actions types
export const FETCH_MATCHES = 'FETCH_MATCHES';
export const FETCH_SUCCESS = 'RECEIVE_SUCCESS';
export const FETCH_ERROR = 'FETCH_ERROR';
export const REQUEST_MATCHES = 'REQUEST_MATCHES';

// actions creator
const requestMatches = filter => ({
	type: REQUEST_MATCHES,
	filter: filter,
});

const fetchSuccess = (items, request) => ({
	type: FETCH_SUCCESS,
	items: items,
	request: request,
});

const fetchError = error => ({
	type: FETCH_ERROR,
	error: error,
});

export const fetchMatches = () => {
	return (dispatch, getState) => {
		const {
			form: { searchProfiles },
			user,
		} = getState();
		const requestObj = requestMatches(searchProfiles);
		dispatch(requestObj);

		return api.matches
			.post('', { filter: searchProfiles, user })
			.then(res => {
				const items = res.data.matches;
				const request = searchProfiles;
				dispatch(fetchSuccess(items, request));
				// reset filter after search
			})
			.catch(err => {
				dispatch(fetchError(err.message));
			});
	};
};
