// actions types
import { FETCH_ERROR, FETCH_SUCCESS, REQUEST_MATCHES } from './actions';

// default state
const initState = {
	isFetching: false,
	error: null,
	items: [],
	lastSearches: {},
};

export default (state = initState, action) => {
	if (!state) {
		return initState;
	}
	switch (action.type) {
		case REQUEST_MATCHES:
			return { ...state, isFetching: true };
      case FETCH_SUCCESS:
			let newLastSearches = action.request.name
				? {
						...state.lastSearches,
						[action.request.name]: action.request,
				  }
				: state.lastSearches;
			return {
				...state,
				isFetching: false,
				items: [...action.items],
				lastSearches: newLastSearches,
			};
		case FETCH_ERROR:
			return { ...state, isFetching: false, error: action.error };
		default:
			return state;
	}
};
