// actions type
import {
	GOT_INVITE,
	SEND_INVITE,
	REJECT_INVITE,
	ACCEPT_INVITE,
	CANCEL_RELATIONSHIP,
	RELATIONSHIP_CANCELED,
	SET_RELATIONSHIP,
	INVITE_ACCEPTED,
} from './actions';

export const statuses = {
	single: 'single', //default status
	inRelationship: 'inRelationship', //user accept invite
	invitee: 'invitee', // user got invite
	inviter: 'inviter', // user send inivte
};

// init state
const initState = {
	status: statuses.single,
	partner: null,
};

// mode reducer
export default (state = initState, action) => {
	const { type } = action;
	switch (type) {
		case SET_RELATIONSHIP:
			return action.relationship;

		case CANCEL_RELATIONSHIP:
			return {...initState};

		case RELATIONSHIP_CANCELED:
			return {...initState};

		case INVITE_ACCEPTED:
			return { ...state, status: statuses.inRelationship };

		case ACCEPT_INVITE:
			return { ...state, status: statuses.inRelationship };

		case REJECT_INVITE:
			return initState;

		case SEND_INVITE:
			return { ...state, status: statuses.inviter, partner: action.partner };

		case GOT_INVITE:
			return { ...state, status: statuses.invitee, partner: action.partner };

		default:
			return state;
	}
};
