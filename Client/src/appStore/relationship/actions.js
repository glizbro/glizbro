// server socket
import { ioEvents, ioSendEvent } from "../../sockets";

// actions types
export const GOT_INVITE = "GOT_INVITE"; // user got invite
export const INVITE_ACCEPTED = "INVITE_ACCEPTED"; // partner accept
export const SEND_INVITE = "SEND_INVITE"; // user send invite
export const REJECT_INVITE = "REJECT_INVITE"; //user reject
export const ACCEPT_INVITE = "ACCEPT_INVITE"; //user accept
export const CANCEL_RELATIONSHIP = "CANCEL_RELATIONSHIP"; //user cancel
export const RELATIONSHIP_CANCELED = "RELATIONSHIP_CANCELED"; //partner cancel
export const SET_RELATIONSHIP = "SET_RELATIONSHIP"; // from db

// actions creators
export const relationshipCanceled = () => ({
   type: RELATIONSHIP_CANCELED
});

export const inviteAccepted = () => ({
   type: INVITE_ACCEPTED
});

export const setRelationshtip = relationship => ({
   type: SET_RELATIONSHIP,
   relationship
});

export const gotInvite = partner => ({
   type: GOT_INVITE,
   partner
});

export const sendInvite = partner => {
   // send socket event ❌
   ioSendEvent(ioEvents.INVITE, partner);

   // update redux ✔️
   return {
      type: SEND_INVITE,
      partner
   };
};

export const rejectInvite = () => {
   // send to server ✔️
   ioSendEvent(ioEvents.REJECT_INVITE);
   // remove invited from store ✔️
   return {
      type: REJECT_INVITE
   };
};

export const acceptInvite = () => {
   // send to server accept
   ioSendEvent(ioEvents.ACCEPT_INVITE);
   // update store relatshion to true
   // update invited to false
   return {
      type: ACCEPT_INVITE
   };
};

export const cancelRelationship = x => {
   // send to server id of user to cancel R'ship
   // update store
   ioSendEvent(ioEvents.CANCEL_RELATIONSHIP);
   return {
      type: CANCEL_RELATIONSHIP
   };
};
