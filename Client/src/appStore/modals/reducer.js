/**
 *  manage application modals /show/close ...
 */

import { OPEN_MODAL, CLOSE_MODAL } from './actions';

const initState = {
	items: [],
};

export default (state = initState, action) => {
	const { type } = action;
	switch (type) {
		case OPEN_MODAL:
			return { ...state, items: [...state.items, action.payload] };
		case CLOSE_MODAL:
			return {
				...state,
				items: [
					...state.items.slice(0, action.index),
					...state.items.slice(action.index + 1),
				],
			};
		default:
			return state;
	}
};
