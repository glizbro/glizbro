// actions types
export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

// actions creator

/**
 *
 * @param {modalType, modalProps} payload
 */
export const openModal = payload => ({
	type: OPEN_MODAL,
	payload,
});

export const closeModal = index => ({
	type: CLOSE_MODAL,
	index,
});
