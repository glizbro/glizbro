import api from '../../api';

// actions types public - used in in components
export const SIGNUP_FETCH = 'SIGNUP_FETCH';

// actions types private - only for internal used
export const SIGNUP_REQUEST = 'SIGNUP_REQUEST';
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_ERROR = 'SIGNUP_ERROR';

// action creators private
export const signupRequest = () => ({
	type: SIGNUP_REQUEST,
});
export const signupSuccess = res => ({
	type: SIGNUP_SUCCESS,
});
export const signupError = errMessage => ({
	type: SIGNUP_ERROR,
	error: errMessage,
});

// action creators public
export const signupFetch = () => (dispatch, getState) => {
	dispatch(signupRequest());
	const form = getState().form.signup || {};
	api.signup
		.post('/', {
			data: form,
		})
		.then(res => {
			dispatch(signupSuccess(res));
		})
		.catch(err => {
			dispatch(signupError(err.message));
		});
};
