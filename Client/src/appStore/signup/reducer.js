import { SIGNUP_SUCCESS, SIGNUP_ERROR, SIGNUP_REQUEST } from './actions';

// status type = of signup request
// pending = no request sending
// loading = request in proccess
// success = server return ok
// error = server return error
export const PENDING = 'pending';
export const LOADDING = 'loading';
export const SUCCESS = 'success';
export const ERROR = 'error';

const initialState = {
	status: PENDING,
};

export default (state = initialState, action) => {
	if (!state) return initialState;
	switch (action.type) {
		case SIGNUP_REQUEST:
			return { ...state, status: LOADDING };
		case SIGNUP_SUCCESS:
			return { ...state, status: SUCCESS };
		case SIGNUP_ERROR:
			return { ...state, status: ERROR };

		default:
			return state;
	}
};
