import { ON_CHANGE, ON_INIT } from './actions';

const initState = {
	signup: {
		firstName: '',
		lastName: '',
		email: '',
		password: '',
	},
	login: {
		email: '',
		password: '',
	},
	searchProfiles: {
		age: {
			min: '',
			max: '',
		},
		city: '',
	},
};

export default (state = initState, action) => {
	switch (action.type) {
		case ON_CHANGE:
			return {
				...state,
				[action.stateName]: {
					...state[action.stateName],
					[action.name]: action.value,
				},
			};
		case ON_INIT:
			return {
				...state,
				[action.stateName]: { ...action.value },
			};
		default:
			return state;
	}
};
