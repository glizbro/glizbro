//action types
export const ON_CHANGE = 'ON_CHANGE';
export const ON_INIT = 'ON_INIT';

//action creator
//action for input or select component, to update the
//specific state name and value
export const _onChange = (name, value, stateName) => {
	return {
		type: ON_CHANGE,
		name: name,
		value: value,
		stateName: stateName,
	};
};

export const _onInit = (value, stateName) => {
	return {
		type: ON_INIT,
		value: value,
		stateName: stateName,
	};
};
