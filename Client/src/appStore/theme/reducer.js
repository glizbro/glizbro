// new theme
const themes = {
	MAIN_THEME: {
		desktop: {
			palette: {
				primary: '#242424',
				secondary: 'palegoldenrod',
				third: 'darkorange',
				fourth: 'red',
				fifth: 'gray',
				inherit: 'inherit',
				white: 'white',
				hover: 'dodgerblue',
				iconColor: '#F34336',
			},
			typography: {
				fontSize: {
					sm: '14px',
					md: '16px',
					lg: '18px',
				},
			},
			spacing: {
				sm: '4px',
				md: '8px',
				lg: '16px',
			},
		},
		mobile: {
			palette: {
				primary: '#242424',
				secondary: 'coral',
			},
		},
		breakpoints: {
			xs: '0px',
			sm: '600px',
			md: '960px',
			lg: '1280px',
			xl: '1920px',
		},
	},
	SECONDARY_THEME: {
		desktop: {
			palette: {
				primary: '#242424',
				secondary: 'coral',
			},
		},
		mobile: {
			palette: {
				primary: '#242424',
				secondary: 'coral',
			},
		},
	},
};

// reducer
export default (state = themes.MAIN_THEME.desktop, action) => {
	return state;
};
