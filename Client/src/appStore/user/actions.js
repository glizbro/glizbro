// ajax
import Api from '../../api';
import axios from 'axios';

// external actions
import { setRelationshtip } from '../relationship/actions';

// actions types
export const SET_USER = 'SET_USER';
export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_START = 'FETCH_USER_START';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR';
export const TOOGLE_ONLINE = 'TOOGLE_ONLINE';
export const UPLOAD_IMG_REQUEST = 'UPLOAD_IMG_REQUEST';
export const SET_IMAGES = 'SET_IMAGES';
export const SET_PROFILE_IMAGE = 'SET_PROFILE_IMAGE';

// action creators
export const setProfileImage = image => ({
	type: SET_PROFILE_IMAGE,
	image,
});
export const setImages = images => ({
	type: SET_IMAGES,
	images: images,
});

export const uploadImgRequest = file => async (dispatch, getState) => {
	dispatch({
		type: UPLOAD_IMG_REQUEST,
	});
	try {
		const response = await Api.profile.post('/uploadImage');
		await axios.put(response.data.url, file, {
			headers: {
				'Content-Type': 'image/*',
			},
		});
	} catch (err) {
	}
};

export const toogleOnline = isOnline => ({
	type: TOOGLE_ONLINE,
	isOnline,
});

export const setUser = user => ({
	type: SET_USER,
	user,
});
export const fetchUserStart = () => ({
	type: FETCH_USER_START,
});

export const fetchUserError = errMessage => ({
	type: FETCH_USER_ERROR,
	error: errMessage,
});

export const fetchUser = () => {
	return dispatch => {
		dispatch(fetchUserStart());

		Api.profile
			.get('/current')
			.then(res => {
				const user = res.data || {};
				const { relationship } = user;
				delete user.relationship;
				dispatch(setUser(user));
				dispatch(setRelationshtip(relationship));
			})
			.catch(err => dispatch(fetchUserError(err.message)));
	};
};


