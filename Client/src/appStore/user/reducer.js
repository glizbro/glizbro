import {
	SET_USER,
	FETCH_USER_START,
	FETCH_USER_ERROR,
	TOOGLE_ONLINE,
	UPLOAD_IMG_REQUEST,
	SET_IMAGES,
	SET_PROFILE_IMAGE,
} from './actions';

const initState = {
	isLoading: false,
	isOnline: false,
	error: '',
	data: {},
};

/* TODO: split into smaller reducers */

export default (state = initState, action) => {
	switch (action.type) {
		case SET_PROFILE_IMAGE:
			return { ...state, data: { ...state.data, profileImg: action.image } };

		case SET_IMAGES:
			const newData = { ...state.data };
			newData.images = action.images;
			return { ...state, data: newData };

		case UPLOAD_IMG_REQUEST:
			return { ...state };

		case SET_USER:
			return { ...state, data: action.user, isLoading: false, isOnline: true };

		case FETCH_USER_START:
			return { ...state, isLoading: true };

		case FETCH_USER_ERROR:
			return { ...state, isLoading: false, error: action.error };

		case TOOGLE_ONLINE:
			return { ...state, isOnline: action.isOnline };

		default:
			return state;
	}
};
