import { SET_APP_CONFIGURATION } from './actions';

const appConfiguration = (state = {}, action) => {
	switch (action.type) {
		case SET_APP_CONFIGURATION:
			return { ...action.payload };
		default:
			return state;
	}
};

export default appConfiguration;
