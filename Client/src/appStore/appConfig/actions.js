import Api from '../../api';

// action types
export const SET_APP_CONFIGURATION = 'SET_APP_CONFIGURATION';

// action creators

export const setAppConfiguration = appConfiguration => ({
	type: SET_APP_CONFIGURATION,
	payload: appConfiguration,
});

export const fetchAppConfig = () => dispatch => {
	Api.appConfiguration
		.get('/')
		.then(res => dispatch(setAppConfiguration(res.data)))
		.catch(err => console.log('err on fetch config', err.message));
};
