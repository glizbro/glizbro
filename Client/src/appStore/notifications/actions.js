// action types
export const POP = 'POP'; // run before real pop for animation
export const POPPED = 'POPPED'; // pop operation finished
export const PUSH = 'PUSH'; // run before real push for animation
export const PUSHED = 'PUSHED'; // push operation finished

// action creators

// private actions
const popped = () => ({
	type: POPPED,
});

// public actions
export const pop = () => (dispatch, getState) => {
	dispatch({
		type: POP,
	});
	const { delay } = getState().notifications;
	setTimeout(() => {
		dispatch(popped());
	}, delay);
};

export const push = payload => (dispatch, getState) => {
	const { messages, maxCount } = getState().notifications;
	if (messages.length === maxCount) {
		dispatch(pop());
		setTimeout(() => {
			dispatch({
				type: PUSH,
				payload,
			});
			setTimeout(() => {
				dispatch({
					type: PUSHED,
				});
			}, 500);
		}, 500);
	} else {
		dispatch({
			type: PUSH,
			payload,
		});
		setTimeout(() => {
			dispatch({
				type: PUSHED,
			});
		}, 500);
	}
};
