/**
 * Handle application notification for user
 */

import { POP, POPPED, PUSH, PUSHED } from './actions';

const initState = {
	messages: [],
	stack: [], // if messages filled, next push go to here
	maxCount: 3, // max amount of messages in view stack
	doPop: false, // used for run animation before real pop
	pushed: false,
	delay: 500, // animation time (ms)
	aliveTime: 3000, // time for wait before auto pop message
	pushTimer: 3500, // how much wait before push if messages filled
};
export default (state = initState, action) => {
	const { messages, pushed } = state;
	const { type } = action;

	switch (type) {
		case PUSHED:
			return { ...state, pushed: false };
		case PUSH: {
			if (!pushed)
				return { ...state, pushed: true, messages: [...messages, action.payload] };
			return state;
		}
		case POP:
			return { ...state, doPop: true };
		case POPPED:
			return { ...state, doPop: false, messages: messages.slice(1) };
		default:
			return state;
	}
};
