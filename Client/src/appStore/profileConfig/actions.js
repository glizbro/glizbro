import api from '../../api';

// handle field actions
import { _onInit } from '../fields/actions';

//actions
import {setUser} from '../user/actions'

//action types
export const OPEN_EDIT = 'OPEN_EDIT';
export const EDIT_CHANGED = 'EDIT_CHANGED';
export const CLOSE_EDIT = 'CLOSE_EDIT';
export const CLICK_SAVE = 'CLICK_SAVE';
export const INIT_PROFILE_CONFIGURATION = 'INIT_PROFILE_CONFIGURATION';
export const SAVE_EDIT_SUCCESS = 'SAVE_EDIT_SUCCESS';
export const SAVE_EDIT_FAILED = 'SAVE_EDIT_FAILED';


//edit handle
//action creator
export const openEdit = (name, categories) => (dispatch, getState) => {
	const state = getState();
	let obj = {};
	for (const category of categories) {
		obj[category] = state.user.data[category];
	}
	dispatch({
		type: OPEN_EDIT,
		name: name,
	});
};

export const initProfileConfiguration = () => ({
	type: INIT_PROFILE_CONFIGURATION,
	name: 'profile ready to use.',
});

export const editChanged = name => ({
	type: EDIT_CHANGED,
	name: name,
});

export const closeEdit = name => ({
	type: CLOSE_EDIT,
	name: name,
});

const fetchSuccess = user => (dispatch, getState) => {
	let cleanObj = {};
	
	//set new user data to state
	dispatch(setUser(user.data));

	//edit success
	dispatch({
		type: SAVE_EDIT_SUCCESS
	});

	//clean profile configuration reducer
	dispatch(_onInit(cleanObj, 'profileConfiguration'));
};

const fetchFailed = error => ({
	type: SAVE_EDIT_FAILED,
	error: error,
});

//save handle
export const clickSave = name => {
	return (dispatch, getState) => {
		const { form } = getState();
		return api
			.profile({
				method: 'post',
				url: '/save',
				data: { fields: { public: form.profileConfiguration }},
			})
			.then(res => {
				dispatch(fetchSuccess(res));
			})
			.catch(err => {
				dispatch(fetchFailed(err.message));
			});
	};
};
