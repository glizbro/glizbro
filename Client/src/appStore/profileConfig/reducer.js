// actions types
import {
	OPEN_EDIT,
	EDIT_CHANGED,
	CLOSE_EDIT,
	CLICK_SAVE,
	INIT_PROFILE_CONFIGURATION,
	SAVE_EDIT_SUCCESS
} from './actions';

const initialState = {
	saved: false,
	editChanged: false,
	editComponent: '',
	isOpened: false
}


const profileConfiguration = (state = initialState, action) => {
	switch (action.type) {
		case EDIT_CHANGED: {
			return {
				...state,
				editChanged: true,
			};
		}
		case OPEN_EDIT:
			return {
				...state,
				saved: false,
				editChanged: false,
				isOpened: true,
				editComponent: action.name,
			};
		case CLOSE_EDIT:
			return {
				saved: false,
				editChanged: false,
				isOpened: false,
				editComponent: ''
			};
		case CLICK_SAVE:
			return {
				...state,
				editChanged: false,
				saved: true,
			};
		case INIT_PROFILE_CONFIGURATION:
			return {
				...state,
				saved: false,
				editChanged: false,
				editComponent: '',
			};
		case SAVE_EDIT_SUCCESS:
			return {
				...state
			};
		default:
			return state;
	}
};

export default profileConfiguration;
