// import serverSocket from '../../../sockets';

import Api from '../../api';
import { setUser } from '../user/actions';
import io from 'socket.io-client';

// actions types
export const FETCH_LOGIN_START = 'FETCH_LOGIN_START';
export const FETCH_LOGIN_SUCCESS = 'FETCH_LOGIN_SUCCESS';
export const FETCH_LOGIN_ERROR = 'FETCH_LOGIN_ERROR';

// actions creators
export const fetchLoginStart = () => ({
	type: FETCH_LOGIN_START,
});

export const fetchLoginSuccess = user => ({
	type: FETCH_LOGIN_SUCCESS,
	user,
});

export const fetchLoginError = () => ({
	type: FETCH_LOGIN_ERROR,
});

export const login = () => {
	return (dispatch, getState) => {
		dispatch(fetchLoginStart());
		const { email, password } = getState().form.login;
		Api.login
			.post('/', { email, password })
			.then(res => {
				dispatch(fetchLoginSuccess());
				dispatch(setUser(res.data));
				window.socket.disconnect();
				window.socket.open();
				// serverSocket.initServerSocket(res.data.token);
			})
			.catch(err => {
				dispatch(fetchLoginError(err.message));
			});
	};
};
