const initState = {};

export default (state = initState, action) => {
	if (!state) return initState;
   
   switch(action.type) {
      default: return state;
   }
};
