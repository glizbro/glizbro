import { combineReducers } from 'redux';

// reducers
import appConfiguration from './appConfig/reducer';
import profileConfiguration from './profileConfig/reducer';
import chat from './chat/reducer';
import matches from './matches/reducer';
import filter from './filter/reducer';
import relationship from './relationship/reducer';
import forms from './fields/reducer';
import user from './user/reducer';
import login from './login/reducer';
import signupReducer from './signup/reducer';
import notifications from './notifications/reducer';
import activities from './activity/reducer';
import modals from './modals/reducer';
import theme from './theme/reducer';

export default combineReducers({
	appConfiguration,
	user,
	chat,
	profileConfiguration,
	matches,
	matchesFilter: filter,
	relationship,
	form: forms,
	signup: signupReducer,
	login,
	notifications,
	activities,
	modals,
	theme,
});
