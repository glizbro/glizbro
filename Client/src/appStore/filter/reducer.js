/**
 * reducer for hanle filter for find matches api
 */

//  helpers - pure functions that have same pattern
import { removeItem } from '../../util/reducersHelper';

import {
	INIT_AVAILABLE_PROPS,
	SELECT_PROP,
	SET_FILTER,
	REMOVE_FILTER,
	SET_FILTER_NAME,
	RESET_FILTER,
} from './actions';

const initState = {
	selectedProps: [], // items that user select for filter
	availableProps: [], // avalible props to select for filter,
	filter: {},
	name: '',
};


const matchesFilter = (state = initState, action) => {
	switch (action.type) {
		case RESET_FILTER:
			return {
				...initState,
				availableProps: action.availableProps,
			};
		case SET_FILTER_NAME:
			return {
				...state,
				name: action.name,
			};

		case REMOVE_FILTER:
			const removedProp = state.selectedProps[action.index];
			let newFilter = { ...state.filter };
			delete newFilter[action.filter];
			return {
				...state,
				filter: newFilter,
				selectedProps: removeItem(state.selectedProps, action),
				availableProps: [...state.availableProps, removedProp],
			};
		case INIT_AVAILABLE_PROPS:
			return {
				...state,
				availableProps: action.availableProps,
			};
		case SELECT_PROP:
			const selectedProp = state.availableProps[action.index];
			return {
				...state,
				availableProps: removeItem(state.availableProps, action),
				selectedProps: [...state.selectedProps, selectedProp],
			};
		case SET_FILTER:
			return { ...state, filter: { ...state.filter, [action.key]: action.value } };
		default:
			return state;
	}
};

export default matchesFilter;
