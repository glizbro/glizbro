// actions types

// dispatch it for init array of all available props for select to filter
// should be used for init first time and refresh options when open filter
export const INIT_AVAILABLE_PROPS = 'INIT_AVAILABLE_PROPS';

// action dispached on user select prop from available props
// this prop removed from available props list and go to selectedProps list
export const SELECT_PROP = 'SELECT_PROP';

// set filter prop
export const SET_FILTER = 'SET_FILTER';

// remove filter
export const REMOVE_FILTER = 'REMOVE_FILTER';

export const SET_FILTER_NAME = 'SET_FILTER_NAME';

// reset filter
export const RESET_FILTER = 'RESET_FILTER';

// actions creator

export const resetFilter = () => (dispatch, getState) => {
	const state = getState();
	const availableProps = state.appConfiguration.homeConfig.filterOptions;
	dispatch({
		type: RESET_FILTER,
		availableProps: availableProps,
	});
};

export const setFilterName = name => ({
	type: SET_FILTER_NAME,
	name: name,
});

export const setFilter = (key, value) => {
	return {
		type: SET_FILTER,
		key,
		value,
	};
};

export const removeFilter = (filter, index) => ({
	type: REMOVE_FILTER,
	filter,
	index,
});

export const initAvailableProps = props => ({
	type: INIT_AVAILABLE_PROPS,
	availableProps: props,
});

export const selectProp = index => ({
	type: SELECT_PROP,
	index: index,
});
