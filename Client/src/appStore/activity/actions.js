import api from '../../api';

// actions types
export const FETCH_ACTIVITIES_SUCCESS = 'FETCH_ACTIVITIES_SUCCESS';
export const FETCH_ACTIVITIES_FAILED = 'FETCH_ACTIVITIES_FAILED';

// actions creator
const fetchSuccess = items => ({
	type: FETCH_ACTIVITIES_SUCCESS,
	items: items,
});

// actions creator
const fetchFail = err => ({
	type: FETCH_ACTIVITIES_FAILED,
	error: err,
});

export const fetchActivities = activityType => (dispatch, getState) => {

	return api.activity
		.get(`/${activityType}`)
		.then(res => {
			const items = res.data;
			// const request = searchProfiles;
			//
			// reset filter after search
			dispatch(fetchSuccess(items));
		})
		.catch(err => {
			dispatch(fetchFail(err.message));
		});
};
