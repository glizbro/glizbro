// actions types
import { FETCH_ACTIVITIES_SUCCESS, FETCH_ACTIVITIES_FAILED } from './actions';

// default state
const initState = {
	'iinvite': [],
	'watchedme': [],
	'iwatched': [],
	'favorites': [],
	'invitedme': [],
	'error': ''
};

export default (state = initState, action) => {
	if (!state) {
		return initState;
	}

	const { type, items, error } = action;


	switch (type) {
      case FETCH_ACTIVITIES_SUCCESS:
			return {
				...state,...items
			};
		case FETCH_ACTIVITIES_FAILED:
			return { ...state,error: [error] };
		default:
			return state;
	}
};
