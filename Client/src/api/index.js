import axios from 'axios';

const aws = {
	s3: {
		async uploadFile(file) {
			try {
				const response = await axios.post('/api/profile/uploadImage', {
					type: file.type,
				});
				await axios.put(response.data.url, file, {
					headers: {
						'Content-Type': file.type,
					},
				});
				return {
					images: response.data.images,
				};
			} catch (err) {
			}
		},
	},
};

function Api() {
	this.baseUrl = '/api';
	this.headers = {
		'Content-Type': 'application/json',
	};
	// api.matches.post(url,body)
	this.matches = axios.create({
		baseURL: `${this.baseUrl}/matches`,
	});
	this.profile = axios.create({
		baseURL: `${this.baseUrl}/profile`,
	});
	this.signup = axios.create({
		baseURL: `${this.baseUrl}/signup`,
		headers: this.headers,
	});
	this.login = axios.create({
		baseURL: `${this.baseUrl}/login`,
		headers: this.headers,
	});
	this.appConfiguration = axios.create({
		baseURL: `${this.baseUrl}/appConfiguration`,
		headers: this.headers,
	});
	this.chat = axios.create({
		baseURL: `${this.baseUrl}/chat`,
		headers: this.headers,
	});
	this.activity = axios.create({
		baseURL: `${this.baseUrl}/activity`,
		headers: this.headers,
	});

	this.aws = aws;
}

const api = new Api();

export default api;
