import styled from 'styled-components';

// animations
import { fadeIn } from '../../components/animations';

const Matches = styled.div`

	width: 100%;

   
	display: flex;
	justify-content: space-evenly;

	/* css for all children of Matches */
	& > div {
		padding: 16px;
	}
`;

const LeftBar = styled.div`
	flex-basis: 25%;
	display: flex;
	align-items: flex-start;

	@media (max-width: 600px) {
		display: none;
	}
`;

const Main = styled.div`
	width: 50%;
	min-width: 250px;
	margin: 0 20px;
	display: flex;
	flex-direction: column;
	align-items: center;
	padding-top: 8vh;

	.profilesList {
		width: 100%;
		display: flex;
		flex-direction: column;
		align-items: center;
		margin: 10px;

		/* profile card */
		& > div {
			margin: 10px;
			animation: ${fadeIn};
		}
	}

	.actionBar {
		width: 100%;
	}

	@media (max-width: 600px) {
		width: 100%;
	}
`;

const RightBar = styled.div`
	flex-basis: 25%;

	@media (max-width: 600px) {
		display: none;
	}
`;

export default Matches;
export { LeftBar, Main, RightBar };
