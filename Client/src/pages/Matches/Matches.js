// react
import React, { Fragment } from 'react';

// redux
import { connect } from 'react-redux';
import { statuses } from '../../appStore/relationship/reducer';

// containers
import Chat from '../../containers/Chat/Chat';
import MatchList from '../../containers/MatchList/MatchList';
import SearchBar from '../../containers/SearchBar/SearchBar';
import CurrentMatchCard from '../../containers/CurrentMatchCard/CurrentMatchCard';

// style components
import Matches, { Main, LeftBar, RightBar } from './MatchesStyled';

const MatchesPage = props => {
	const { relationship } = props;
	const userInRelationship = relationship.status === statuses.inRelationship;

	return (
		<Matches className='MatchesPage'>
			<LeftBar className='LeftBar'>
				<CurrentMatchCard />
			</LeftBar>
			<Main className='Main'>
				{userInRelationship ? (
					<Chat />
				) : (
					<Fragment>
						<SearchBar />
						<MatchList />
					</Fragment>
				)}
			</Main>
			<RightBar className='RightBar' />
		</Matches>
	);
};

const mapStateToProps = state => {
	return {
		relationship: state.relationship,
	};
};

const mapDispatchToProps = dispatch => ({});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(MatchesPage);
