import React from 'react';

// redux
import { connect } from 'react-redux';
import {
	selectProp,
	setFilter,
	removeFilter,
	setFilterName,
} from '../../../appStore/filter/actions';
import { fetchMatches } from '../../../appStore/matches/actions';
// steps
import Select from '../../../components/Select/Select';
import MultipleSelect from '../../../components/MultipleSelect/MultipleSelect';
import RangeInput from '../../../components/RangeInput/RangeInput';
import Button from '../../../components/Button/Button';
import Chip from '../../../components/Chip/Chip';

// styled components
import { Main, Header, Footer, Sidebar, Items } from './CreateFilterStyled';

class CreateFilter extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			sideNavIsOpen: false,
		};
	}
	selectItem = i => {
		this.setState(
			{
				sideNavIsOpen: false,
			},
			() => {
				// delay rerender
				setTimeout(() => {
					this.props.selectProp(i);
				}, 500); //TODO: refactor
			},
		);
	};

	closeSideBar = () => {
		this.setState({
			sideNavIsOpen: false,
		});
	};
	openSideBar = () => {
		this.setState({
			sideNavIsOpen: true,
		});
	};

	onSubmit = () => {
		this.props.toggleModal();
		this.props.fetchMatches();
	};

	render() {
		const { selectItem, closeSideBar } = this;
		const { sideNavIsOpen } = this.state;
		const {
			toggleModal,
			availableProps = [],
			selectedProps = [],
			removeFilter,
			setFilterName,
		} = this.props;
		return (
			<Main className='CreateFilter'>
				<div className='close'>
					<span className='btn' onClick={toggleModal}>
						X
					</span>
				</div>

				<Header>
					<h3>Find Your Love!</h3>
				</Header>
				<div>
					add name <input onBlur={e => setFilterName(e.target.value)} type='text' />
				</div>
				<Items>
					{selectedProps.map((o, i) => (
						<div key={o} className='item'>
							{this.getComponentByFilterName(o)}
							<i className='fa fa-trash' onClick={() => removeFilter(o, i)} />
						</div>
					))}
				</Items>
				<Footer>
					<Button type='circle' onClick={this.openSideBar}>
						+
					</Button>
					<Button onClick={this.onSubmit}> Submit </Button>
				</Footer>
				<Sidebar open={sideNavIsOpen} className='SidaBar'>
					<span className='close' onClick={closeSideBar}>
						X
					</span>
					<div className='items'>
						{availableProps.map((o, i) => (
							<Chip onClick={() => selectItem(i)} key={o}>
								{o}
							</Chip>
						))}
					</div>
				</Sidebar>
			</Main>
		);
	}

	getComponentByFilterName = filterProp => {
		const {
			statuses,
			languages,
			religions,
			pets,
			countries,
			characters,
			transports,
			interests,
			relationshipPurposes,
			educations,
		} = this.props.contentConfig;
		const { setFilter, filter } = this.props;
		const options = {
			age: renderRangeInput({
				prop: 'age',
				value: filter.age,
				onChange: setFilter,
				minVal: 0,
				maxVal: 120,
			}),
			tall: renderRangeInput({
				prop: 'tall',
				value: filter.tall,
				onChange: setFilter,
				minVal: 0,
				maxVal: 200,
				step: 5,
			}),
			weight: renderRangeInput({
				prop: 'weight',
				value: filter.weight,
				onChange: setFilter,
				minVal: 0,
				maxVal: 250,
			}),

			pets: (
				<MultipleSelect
					id='selectPets'
					name='pets'
					onChange={setFilter}
					options={pets}
					type='text'
					placeHolder='What pets you want on your match'
				/>
			),
			characters: (
				<MultipleSelect
					key='selectCharacter'
					id='selectCharacter'
					options={characters}
					name='characters'
					onChange={setFilter}
					type='text'
					placeHolder='Select characters you looking at your match'
				/>
			),
			country: (
				<Select
					id='selectCountries'
					key='selectCountries'
					options={countries.map(c => c.name)}
					name='country'
					onChange={setFilter}
					defaultValue='Select country of your match'
				/>
			),
			statuses: (
				<Select
					options={statuses}
					name='status'
					onChange={setFilter}
					defaultValue='Select status of your partner'
				/>
			),
			religion: (
				<Select
					options={religions}
					name='religion'
					onChange={setFilter}
					defaultValue='Select religion of your partner'
				/>
			),
			language: (
				<Select
					options={languages}
					name='language'
					onChange={setFilter}
					defaultValue='Enter your partner languages'
				/>
			),
			interests: (
				<MultipleSelect
					options={interests}
					name='interests'
					onChange={setFilter}
					id='selectInterests'
					type='text'
					placeHolder='What interests your partner should have?'
				/>
			),
			transport: (
				<Select
					name='transport'
					options={transports}
					onChange={setFilter}
					defaultValue='Enter your partner transport'
				/>
			),
			education: (
				<Select
					name='education'
					options={educations}
					onChange={setFilter}
					defaultValue='Enter your partner education'
				/>
			),
			relationshipPurpose: (
				<Select
					name='relationshipPurpose'
					options={relationshipPurposes}
					onChange={setFilter}
					defaultValue='Enter your partner relationship Purpose'
				/>
			),
		};
		return options[filterProp];
	};
}

const mapStateToProp = state => {
	return {
		availableProps: state.matchesFilter.availableProps,
		selectedProps: state.matchesFilter.selectedProps,
		homeConfig: state.appConfiguration.homeConfig,
		contentConfig: state.appConfiguration.contentConfig,
		filter: state.matchesFilter.filter,
	};
};

const mapDispatchToProps = dispatch => ({
	setFilterName: name => dispatch(setFilterName(name)),
	selectProp: prop => dispatch(selectProp(prop)),
	setFilter: (key, value) => dispatch(setFilter(key, value)),
	fetchMatches: () => dispatch(fetchMatches()),
	removeFilter: (filter, index) => dispatch(removeFilter(filter, index)),
});

export default connect(
	mapStateToProp,
	mapDispatchToProps,
)(CreateFilter);

// TODO MOVE IT TO FOLODERS

function renderRangeInput({ prop, value, onChange, minVal, maxVal, step }) {
	const min = value ? value.min : minVal;
	const max = value ? value.max : maxVal;
	return (
		<React.Fragment>
			<span>
				{prop} {min} - {max}
			</span>
			<RangeInput
				name={prop}
				onChange={onChange}
				step={step}
				min={minVal}
				max={maxVal}
			/>
		</React.Fragment>
	);
}
