import styled from 'styled-components';
import theme from '../../../theme/theme';

export const Main = styled.div`
	width: 50vw;
	height: 60vh;

	position: relative;

	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: stretch;

	.close {
		text-align: right;

		.btn {
			font-size: 1.5em;
			font-weight: 600;
			padding: 10px;
			cursor: pointer;
		}
   }
   
   @media (max-width: 600px) {
      width: 100%;
   }
`;

export const Header = styled.div`
	text-align: center;
	margin-bottom: 10px;
`;

export const Items = styled.div`
	width: 100%;
	flex-grow: 1;
	display: flex;
	flex-wrap: wrap;
	justify-content: space-evenly;
	align-content: flex-start;
	overflow-y: auto;

	.item {
		width: 45%;
		display: flex;
		align-items: center;
		margin: 10px 2px;
		min-height: 29px;
		justify-content: space-between;

		& > i {
			margin-left: 5px;
		}
	}
`;

// buttons wrapper
export const Footer = styled.div`
	display: flex;
	justify-content: space-evenly;
	align-items: center;
`;

export const Sidebar = styled.div`
	height: 100%;
	width: ${props => (props.open ? '100%' : 0)};
	display: flex;
	flex-direction: column;
	position: absolute;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: ${props => props.theme.main.fg};
	overflow: hidden;
	transition: width 0.5s;

	.close {
		font-size: 1.3em;
		color: ${theme.main.white};
		font-weight: 600;
		text-align: right;
		padding: 10px;
		text-align: right;
		cursor: pointer;

		&:hover {
			color: ${theme.main.error};
		}
	}
	.items {
	}
`;
