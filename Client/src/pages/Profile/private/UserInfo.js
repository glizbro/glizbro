// React
import React, { useState, memo } from 'react';

// Redux
import { connect } from 'react-redux';

import styled from 'styled-components';
import NavItem from './Categories/CategorySelector/NavItem/NavItem';

//loader
import CategoriesContainer from './Categories/CategoryContainer/CategoryContainer';

const UserInfo = memo(props => {
	const { firstName, lastName } = props.user.public;

	const fullName = `${firstName} ${lastName}`;
	const { profileImg } = props.user;
	console.log('UserInfo', props);

	const [currentCategoryClicked, setCurrentCategoryClicked] = useState('Personal Details');

	const clickCategory = event => {
		const name = event.currentTarget.getAttribute('name');
		setCurrentCategoryClicked(name);
	};

	return (
		<Container>
			<div className="profilePic">
				<img
					src={`https://glizbro-images-1.s3.amazonaws.com/${profileImg.src}`}
					alt="personal card in profile"
				/>
			</div>
			<div className="name">{fullName}</div>
			
			<div className="categories">
				<NavItem
					onClick={clickCategory}
					currentCategoryClicked={currentCategoryClicked}
					name="Personal Details"
				>
					Personal Details
				</NavItem>
				<NavItem
					onClick={clickCategory}
					currentCategoryClicked={currentCategoryClicked}
					name="Appearance"
				>
					Appearance
				</NavItem>
				<NavItem
					onClick={clickCategory}
					currentCategoryClicked={currentCategoryClicked}
					name="Country and Religion"
				>
					Country and Religion
				</NavItem>
			</div>
			<div className="details" />
			<div className="userData">
				<CategoriesContainer
					className="categories"
					currentCategoryClicked={currentCategoryClicked}
				/>
			</div>
		</Container>
	);
});

const mapState = state => ({
	user: state.user.data,
});

export default connect(
	mapState,
	{},
)(UserInfo);

const Container = styled.div`
	.profilePic {
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		height: 150px;

		img {
			border-radius: 50%;
			width: 95px;
			height: 95px;
			border: 1px solid;
		}
	}

	.name {
		text-align: center;
	}
	.userData {
		width: 100%;

		display: flex;
		flex-direction: column;
		justify-content: space-between;
		padding: 20px;
		align-items: center;

		.aboutMe {
			width: 100%;
			display: flex;
			flex-direction: column;
			justify-content: flex-start;
			color: ${p => p.theme.palette.primary};

			.header {
				font-weight: bold;
			}
		}

		.categoriesSelector {
			flex-direction: column;
			font-size: 10px;
		}

		.userDataContent {
			width: 100%;
			display: flex;
			flex-direction: column;
			justify-content: center;
			align-items: flex-start;
			padding-top: 40px;

			@media (max-width: 900px) {
				padding-left: 0;
				padding-right: 0;
			}

			.userDataContentInner {
				width: 100%;
				display: flex;
				flex-direction: column;
				justify-content: space-between;

				.userDataHeader {
					width: 100%;
					display: flex;
					flex-direction: row;
					justify-content: space-between;
					align-items: center;

					color: ${p => p.theme.palette.primary};
					border-bottom: 1px solid #edeff1;
					border-top: 1px solid #edeff1;

					.text {
						width: 60%;
						display: flex;
						flex-direction: row;
						justify-content: flex-end;
						align-items: center;
					}

					.buttonsArea {
						width: 100%;

						display: flex;
						flex-direction: row;
						justify-content: flex-end;
						align-items: center;
					}
				}
			}
		}
	}
`;
