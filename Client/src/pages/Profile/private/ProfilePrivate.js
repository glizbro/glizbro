// React
import React from 'react';

//styled components
import { ProfileStyled } from './ProfilePrivateStyled';

//components
import Card from '../../../components/Card/Card';

// redux
import { connect } from 'react-redux';
import { uploadImgRequest } from '../../../appStore/user/actions';

// containers
import UserInfo from './UserInfo';
import ImageViewer from '../../../containers/ImageViewer/ImageViewer';

const ProfilePage = props => {
	return (
		<ProfileStyled className="ProfilePage">
			<Card className="imageGallery">
				<ImageViewer />
			</Card>
			<Card className="userInfo">
				<UserInfo />
			</Card>
		</ProfileStyled>
	);
};

const mapStateToProps = state => {
	return {};
};

const mapDispatch = dispatch => ({
	uploadImgRequest: file => dispatch(uploadImgRequest(file)),
});

export default connect(
	mapStateToProps,
	mapDispatch,
)(ProfilePage);
