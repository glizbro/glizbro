import styled from 'styled-components';
import theme from '../../../../theme/theme';

export const Sidebar = styled.div`
	height: 100%;
	width: ${props => (props.open ? '100%' : 0)};
	display: flex;
	flex-direction: column;
	position: absolute;
	z-index: 1;
	top: 33%;
	left: 0; 
	background-color: ${theme.main.gray};
	overflow: hidden;
	transition: width 0.5s;

	& > span {
		font-size: 1.3em;
		color: ${theme.main.white};
		font-weight: 600;
		text-align: right;
		cursor: pointer;

		&:hover {
			color: ${theme.main.primary};
		}
	}
	.items {
	}
`;

export const Items = styled.div`
	/* width: 100%; */
	flex-grow: 1;
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	align-content: flex-start;
	/* overflow-y: auto; */

	.item {
		width: 90%;
		display: flex;
		align-items: center;
		/* margin: 10px 2px; */
		min-height: 29px;
		justify-content: center;

		& > i {
			margin-left: 5px;
		}
	}
`;