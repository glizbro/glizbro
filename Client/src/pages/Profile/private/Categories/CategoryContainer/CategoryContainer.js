import React from 'react';

import Card from '../../../../../components/Card/Card';
import Label from '../../../../../components/Label/Label';
import DatePicker from '../../../../../components/DatePicker/DatePicker';
import Select from '../../../../../components/Select/Select';
import Input from '../../../../../components/Input/Input';
import Button from '../../../../../components/Button/Button';
import CardSimpleStyled from '../../../../../components/CardSimple/CardSimpleStyled';
import Loader from '../../../../../components/Loader/Loader';

import theme from '../../../../../theme/theme';
import { userProperties } from '../../../../../config';

// redux
import { connect } from 'react-redux';
import {
	openEdit,
	closeEdit,
	initProfileConfiguration,
	clickSave,
	editChanged,
} from '../../../../../appStore/profileConfig/actions';
import { _onChange, _onInit } from '../../../../../appStore/fields/actions';

//profile config
const Components = {
	age: DatePicker,
	tall: Input,
	weight: Input,
	country: Select,
	religion: Select,
	language: Select,
	education: Select,
	gender: Select,
};

class CategoriesContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	profileOnChange = (name, value) => {
		this.props._onChange(name, value);
		this.props.editChanged(name);
	};

	getDay = date => {
		if (!date) return;
		return date.split('/')[0];
	};

	getMonth = date => {
		if (!date) return;
		return date.split('/')[1];
	};

	getYear = date => {
		if (!date) return;
		return date.split('/')[2];
	};

	render() {
		const { user, currentCategoryClicked } = this.props;
		const { isOpened } = this.props.profileConfiguration;
		if (!user || !user.data || !user.data.public) {
			return <Loader />;
		}
		return (
			<div className='userDataContent'>
				<CardSimpleStyled
					width='100%'
					key={currentCategoryClicked}
					background={theme.inherit.fg}
					className='userDataContentInner'
				>
					<div className='userDataHeader'>
						<div className='buttonsArea'>
							{this.props.profileConfiguration.editChanged ? (
								<Button
									width='60px'
									height='25px'
									color={theme.main.fg}
									title='Save'
									onClick={() => {
										this.props.clickSave(currentCategoryClicked);
										this.props.closeEdit(
											currentCategoryClicked,
											categories[currentCategoryClicked],
										);
									}}
									name={currentCategoryClicked}
								>
									<i className='fas fa-pen fa-sm'> Save</i>
								</Button>
							) : (
								<Button
									width='60px'
									height='25px'
									color={theme.main.fg}
									title='Edit'
									onClick={() => {
										this.props.openEdit(
											currentCategoryClicked,
											categories[currentCategoryClicked],
										);
										this.props._onInit({}, 'profileConfiguration');
									}}
									name={currentCategoryClicked}
								>
									<i className='fas fa-pen fa-sm'> Edit</i>
								</Button>
							)}
							<Button
								width='60px'
								height='25px'
								color={theme.main.fg}
								title='Close'
								disabled={this.props.profileConfiguration.editComponent ? 0 : 1}
								onClick={() => {
									this.props.closeEdit(
										currentCategoryClicked,
										categories[currentCategoryClicked],
									);
									let cleanObj = {};
									this.props._onInit(cleanObj, 'profileConfiguration');
								}}
								name={currentCategoryClicked}
							>
								<i className='fa fa-window-close'> Close</i>
							</Button>
						</div>
					</div>
					{isOpened === false ? (
						<Card className='categories' width='100%'>
							{categories[currentCategoryClicked].map(field => {
								return (
									<Label
										key={field}
										icon={labelIcons[field]}
										padding='5px'
										labelName={field}
										labelNameColor={theme.staticContent.gb}
										labelValueColor={theme.main.fg}
										text={user.data.public[field]}
									/>
								);
							})}
						</Card>
					) : (
						<Card className='categories' width='100%'>
							{categories[currentCategoryClicked].map(field => {
								let Component = Components[field];
								let options = this.props.contentConfig[
									userProperties[field].option
								];
								return (
									<Label
										key={field}
										icon={labelIcons[field]}
										padding='5px'
										labelName={field}
										labelNameColor={theme.staticContent.gb}
										labelValueColor={theme.main.fg}
										textComponent={
											<Component
												{...userProperties[field]}
												onChange={this.profileOnChange}
												options={options ? options : []}
												value={
													isOpened === true &&
													field in this.props.form.profileConfiguration
														? this.props.form.profileConfiguration[field]
														: user.data.public[field]
												}
												day={this.getDay(user.data.public[field])}
												month={this.getMonth(user.data.public[field])}
												year={this.getYear(user.data.public[field])}
											/>
										}
									/>
								);
							})}
						</Card>
					)}
				</CardSimpleStyled>
			</div>
		);
	}
}

let labelIcons = {
	verified: () => <i className='fas fa-home' />,
	age: () => <i className='fas fa-gifts' />,
	email: () => <i className='far fa-envelope-open' />,
	country: () => <i className='fas fa-globe' />,
	city: () => <i className='fas fa-home' />,
	gender: () => <i className='far fa-heart' />,
	status: () => <i className='fas fa-graduation-cap' />,
	religion: () => <i className='fas fa-grip-horizontal' />,
	tall: () => <i className='fas fa-grip-vertical' />,
	weight: () => <i className='fas fa-grip-horizontal' />,
	smoking: () => <i className='fas fa-smoking' />,
	alcohol: () => <i className='fas fa-glass-cheers' />,
	language: () => <i className='fas fa-graduation-cap' />,
	pets: () => <i className='fas fa-paw' />,
	character: () => <i className='fas fa-hiking' />,
	education: () => <i className='fas fa-graduation-cap' />,
	relationshipPurpose: () => <i className='far fa-heart' />,
	transport: () => <i className='fas fa-tachometer-alt' />,
	interests: () => <i className='fas fa-hiking' />,
};

//properties to show in the UserDataContent
let categories = {
	'Personal Details': ['gender', 'age', 'country', 'education'],
	Appearance: ['weight', 'tall'],
	'Country and Religion': ['religion'],
};

const mapStateToProps = state => {
	return {
		matches: state.matches,
		user: state.user,
		contentConfig: state.appConfiguration.contentConfig,
		profileConfiguration: state.profileConfiguration,
		filter: state.matchesFilter.filter,
		form: state.form,
	};
};

const mapDispatchToProps = dispatch => ({
	openEdit: (name, categories) => dispatch(openEdit(name, categories)),
	editChanged: name => dispatch(editChanged(name)),
	closeEdit: name => dispatch(closeEdit(name)),
	_onChange: (name, value) => dispatch(_onChange(name, value, 'profileConfiguration')),
	_onInit: (value, stateName) => dispatch(_onInit(value, stateName)),
	initProfileConfiguration: props => dispatch(initProfileConfiguration(props)),
	clickSave: name => dispatch(clickSave(name)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(CategoriesContainer);
