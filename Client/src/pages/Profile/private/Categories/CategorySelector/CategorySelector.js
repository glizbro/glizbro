import React from 'react';

import Button from '../../../../../components/Button/Button';
import Categories from './CatergoriesStyled';
import NavItem from './NavItem/NavItem';

class CategoriesSelector extends React.Component {
	render() {
		const { clickCategory, currentCategoryClicked } = this.props;
		return (
			<Categories className="categoriesSelector">
				<NavItem
					onClick={clickCategory}
					currentCategoryClicked={currentCategoryClicked}
					name="Personal Details"
				>
					Personal Details
				</NavItem>
				<NavItem
					onClick={clickCategory}
					currentCategoryClicked={currentCategoryClicked}
					name="Appearance"
				>
					Appearance
				</NavItem>
				<NavItem
					onClick={clickCategory}
					currentCategoryClicked={currentCategoryClicked}
					name="Country and Religion"
				>
					Country and Religion
				</NavItem>
			</Categories>
		);
	}
}

export default CategoriesSelector;
