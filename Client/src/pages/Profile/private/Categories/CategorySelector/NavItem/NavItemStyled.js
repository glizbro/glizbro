import styled from "styled-components";
import theme from '../../../../../../theme/theme';

const NavStyled = styled.div`
   display: flex;
    width: 100%;
    height: 30px;
    align-items: center;
    justify-content: center;
    background: ${props => (props.selected ? theme.main.fg : theme.main.white)};
    color: ${props => (props.selected ? theme.main.white : theme.main.fg)};
    border-radius: 4px;
    cursor: pointer;

    &:hover {
        color: dodgerblue;
    }
`

export default NavStyled;