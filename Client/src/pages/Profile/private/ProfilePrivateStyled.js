import styled from 'styled-components';

// animations
import { fadeIn } from '../../../components/animations';

const ProfileStyled = styled.div`
	width: 100%;
	height: 90vh;

	flex-grow: 1;
	display: flex;
	justify-content: space-evenly;
	align-items: stretch;

	padding: ${p => p.theme.spacing.lg};

	.imageGallery,
	.userInfo {
		width: 45%;
		padding: ${p => p.theme.spacing.lg};
	}

	
`;

export { ProfileStyled };
