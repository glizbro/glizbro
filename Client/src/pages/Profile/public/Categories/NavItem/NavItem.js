

import React from "react";
import NavStyled from './NavItemStyled';

class NavItem extends React.Component {
   constructor(props) {
      super(props);
   }

   isSelected(categoryClicked, selfName) {
      return categoryClicked === selfName;
   }

   render() {
      const {onClick, currentCategoryClicked, name} = this.props;
      return(
         <NavStyled onClick={onClick} selected = {this.isSelected(currentCategoryClicked, name)} name = {name}>
            {this.props.children}
         </NavStyled>
      );
  }

}

export default NavItem;