import React from 'react';

//styled components
import { ProfilePublicStyled, ImageProfile } from './ProfilePublicStyled';
// theme
import theme from '../../../theme/theme';

//components
import CardSimpleStyled from '../../../components/CardSimple/CardSimpleStyled';
import Card from '../../../components/Card/Card';

import Label from '../../../components/Label/Label';


import Api from '../../../api';

//loader
import Loader from '../../../components/Loader/Loader';

class ProfilePage extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			currentCategoryClicked: 'Personal Details',
			publicUser: null,
		};
	}

	clickCategory = event => {
		const name = event.currentTarget.getAttribute('name');
		this.setState({
			currentCategoryClicked: name,
		});
	};

	componentDidMount() {
		Api.profile
			.get(`/${this.props.match.params.id}`)
			.then(res => {
				this.setState({
					publicUser: res.data,
				});
			})
			.catch(err => {});
	}

	render() {
		const { currentCategoryClicked, publicUser } = this.state;
		if (!publicUser) {
			return <Loader />;
		}

		const fullName = `${publicUser.public.firstName} ${publicUser.public.lastName}`;

		return (
			<ProfilePublicStyled className="Profile">
				{/* image area */}
				<Card color={theme.main.gray} className="ProfileInner" row>
					<Card width="60vh" marginBottom="4%" className="firstCard">
						<ImageProfile>
							<div className="img">
								<div className="txt">{fullName}</div>
							</div>
						</ImageProfile>
					</Card>
					{/* profile content */}
					<Card width="60vh" marginBottom="4%" className="secondCard">
						<div className="userData">
							<div className="innerImage">
								<img  alt="personal card in profile" />
								<div className="innerImageTxt">{fullName}</div>
							</div>
							<div className="aboutMe">
								<div className="header">About Me</div>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry.
							</div>
							
							<div className="userDataContent">
								<CardSimpleStyled
									width="100%"
									height="100%"
									key={currentCategoryClicked}
									background={theme.inherit.fg}
									className="userDataContentInner"
								>
									<Card className="categories">
										{categories[currentCategoryClicked].map(field => {
											return (
												<Label
													key={field}
													icon={labelIcons[field]}
													padding="5px"
													labelName={field}
													labelNameColor={theme.staticContent.gb}
													labelValueColor={theme.main.fg}
													text={publicUser.public[field]}
												/>
											);
										})}
									</Card>
								</CardSimpleStyled>
							</div>
						</div>
					</Card>
				</Card>
			</ProfilePublicStyled>
		);
	}

	getDay = date => {
		if (!date) return;
		return date.split('/')[0];
	};

	getMonth = date => {
		if (!date) return;
		return date.split('/')[1];
	};

	getYear = date => {
		if (!date) return;
		return date.split('/')[2];
	};
}

//properties to show in the UserDataContent
let categories = {
	'Personal Details': ['gender', 'age', 'country', 'education'],
	Appearance: ['weight', 'tall'],
	'Country and Religion': ['religion'],
};

let labelIcons = {
	verified: () => <i className="fas fa-home" />,
	age: () => <i className="fas fa-gifts" />,
	email: () => <i className="far fa-envelope-open" />,
	country: () => <i className="fas fa-globe" />,
	city: () => <i className="fas fa-home" />,
	gender: () => <i className="far fa-heart" />,
	status: () => <i className="fas fa-graduation-cap" />,
	religion: () => <i className="fas fa-grip-horizontal" />,
	tall: () => <i className="fas fa-grip-vertical" />,
	weight: () => <i className="fas fa-grip-horizontal" />,
	smoking: () => <i className="fas fa-smoking" />,
	alcohol: () => <i className="fas fa-glass-cheers" />,
	language: () => <i className="fas fa-graduation-cap" />,
	pets: () => <i className="fas fa-paw" />,
	character: () => <i className="fas fa-hiking" />,
	education: () => <i className="fas fa-graduation-cap" />,
	relationshipPurpose: () => <i className="far fa-heart" />,
	transport: () => <i className="fas fa-tachometer-alt" />,
	interests: () => <i className="fas fa-hiking" />,
	//missing
	//ToDo: add gender you looking for
	//ToDo: add place of education
	//ToDo: country birth
	//ToDo: number of kids
	//ToDo: Basar/vegan
	// /ToDo: rock/rap
	//Military service ToDo: true/false
	//Political ViewsToDo: harta barta
	//ToDo: tatoo/piercing
	//Hair color ToDo: color
	//Hair Style ToDo: Style
	//Color of the eyes ToDo: color
};

export default ProfilePage;
