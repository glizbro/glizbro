import styled from "styled-components";
import theme from "../../../theme/theme";
import faker from 'faker'

// animations
import { fadeIn } from '../../../components/animations';

const ProfilePublicStyled = styled.div`
   width: 100%;
   display: flex;
   flex-direction: column;
   justify-content: space-between;
   align-items: center;
   
   /* css for all children */
   & > div {
      padding: 16px;
   }

   .ProfileInner {
      align-items: stretch;
      width: 100%;
      animation: ${fadeIn};

      @media (max-width: 900px) {
         display: flex;
         flex-direction: column;
         justify-content: space-between;
	   }

      .userData {
         width: 100%;
  
         display: flex;
         flex-direction: column;
         justify-content: space-between;
         padding: 20px;
         align-items: center;

         @media (max-width: 900px) {
            padding-bottom: 15px;
            
         }
         
         .innerImage {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height:150px;

            img {
               border-radius: 50%;
            }

            .innerImageTxt {
               color: ${theme.main.fg};
               font-weight: bold;
            }
         }

         .aboutMe {
               width: 100%;
               display: flex;
               flex-direction: column;
               justify-content: flex-start;
               color: ${theme.main.fg};
               height: 200px;

               .header {
                  font-weight: bold;
               }
            }

            .categoriesSelector {
               flex-direction: column;
               font-size: 10px;
            }


         .userDataContent {
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: flex-start;
            padding-top: 40px;

            @media (max-width: 900px) {
               padding-left: 0;
               padding-right: 0;
            }

            .userDataContentInner{
               width: 100%;
               display: flex;
               flex-direction: column;
               justify-content:space-between;

               .userDataHeader{
                  width: 100%;
                  display: flex;
                  flex-direction: row;
                  justify-content: space-between;
                  align-items: center;
               
                  color: ${theme.main.fg};
                  border-bottom: 1px solid #edeff1;
                  border-top: 1px solid #edeff1;

                  .text {
                     width: 60%;
                     display: flex;
                     flex-direction: row;
                     justify-content: flex-end;
                     align-items: center;
                  }
                  
                  .buttonsArea {
                     width: 100%;

                     display: flex;
                     flex-direction: row;
                     justify-content: flex-end;
                     align-items: center;
                  }
               }
            }
         }
      }
   }

   .firstCard {
      width: 60vh;
      display: flex;
      align-items: center;
      justify-content: center;

      @media (max-width: 900px) {
		   width: auto;
	   }
   }
`;

const ImageProfile = styled.div`
   width: 98%;
   height: ${props => (props.height ? props.height : '98%')};

   position: initial;
   display: flex;
   flex-direction: row;
   justify-content: center;
   align-items: flex-end;

   .UserName {
      font-size: 1.7em;
      color: ${theme.main.primary};
      font-weight: bold;
      position: absolute;
      padding-bottom: 25%;
   }

   .img {
      height: 100%;
      width: 100%;
      background-image: url(${faker.image.avatar()});
      background-size: cover;
      background-position: center;
      font-size: 1.7em;
      color: ${theme.main.primary};
      font-weight: bold;
      padding-bottom: 25%;
      display: flex;
      align-items: flex-end;
      justify-content: left;
      color: ${theme.main.white};
      .txt {
         background: rgba(0, 0, 0, 0.8);
      }
   }
`;

export { ProfilePublicStyled, ImageProfile };
