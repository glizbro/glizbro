import React from 'react';
import Input from '../../components/Input/Input';
import { connect } from 'react-redux';
import Card from '../../components/Card/Card';
import Page from './LoginStyled';
import Button from '../../components/Button/Button';

// react router
import { Link } from 'react-router-dom';

// redux
import { login } from '../../appStore/login/actions';
import { _onChange } from '../../appStore/fields/actions';

const LoginPage = props => {
   const { onChange, loginForm, login } = props;
   console.log('login render')
	return (
		<Page className="LoginPage">
			<Card className="Form">
				<div className="header">Header</div>
				<div className="fields">
					<Input value={loginForm.email} name="email" onChange={onChange} placeholder="Email" />
					<Input
						value={loginForm.password}
						name="password"
						onChange={onChange}
						placeholder="Password"
						type="password"
					/>
				</div>
				<div className="submit">
					<Button onClick={login} type='button' >
						Login
					</Button>
				</div>
				<div className="links">
					<Link to="/signup"> Register</Link>
					<Link to="/signup"> Forget Password?</Link>
				</div>
			</Card>
		</Page>
	);
};

const mapStateToProps = state => ({
	loginForm: state.form.login,
});

const mapDispatchToProps = dispatch => ({
	login: () => dispatch(login()),
	onChange: (name, value) => dispatch(_onChange(name, value, 'login')),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(LoginPage);
