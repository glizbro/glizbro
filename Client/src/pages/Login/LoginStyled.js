import styled from 'styled-components';
import theme from '../../theme/theme';

const Login = styled.div`
	width: 100%;
	flex-grow: 1;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	background-size: cover;
	background-position: center;
	background: url(/mainpic.jpg);

	.Form {
		display: flex;
		flex-direction: column;
		align-items: stretch;
		justify-content: flex-start;

		color: palegoldenrod;
		font-weight: 600;
		background: #242424;
		border: 1px solid black;
		opacity: 0.9;
		padding: 16px;
		text-align: center;
		min-height: 300px;
		min-width: 320px;

		.header {
		}
		.fields {
			flex-grow: 1;
			display: flex;
			flex-direction: column;
			justify-content: flex-start;

			input {
				margin: 10px auto;
				width: 100%;
			}
		}

		.submit {
			margin: 10px 0;
			button {
				width: 100%;
				background: #ff1771;
				margin: 10px auto;
			}
		}
		.links {
			display: flex;
			justify-content: space-between;

			a {
				color: palegoldenrod;
			}
		}
	}
`;

export default Login;
