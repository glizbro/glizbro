import styled from "styled-components";
import Card from "../../components/Card/Card";
import theme from "../../theme/theme";

export default styled(Card)`
	width: 100%;
	background: ${props => props.theme.palette.inherit};
	color: ${props => props.theme.palette.primary};
	margin-bottom: 10px;
	padding: 6px;
	font-weight: initial;
   display:flex;
	flex-direction: row;
	justify-content: center;
	align-items: flex-start;
	box-shadow: none;

	.activityNavLink {
		text-decoration: none;
		
	}

	.leftPanelWrapper {
		width: 100%;
		height: 90vh;
		display: flex;
		flex-direction: row;
		align-items: flex-start;
		justify-content: flex-start;
		position: absolute;
	}

	.link {
		color: ${props => props.theme.palette.primary};
		text-decoration: none;
		text-decoration-line: none;
		/* font-weight: bold; */
		height: 10%;
		width: 70%;
		display: flex;
		flex-direction: row;
		align-items: center;
		justify-content: space-between;
		border-top: 1px solid ${props => props.theme.palette.primary};
		padding-right: 10px;


		

		&:hover {
			color: ${props => props.theme.palette.primary};
			/* text-decoration: underline; */
			text-decoration: none;
		}
	}
   
`;
