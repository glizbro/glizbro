

// react
import React, { useState, useCallback, useEffect } from 'react';

import { connect } from 'react-redux';

import Container from '../ContentPageStyled';
import DefaultContent from "../../../Activity/ContentPages/Default/DefaultContent";
import ActivityCard from "../../../../containers/ActivityCard/ActivityCard";
import theme from "../../../../theme/theme";

import contract from '../../../../util/contract';

//redux
import {fetchActivities} from '../../../../appStore/activity/actions';

const AddedToFavorties = props => {
   useEffect(() => {
		props.fetchActivities(contract.activities.favorites);
	}, []);
   const favorites = props.activities.favorites;
   return (
      <Container className = 'Favorties'>
         {favorites === undefined || favorites.length == 0 ? (
            <DefaultContent icon={theme.icons.activity.favorites} text="Here you will see you favorite profiles" />
         ) : (
            favorites.map(favorite => {
               const { firstName, lastName, age, country, education } = favorite.public;
               const { _id } = favorite.userData;
               const { eventDate } = favorite;
               return (
                  <ActivityCard
                     firstName={firstName}
                     lastName={lastName}
                     age={age}
                     country={country}
                     education={education}
                     eventDate={eventDate}
                     _id={_id}
                  />
               );
            })
         )}
      </Container>
   );
}



const mapStateToProps = state => {
	return {
      relationship: state.relationship,
      activities: state.activities
	};
};

const mapDispatchToProps = dispatch => ({
   fetchActivities: (activityType) => dispatch(fetchActivities(activityType)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(AddedToFavorties);