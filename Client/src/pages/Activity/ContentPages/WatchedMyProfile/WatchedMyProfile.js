// react
import React, { useEffect } from 'react';

import { connect } from 'react-redux';

import Container from '../ContentPageStyled';
import DefaultContent from '../../../Activity/ContentPages/Default/DefaultContent';
import ActivityCard from '../../../../containers/ActivityCard/ActivityCard';
import theme from '../../../../theme/theme';

import contract from '../../../../util/contract';

//redux
import { fetchActivities } from '../../../../appStore/activity/actions';

const WatchedMyProfile = props => {
	useEffect(() => {
			props.fetchActivities(contract.activities.watchedme);
	}, []);
	const watchedme = props.activities.watchedme;
	return (
		<Container className='MyProfile'>
			{watchedme === undefined || watchedme.length === 0 ? (
				<DefaultContent
					icon={theme.icons.activity.watchedme}
					text='Here you will see profiles that visited you'
				/>
			) : (
				watchedme.map(watchedmeprofile => {
					const {
						firstName,
						lastName,
						age,
						country,
						education,
					} = watchedmeprofile.userData.public;
					const { _id } = watchedmeprofile.userData;
					const { eventDate } = watchedmeprofile;
					return (
						<ActivityCard
							firstName={firstName}
							lastName={lastName}
							age={age}
							country={country}
							education={education}
							eventDate={eventDate}
							_id={_id}
						/>
					);
				})
			)}
		</Container>
	);
};

const mapStateToProps = state => {
	return {
		relationship: state.relationship,
		activities: state.activities,
	};
};

const mapDispatchToProps = dispatch => ({
	fetchActivities: activityType => dispatch(fetchActivities(activityType)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(WatchedMyProfile);
