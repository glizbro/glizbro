import styled from "styled-components";
import theme from "../../../theme/theme";
import MediaQuery from '../../../config/media-query';

export default styled.div`
   width: 70vw;
   background: ${theme.main.white};
   /* color: ${theme.main.black}; */
   color: red;
   margin-bottom: 10px;
   padding: 6px;
   font-weight: initial;
   display: flex;
   flex-direction: column;
   align-items: center;
   justify-content: center;
   padding: 15px;
   overflow: hidden;
   background: ${theme.main.inherit};
   z-index: 9999;
   


   @media (max-width: ${MediaQuery.mobile}) {
			width: 100%;
			
		}
`;