// react
import React, {  useEffect } from 'react';

import { connect } from "react-redux";
import Container from '../ContentPageStyled';

//components
import ActivityCard from "../../../../containers/ActivityCard/ActivityCard";
import DefaultContent from "../../../Activity/ContentPages/Default/DefaultContent";
import Loader from "../../../../components/Loader/Loader";
import theme from "../../../../theme/theme";

import contract from '../../../../util/contract';

//redux
import {fetchActivities} from '../../../../appStore/activity/actions';

const IInviteProfiles = props => {
   if (props.activities.iinvite === undefined ) {
      return (
         <Loader></Loader>
      );
   }
   useEffect(() => {
		props.fetchActivities(contract.activities.iinvite);
	}, []);
   const iinvite = props.activities.iinvite;
   return (
      <Container className = 'IInvite'>
         {iinvite === undefined || iinvite.length === 0 ? (
            <DefaultContent icon={theme.icons.activity.iinvite} text="Here you will see profiles you have invited" />
         ) : (
            iinvite.map(invitee => {
               const { firstName, lastName, age, country, education } = invitee.userData.public;
               const { _id } = invitee.userData;
               const { eventDate } = invitee;
               return (
                  <ActivityCard
                     firstName={firstName}
                     lastName={lastName}
                     age={age}
                     country={country}
                     education={education}
                     eventDate={eventDate}
                     _id={_id}
                  />
               );
            })
         )}
      </Container>
   );
};

const mapStateToProps = state => {
   return {
      relationship: state.relationship,
      activities: state.activities
   };
};

const mapDispatchToProps = dispatch => ({
   fetchActivities: (activityType) => dispatch(fetchActivities(activityType)),
});


export default connect(
   mapStateToProps,
   mapDispatchToProps
)(IInviteProfiles);
