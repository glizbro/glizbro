


// react
import React, { useEffect, useState, useCallback } from "react";

import { connect } from 'react-redux';

import Container from './DefaultContentStyled';

//components
import ActivityCard from '../../../../containers/ActivityCard/ActivityCard';
import Loader from "../../../../components/Loader/Loader";

const DefaultContent = props => {
   return (
      <Container className = 'DefaultActivityContent'>
            <div className = 'icon'>
               {props.icon()}
            </div>
            <div className = 'defaultText'>
               {props.text}
            </div>
      </Container>
   );
}


export default DefaultContent;