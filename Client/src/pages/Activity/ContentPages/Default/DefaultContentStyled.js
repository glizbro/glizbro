import styled from "styled-components";
import theme from "../../../../theme/theme";

export default styled.div`
  height: 80vh;
   width: 80%;
   background: ${theme.main.inherit};
   /* color: ${theme.main.black}; */
   color: red;
   margin-bottom: 10px;
   padding: 6px;
   font-weight: initial;
   display: flex;
   flex-direction: column;
   align-items: center;
   justify-content: center;
   padding: 15px;

   .icon {
     width: 100px;
     height: 100px;

     & > i{
      font-size: 100px;
      color: ${theme.main.primary}
     }
   }
   .defaultText {
     display: flex;
     flex-direction: row;
     align-items: center;
     justify-content: center;
     padding: 20px;
     width: 100%;
     height: 100px;
     color: ${theme.main.primary}
   }
`;
