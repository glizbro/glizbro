// react
import React, { useEffect } from 'react';

import { connect } from 'react-redux';

import Container from '../ContentPageStyled';
import DefaultContent from '../../../Activity/ContentPages/Default/DefaultContent';
import ActivityCard from '../../../../containers/ActivityCard/ActivityCard';
import theme from '../../../../theme/theme';

import contract from '../../../../util/contract';

//redux
import { fetchActivities } from '../../../../appStore/activity/actions';

const IWatchProfiles = props => {
	useEffect(() => {
		props.fetchActivities(contract.activities.iwatched);
	}, []);
	const iwatched = props.activities.iwatched;
	return (
		<Container className='IWatch'>
			{iwatched === undefined || iwatched.length === 0 ? (
				<DefaultContent
					icon={theme.icons.activity.iwatched}
					text='Here you will see profiles you have visited'
				/>
			) : (
				iwatched.map(iwatch => {
					const {
						firstName,
						lastName,
						age,
						country,
						education,
					} = iwatch.userData.public;
					const { _id } = iwatch.userData;
					const { eventDate } = iwatch;
					return (
						<ActivityCard
							firstName={firstName}
							lastName={lastName}
							age={age}
							country={country}
							education={education}
							eventDate={eventDate}
							_id={_id}
						/>
					);
				})
			)}
		</Container>
	);
};

const mapStateToProps = state => {
	return {
		relationship: state.relationship,
		activities: state.activities,
	};
};

const mapDispatchToProps = dispatch => ({
	fetchActivities: activityType => dispatch(fetchActivities(activityType)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(IWatchProfiles);
