// react
import React, { useEffect } from 'react';

import { connect } from 'react-redux';

import Container from '../ContentPageStyled';
import DefaultContent from '../../../Activity/ContentPages/Default/DefaultContent';
import ActivityCard from '../../../../containers/ActivityCard/ActivityCard';
import theme from '../../../../theme/theme';

import contract from '../../../../util/contract';

//redux
import { fetchActivities } from '../../../../appStore/activity/actions';

const ProfilesInvitedMe = props => {
	useEffect(() => {
		props.fetchActivities(contract.activities.invitedme);
	}, []);
	const invitedme = props.activities.invitedme;
	return (
		<Container className='InvitedMe'>
			{invitedme === undefined || invitedme.length === 0 ? (
				<DefaultContent
					icon={theme.icons.activity.invitedme}
					text='Here you will see profiles that invited you'
				/>
			) : (
				invitedme.map(inviteme => {
					const {
						firstName,
						lastName,
						age,
						country,
						education,
					} = inviteme.userData.public;
					const { _id } = inviteme.userData;
					const { eventDate } = inviteme;
					return (
						<ActivityCard
							firstName={firstName}
							lastName={lastName}
							age={age}
							country={country}
							education={education}
							eventDate={eventDate}
							_id={_id}
						/>
					);
				})
			)}
		</Container>
	);
};

const mapStateToProps = state => {
	return {
		relationship: state.relationship,
		activities: state.activities,
	};
};

const mapDispatchToProps = dispatch => ({
	fetchActivities: activityType => dispatch(fetchActivities(activityType)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(ProfilesInvitedMe);
