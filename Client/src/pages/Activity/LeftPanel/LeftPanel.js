import Container from "./LeftPanelStyled";
import React from "react";

import { connect } from "react-redux";

// React Router
import { withRouter, Route, Switch, NavLink } from "react-router-dom";

const LeftPanel = props => {
   return (
      <Container className="LeftPanel">
         <div className="leftPanelHeader">Activity</div>
         <div className="leftPanelContent">
            <NavLink
               to="/activity/watchedme"
               className="link"
               activeClassName="active"
               isActive={
                  (match, location) => location.pathname === "/activity" || location.pathname === "/activity/watchedme" //set default color
               }
            >
               <div className="icon">
                  <i className="fas fa-eye" />
               </div>
               <div className="activityItem">Visited me</div>
            </NavLink>
            <NavLink to="/activity/iwatched" className="link" activeClassName="active">
               <div className="icon">
                  <i className="far fa-eye" />
               </div>
               <div className="activityItem">I Visited</div>
            </NavLink>
            <NavLink to="/activity/favorites" className="link" activeClassName="active">
               <div className="icon">
                  <i className="far fa-heart" />
               </div>
               <div className="activityItem">Favorites</div>
            </NavLink>
            <NavLink to="/activity/invitedme" className="link" activeClassName="active">
               <div className="icon">
                  <i className="fas fa-sign-in-alt" />
               </div>
               <div className="activityItem">Invited me</div>
            </NavLink>
            <NavLink to="/activity/iinvite" className="link" activeClassName="active">
               <div className="icon">
                  <i className="fas fa-sign-out-alt" />
               </div>
               <div className="activityItem">I invite</div>
            </NavLink>
         </div>
      </Container>
   );
};

const mapStateToProps = state => {
   return {
      relationship: state.relationship
   };
};

const mapDispatchToProps = dispatch => ({});

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(LeftPanel);
