import styled from "styled-components";
import Card from "../../../components/Card/Card";
import theme from "../../../theme/theme";

export default styled(Card)`
   height: 100%;
	width: 25vw;
	background: ${theme.main.inherit};
	color: ${theme.main.black};
	margin-bottom: 10px;
	padding: 6px;
	font-weight: initial;
   display:flex;
   flex-direction: column;
   align-items: flex-end;
   justify-content: flex-start;
   position: fixed;
   box-shadow: none;
   border-right: 1px solid ${theme.main.border};

   .leftPanelContent {
      width: 100%;
      height: 100%;
      display: flex;
      flex-direction: column;
      align-items: flex-end;
      justify-content: flex-start;
      font-size: 1.1vw;
      border-right: 1px solid #e9ebee;
         .icon {
            padding: 15px;

         }

      .active {
            .icon {
               color: ${theme.main.hover};
               font-size: 25px;
            }
            .activityItem {
               color: ${theme.main.hover};
            }
         }


   

   .activityItem {
      height: 10%;
      width: 100%;
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      justify-content: center;
      /* border-top: 1px solid ${theme.main.gray} */

      &:hover {
         color: ${theme.main.hover};
         cursor: pointer;
      }
      
   }
   }

   .leftPanelHeader {
      height: 10%;
      width: 100%;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: flex-end;
      color: ${theme.main.primary};
      font-size: 4vh;
      padding: 30px;
      /* background: ${theme.main.primary}; */
   }

   

`;
