import styled from "styled-components";
import theme from "../../../theme/theme";

import MediaQuery from '../../../config/media-query';

export default styled.div`
   width: 100%;
   background: ${theme.main.inherit};
   color: ${theme.main.black};
   margin-bottom: 10px;
   padding: 6px;
   font-weight: initial;
   display: flex;
   flex-direction: column;
   align-items: flex-end;
   justify-content: center;
   padding: 15px;

   @media (max-width: ${MediaQuery.mobile}) {
      padding: 30px;
   }
`;
