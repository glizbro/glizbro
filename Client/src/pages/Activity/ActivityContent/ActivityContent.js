


// react
import React, { useEffect, useState, useCallback } from "react";

import { connect } from 'react-redux';

import Container from './ActivityContentStyled';
import { Switch, Route, withRouter } from "react-router-dom";

//activity pages components
import WatchedMyProfile from '../ContentPages/WatchedMyProfile/WatchedMyProfile';
import IWatchProfiles from '../ContentPages/IWatchProfiles/IWatchProfiles';
import AddedToFavorites from '../ContentPages/AddedToFavorites/AddedToFavorites';
import ProfilesInvitedMe from '../ContentPages/ProfilesInvitedMe/ProfilesInvitedMe';
import IInviteProfiles from '../ContentPages/IInviteProfiles/IInviteProfiles';



const configPages = {
   'WatchedMyProfile': WatchedMyProfile,
   'IWatchProfiles': IWatchProfiles,
   'AddedToFavorites': AddedToFavorites,
   'ProfilesInvitedMe': ProfilesInvitedMe,
   'IInviteProfiles': IInviteProfiles
}

const ActivityContent = props => {
   return (
      <Container className = 'ActivityContent'>
            <Route path="/activity/watchedme" component={WatchedMyProfile} exact />
            <Route path="/activity/iwatched" component={IWatchProfiles} exact />
            <Route path="/activity/favorites" component={AddedToFavorites} exact />
            <Route path="/activity/invitedme" component={ProfilesInvitedMe} exact />
            <Route path="/activity/iinvite" component={IInviteProfiles} exact />
            <Route path="/activity" component={WatchedMyProfile} exact />
            
      </Container>
   );
}



const mapStateToProps = state => {
	return {
		relationship: state.relationship,
	};
};

const mapDispatchToProps = dispatch => ({});

export default withRouter((ActivityContent));