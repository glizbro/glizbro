// react
import React, { useState, useCallback, useEffect } from "react";

import { connect } from "react-redux";

import LeftPanel from "./LeftPanel/LeftPanel";
import Container from "./ActivityStyled";
import ActivityContent from "./ActivityContent/ActivityContent";

import Test from "../../oleg-zevel/test";

const ActivityPage = props => {
   // Declare a new state variable, which we'll call "count"
   const [width, setWidth] = useState(window.innerWidth);
   const [height, setHeight] = useState(window.innerHeight);

   const handleWindowSizeChange = () => {
      setWidth(window.innerWidth);
      setHeight(window.innerHeight);
   };
   useEffect(() => {
      // Update the document title using the browser API
      window.addEventListener("resize", handleWindowSizeChange);
      return () => {
         window.removeEventListener("resize", handleWindowSizeChange);
      }
   });

   

   return (
      <Container className="Activity">
         <div className="leftPanelWrapper">
            { width < 600 ? <Test/> : <LeftPanel />}
         </div>
         <ActivityContent />
      </Container>
   );
};

const mapStateToProps = state => {
   return {
      relationship: state.relationship
   };
};

export default connect(
   mapStateToProps,
   null
)(ActivityPage);
