// react
import React from 'react';

// styled components
import HomeContaniner from './HomeStyled';

const HomePage = props => {
	return (
		<HomeContaniner>
			<div className='section'>
				<div className='logo'>
					<img src='/glizbro-logo.png' alt='' />
				</div>
			</div>
			<hr />
			<div className='section'>
				<h2 className='title'>Who Are We?</h2>
				<div className='content'>
					GLIZBRO is a unique dating platform Allows people to get to know each other
					easily Our goal is to take advantage of modern technology To take out the
					disadvantages that exist in the dating world
				</div>
			</div>
			<hr />
			<div className='section'>
				<h2 className='title'>What do we offer?</h2>
				<div className='cards'>
					<div className='card'>
						<div className='icon'>
							<img src='/fast-service.svg' alt='' />
						</div>
						<div className='header'>
							<h2>Fast Reply</h2>
						</div>
						<div className='text'>
							We force users to be notified Answer Immediately, a user who does not
							respond to a comment can not continue their activity on the site
						</div>
					</div>
					<div className='card'>
						<div className='icon'>
							<img src='/fast-service.svg' alt='' />
						</div>
						<div className='header'>
							<h2>A unique relationship</h2>
						</div>
						<div className='text'>
							Any user can invite or speak to only one user at any given moment
						</div>
					</div>
					<div className='card'>
						<div className='icon'>
							<img src='/fast-service.svg' alt='' />
						</div>
						<div className='header'>
							<h2>100% Real Users</h2>
						</div>
						<div className='text'>
							There are no bots, all users are passing our test, And we reassure
							that every user is real, We think quality and not quantity
						</div>
					</div>
				</div>
			</div>
			<hr />
		</HomeContaniner>
	);
};

export default HomePage;
