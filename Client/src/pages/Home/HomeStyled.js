import styled from 'styled-components';

export default styled.div`
	flex-grow: 1;
	display: flex;
	flex-direction: column;
	align-items: stretch;
	background: rgba(0, 0, 0, 0.95);
	color: #c7d1d8;

	.section {
		padding: 8px;
		display: flex;
		flex-direction: column;
		align-items: center;

		.title {
			padding: 16px 8px;
		}
		.content {
			width: 50%;
			padding: 8px;
			text-align: center;
			font-size: 1.2em;
		}

		.cards {
			display: flex;
			justify-content: space-evenly;
			.card {
				display: flex;
				flex-direction: column;
				align-items: center;
				text-align: center;
				flex-basis: 30%;
				line-height: 1.3;
				.icon {
					width: 75px;
					height: 75px;
					img {
						width: 100%;
						height: 100%;
					}
				}
				.text {
					width: 80%;
				}
			}
		}
	}

	hr {
		width: 90%;
		border: 4px solid white;
		border-radius: 20px;
	}

	.logo {
		width: 25%;

		img {
			width: 100%;
			height: auto;
		}
	}
`;
