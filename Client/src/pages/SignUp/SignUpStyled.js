import styled from 'styled-components';

import MediaQuery from '../../config/media-query';
import theme from '../../theme/theme';

const Page = styled.div`
	flex-grow: 1;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	width: 100%;
	background: url(/static/pic/bg/signup-bg.jpg);
	background-size: cover;
	background-position: center;

	.Form {
		width: 40vw;
		height: 65vh;

		display: flex;
		flex-direction: column;
		justify-content: space-between;
		align-items: center;

		overflow: auto;

		background: rgba(0, 0, 0, 0.9);

		color: ${theme.main.white};

		.successSignup {
			text-align: center;
		}

		.header {
			text-align: center;
		}
		.inputsWrapper {
			flex-grow: 1;
			display: flex;
			flex-direction: column;
			justify-content: space-evenly;
		}
		.error {
			height: 30px;
			background: white;
			color: ${theme.main.error};
			display: flex;
			justify-content: center;
			align-items: center;
			width: 100%;
		}
		.footer {
		}

		.link {
			height: 30px;
			display: flex;
			align-items: center;
			a {
				color: cornflowerblue;
				text-decoration: none;
			}
		}

		@media (max-width: ${MediaQuery.mobile}) {
			width: 100%;
			flex-grow: 1;
			background: rgba(0, 0, 0, 0.9);
		}
	}
`;

export { Page };
