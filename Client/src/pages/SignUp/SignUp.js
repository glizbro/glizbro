// react
import React, { useState, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

// react router
import { Link } from 'react-router-dom';

// redux
import { connect } from 'react-redux';
import { _onChange } from '../../appStore/fields/actions';
import { signupFetch } from '../../appStore/signup/actions';
// signup status types
import { ERROR, LOADDING, SUCCESS } from '../../appStore/signup/reducer';

// config
import userProperties from '../../config/userProperties';

// helpers
import { getInput, selectKeys } from '../../helpers/renderInputs';
import theme from '../../theme/theme';

// validator
import Validator from '../../helpers/validator';

// components
import Card from '../../components/Card/Card';
import Button from '../../components/Button/Button';
import Loader from '../../components/Loader/Loader';

//  styled component
import { Page } from './SignUpStyled.js';

/**
 * field that used on signup form
 */
const signUpInputs = selectKeys(userProperties, [
	'firstName',
	'lastName',
	'email',
	'password',
]);

const propTypes = {
	signupStatus: PropTypes.string.isRequired, // signup status (success,error, waiting for res)
	signUpForm: PropTypes.object.isRequired, // field container
	_onChange: PropTypes.func.isRequired, // field onChange handler
	signupFetch: PropTypes.func.isRequired, // register user ajax
};

const SignUp = ({ _onChange, signUpForm, signupFetch, signupStatus }) => {
	const [error, setError] = useState('');

	const onSubmit = useCallback(() => {
		// validate form
		for (const key of Object.keys(signUpInputs)) {
			const { validations, label } = userProperties[key];
			const value = signUpForm[key];
			for (const test of validations) {
				const err = Validator[test](label, value);
				if (err) return setError(err);
			}
		}
		signupFetch();
	});
	const onChange = useCallback((key, value) => {
		// 'react hooks'
		_onChange(key, value);
		if (error) setError('');
	});

	useEffect(() => {
		if (signupStatus === ERROR) {
			setError('we have problem on server try later pls');
		}
	}, [signupStatus]);

	if (signupStatus === SUCCESS) {
		return (
			<Page className='SignupPage'>
				<Card className='Form'>
					<h3 className='successSignup'>
						Sign up to success verify account and start find yor love
					</h3>
				</Card>
			</Page>
		);
	}

	return (
		<Page className='SignupPage'>
			<Card className='Form'>
				<div className='header'>
					<h3>Sign Up For True Love</h3>
				</div>
				<div className='inputsWrapper'>
					{Object.keys(signUpInputs).map(prop => {
						const { inputType, inputProps } = userProperties[prop];
						const Input = getInput(inputType);
						return (
							<Input
								key={prop}
								color={theme.main.primary}
								value={signUpForm[prop] || ''}
								onChange={onChange}
								{...inputProps}
							/>
						);
					})}
					{error && <div className='error'>{error}</div>}
				</div>
				<div className='footer'>
					{signupStatus === LOADDING ? (
						<Loader />
					) : (
						<Button type='submit' onClick={onSubmit}>
							Sign Up
						</Button>
					)}
				</div>
				<div className='link'>
					<span className='text'>already have account? </span>
					<Link to='/login'> Log in </Link>
				</div>
			</Card>
		</Page>
	);
};

SignUp.propTypes = propTypes;

const mapStateToProp = state => ({
	signupStatus: state.signup.status,
	signUpForm: state.form.signup,
	signupConfig: state.appConfiguration.signupConfig,
	countries: state.appConfiguration.countries,
});

const mapDispatchToProps = dispatch => ({
	_onChange: (key, value) => dispatch(_onChange(key, value, 'signup')),
	signupFetch: () => dispatch(signupFetch()),
});

export default connect(
	mapStateToProp,
	mapDispatchToProps,
)(SignUp);
