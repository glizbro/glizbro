// listeners and events
import relationship, { relationshipEvents } from './relationship/relationship';
import chat, {chatEvents} from './chat/chat';
import user, {userEvents} from './user/user';

//import it when use want to emit event
export const ioEvents = {
	...relationshipEvents,
	...chatEvents,
	...userEvents
};

// register events
export default socket => {
	relationship(socket);
	chat(socket);
	user(socket);
};

/**
 * @description use for send client events to server
 * @param {String} eventName
 * @param {Object} data
 * @param {Function} callback - run code after server recieve message
 */
export const ioSendEvent = (eventName, data, callback) => {
	window.socket.emit(eventName, data, callback);
};

/**
 *
 * @description use for listen to server events
 * @param {String} eventName
 * @param {Function} listener
 */
export const ioAddListener = (eventName, listener) => {
	window.socket.on(eventName, listener);
};

/**
 * @description use for remove listener of server event
 * @param {String} eventName
 * @param {Function} listener
 */
export const ioRemoveListener = (eventName, listener) => {
	window.socket.removeListener(eventName, listener);
};

//TODO: find better way?
export const clientEvent = {
	GET_USER_STATUS: 'GET_USER_STATUS', // if user online
};

