/**
 *  socket events from server for manage user
 */

//  redux
import store from '../../appStore/Store';

export const userEvents = {
	USER_CONNECT: 'USER_CONNECT',
   USER_DISCONNECT: 'USER_DISCONNECT',
};


export default socket => {
	socket.on(userEvents.USER_CONNECT, received => {
	});

	socket.on(userEvents.USER_DISCONNECT, () => {
	});



};