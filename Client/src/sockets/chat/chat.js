/**
 *  socket events from server for manage relationship
 */

//  redux
import store from '../../appStore/Store';
import {receiveMessage} from '../../appStore/chat/actions';

export const chatEvents = {
	SEND_MESSAGE: 'SEND_MESSAGE',
   GET_ALL_MESSAGES: 'GET_ALL_MESSAGES',
   DELETE_MESSAGE: 'DELETE_MESSAGE',
};


export default socket => {
	socket.on(chatEvents.SEND_MESSAGE, received => {
      store.dispatch(receiveMessage(received.message));
	});

	socket.on(chatEvents.GET_ALL_MESSAGES, () => {
	});

	socket.on(chatEvents.DELETE_MESSAGE, () => {
	});

};