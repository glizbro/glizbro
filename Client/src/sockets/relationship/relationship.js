/**
 *  socket events from server for manage relationship
 */

//  redux
import store from '../../appStore/Store';
import {
	gotInvite,
	acceptInvite,
	inviteAccepted,
	rejectInvite,
	cancelRelationship,
	relationshipCanceled,
} from '../../appStore/relationship/actions';
import { push } from '../../appStore/notifications/actions';

export const relationshipEvents = {
	INVITE: 'INVITE',
	ACCEPT_INVITE: 'ACCEPT_INVITE',
	REJECT_INVITE: 'REJECT_INVITE',
	CANCEL_RELATIONSHIP: 'CANCEL_RELATIONSHIP',
};

export default socket => {
	socket.on('INVITE', user => {
		store.dispatch(gotInvite(user));
		store.dispatch(push({ type: 'invite', user: user }));
	});

	socket.on('REJECT_INVITE', () => {
		store.dispatch(rejectInvite());
	});

	socket.on('ACCEPT_INVITE', () => {
		store.dispatch(inviteAccepted());
	});

	socket.on('CANCEL_RELATIONSHIP', () => {
		store.dispatch(relationshipCanceled());      
	})
};
