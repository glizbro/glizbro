import styled from 'styled-components';
import theme from '../../theme/theme';

// media query
import media from '../../config/media-query';

const Card = styled.div`
	display: flex;
	flex-direction: ${props => (props.row ? 'row' : 'column')};
	align-items: 'center';
	justify-content: space-evenly;
	color: ${theme.main.black};
	margin-bottom: ${props => (props.marginBottom ? props.marginBottom : '0')};
	background: ${props => (props.color ? props.color : theme.main.white)};
   padding: 8px;
	border-radius: 4px;
	box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15), 0 2px 3px rgba(0, 0, 0, 0.2);

	/* mobile */
	@media (max-width: ${media.mobile}) {
		width: 100%;
		padding: 8px;
	}

	@media (max-width: ${media.mobile}) {
			min-width: 250px;
		}
`;

export default Card;
