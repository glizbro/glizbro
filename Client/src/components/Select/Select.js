import React, { useState, useRef } from 'react';
import { SelectStyled, Text, Item, Items } from './SelectStyled.js';

const Select = React.memo(props => {
	const { options, onChange, name } = props;
	const [open, setOpen] = useState(false);
	const [text, setText] = useState(false);
	const selectRef = useRef(null);
	const itemOnClick = value => () => {
		setText(value);
		onChange(name, value);
	};

	let items = options.map(option => (
		<Item key={option} data={1} onClick={itemOnClick(option)}>
			{option}
		</Item>
	));

	window.addEventListener('click', event => {
		if (open && event.target !== selectRef.current) setOpen(false);
	});
	return (
		<SelectStyled
			className='select'
			value={props.value}
			onChange={hanldeChage}
			name={props.name}
		>
			<Text
				ref={selectRef}
				onClick={e => {
					setOpen(!open);
				}}
			>
				{text || props.value}
				{!open ? (
					<i className='fas fa-angle-down' />
				) : (
					<i className='fas fa-angle-up' />
				)}
			</Text>
			<Items open={open}>{items}</Items>
		</SelectStyled>
	);

	function hanldeChage(event) {
		const name = event.target.name;
		const value = event.target.value;
		props.onChange(name, value);
	}
});

Select.defaultProps = {
	options: [],
	onChange: val => console.log('MISSING ONCHANGE CALLBACK', val),
};

export default Select;
