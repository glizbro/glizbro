import styled from 'styled-components';

import theme from '../../theme/theme';

export const SelectStyled = styled.div`
	width: 100%;
	border: 1px solid black;
	background: white;
	color: ${theme.main.fg};
	height: 20px;
	position: relative;
	cursor: pointer;
	display:flex;
	align-items: center;
`;

export const Text = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 20px;
	display: flex;
	flex-direction: row;
	align-items: center;
	padding: 2px 5px;
	justify-content: space-between;
`;

export const Items = styled.div`
	position: relative;
	z-index: 99;
	width: 100%;
	margin-top: 20px;
	max-height: ${props => (props.open ? '200px' : 0)};
	overflow-y: ${props => (props.open ? 'auto' : 'hidden')};
	transition: max-height 0.3s;
`;

export const Item = styled.div`
	width: 100%;
	padding: 8px;
   
	background: ${theme.staticContent.fg};
	height: 30px;
	border: 1px solid white;
	overflow: hidden;
`;
