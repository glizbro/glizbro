
//TODO: refactor why we need it? its container?

import styled from "styled-components";
// theme
import theme from "../../theme/theme";
// animations
import { fadeIn } from '../../components/animations';


const CardSimpleStyled = styled.div`
   width: ${props => (props.width ? props.width : "")};
   height: ${props => (props.height ? props.height : "")};

   display: flex;
   flex-direction: column;
   align-items: center;
   justify-content: center;
   position: relative;
   margin: ${props => (props.margin ? props.margin : "0")};
   background: ${props => (props.background ? props.background : "white")};
   /* border: 1px solid black; */

   
   .Header {
      width: 100%;
      height: 10%;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
    
		color: ${theme.main.fg};
		border-bottom: 1px solid #edeff1;
		border-top: 1px solid #edeff1;

      .text {
         width: 60%;
         display: flex;
         flex-direction: row;
         justify-content: flex-end;
         align-items: center;
		}
		
		.buttonsArea {
			width: 40%;

			display: flex;
			flex-direction: row;
			justify-content: flex-end;
			align-items: center;
		}

		
   }

   .categories {
		width:100%;
		animation:${fadeIn};
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      height: 200px;
      
   }

   
`;

export default CardSimpleStyled;
