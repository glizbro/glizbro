/**
 *  multi select - return array of selected items
 */

import React from 'react';
// styled components
import { SelectStyled, Text, Item, Items } from '../Select/SelectStyled';

// TODO: REFACTOR THIS BULLSHIT
class MultipleSelect extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			values: [],
			open: false,
		};
	}
	render() {
		const { options, name, id, placeHolder } = this.props;
		const {
			renderOption,
			state: { values },
		} = this;
		return (
			<SelectStyled className='multiselect'>
				<Text
					id={id}
					className='selectMain'
					placeholder={placeHolder}
					type='text'
					value={values}
					autoComplete='off'
					onClick={this.showCheckboxes}
				>
					{this.state.values.join(',')}
				</Text>
				<Items
					open={this.state.open}
					className='options'
					ref={node => (this.options = node)}
				>
					{options.map(option =>
						renderOption(`${this.props.id}-${option}`, option, name),
					)}
				</Items>
			</SelectStyled>
		);
	}

	componentDidMount() {
		window.addEventListener('click', this.showCheckboxes);
	}
	componentWillUnmount() {
		window.removeEventListener('click', this.showCheckboxes);
	}
	showCheckboxes = event => {
		const id = this.props.id;
		const target = event.target;
		if (target.classList.contains('multiSelectItem')) return;
		if (target.classList.contains('selectMain')) {
			this.setState({
				open: !this.state.open,
			});
			return event.stopPropagation();
		}

		const open = this.options.classList.contains('open');
		if (target.id === id && !open) {
			this.setState({
				open: true,
			});
		} else {
			this.setState({
				open: false,
			});
		}
	};

	inputChange = event => {
		event.persist();
		const name = event.target.name;
		const { values } = this.state;
		let newValues;

		if (values.includes(name)) {
			const index = values.indexOf(name);
			newValues = [...values.slice(0, index), ...values.slice(index + 1)];
		} else newValues = [...values, name];
		this.setState({ values: newValues });

		// update parent
		if (this.props.onChange) {
			this.props.onChange(this.props.name, newValues);
		}
	};

	renderOption = (id, option, name) => (
		<Item className='option' key={option}>
			<input
				id={id}
				checked={this.state.values.includes(option)}
				onChange={this.inputChange}
				type='checkbox'
				name={option}
				className='multiSelectItem'
			/>
			<label htmlFor={id} className='multiSelectItem'>
				{' '}
				{option}{' '}
			</label>
		</Item>
	);
}

export default MultipleSelect;
