import React from 'react';

import FileUploadStyled from './FileUploadStyled';

const FileUpload = ({ name, onChange, children }) => {
	return (
		<FileUploadStyled className='FileUpload' htmlFor={name}>
			<input
				type='file'
				id={name}
				onChange={e => {
					onChange(name, e.target.files[0]);
					// clear value after change
					e.target.value = '';
				}}
			/>
			{children}
		</FileUploadStyled>
	);
};

export default FileUpload;
