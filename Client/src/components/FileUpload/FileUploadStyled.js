import styled from 'styled-components';

const FileUploadStyled = styled.label`
	cursor: pointer;

	input {
		display: none;
	}
`;

export default FileUploadStyled;
