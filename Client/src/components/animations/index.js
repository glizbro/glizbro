import { css, keyframes } from 'styled-components';

const fadeInFrames = keyframes`
   0% {
      opacity: 0;
   }

   100% {
      opacity: 1;
   }
`;

export const fadeIn = props => css`
	${fadeInFrames};
	animation-duration: 1s;
`;

const slideLeft = keyframes`
   0% {
      left: -240px;
   }

   100% {
      left: 30;
   }
`;

const slideRight = keyframes`
   0% {
      left: 30px;
   }

   100% {
      left: -280px;
   }
`;

const down = (start, end) => {
	return keyframes`
      0% {
         bottom: ${start};
      }

      100% {
         bottom: ${end};
      }
   `;
};

export const downLine = (start, end) => {

	return css`
		${down(start, end)};
		animation-duration: 0.5s;
		animation-fill-mode: forwards;
		animation-delay: 0.5s;
	`;
};

export const slideIn = props => css`
	${slideLeft};
	animation-duration: 0.4s;
	animation-fill-mode: forwards;
`;
export const slideOut = props => css`
	${slideRight};
	animation-duration: 0.6s;
	animation-fill-mode: forwards;
`;

/**
 * Required:
 *
 * name: ${fadeInFrames}
 * animation-duration: 4s;
 *
 * Optional:
 *
 * animation-delay: 2s; -> (negative value (-1s)= how mutch to skip )
   animation-iteration-count: number | infinite; -> times an animation should run
   animation-direction: normal | reverse | alternate | alternate-reverse
 * 
 *
 */
