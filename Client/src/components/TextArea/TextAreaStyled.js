import styled from 'styled-components';

const TextAreaStyled = styled.textarea`
	width: ${props => (props.width ? props.width : '100%')};
	height: ${props => (props.height ? props.height : '100%')};
	/* resize: horizontal; */
	max-width: 100%;
	max-height: 150px;
	margin-bottom: ${props => (props.marginBottom ? props.marginBottom : '0')};
	background: white;
`;

export default TextAreaStyled;
