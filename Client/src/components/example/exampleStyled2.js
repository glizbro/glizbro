import styled from "styled-components";
import theme from '../../theme/theme';

export default styled.div`
   position: relative;
   width: 100px;
   height: 50px;
   margin-left: auto;
   margin-right: auto;
   /* margin-top: 6vh; */
   overflow: hidden;
   border: 2px solid ${props => props.theme.palette.primary};
   font-family: "Lato", sans-serif;
   font-weight: 300;
   font-size: 20px;
   transition: 0.5s;
   letter-spacing: 1px;
   background: ${props => props.theme.palette.primary}; /* background color after hover */
   

   button {
      width: 101%;
      height: 100%;
      font-family: "Lato", sans-serif;
      font-weight: 300;
      font-size: 20px;
      letter-spacing: 1px;
      background: ${props => props.theme.palette.primary};
      -webkit-mask: url("https://raw.githubusercontent.com/robin-dela/css-mask-animation/master/img/nature-sprite.png");
      mask: url("https://raw.githubusercontent.com/robin-dela/css-mask-animation/master/img/nature-sprite.png");
      -webkit-mask-size: 2300% 100%;
      mask-size: 2300% 100%;
      border: none;
      color: ${props => props.theme.palette.white};
      cursor: pointer;
      -webkit-animation: ani2 0.7s steps(22) forwards;
      animation: ani2 0.7s steps(22) forwards;


      
   }
   button:hover {
      -webkit-animation: ani 0.7s steps(22) forwards;
      animation: ani 0.7s steps(22) forwards;
      
   }
   .mas {
      position: absolute;
      color: ${props => props.theme.palette.hover}; /* text color after hover*/
      text-align: center;
      width: 101%;
      font-family: "Lato", sans-serif;
      font-weight: 300;
      position: absolute;
      font-size: 20px;
      margin-top: 12px;
      overflow: hidden;
      
   }
   @-webkit-keyframes ani {
      from {
         -webkit-mask-position: 0 0;
         mask-position: 0 0;
      }
      to {
         -webkit-mask-position: 100% 0;
         mask-position: 100% 0;
      }
   }
   @keyframes ani {
      from {
         -webkit-mask-position: 0 0;
         mask-position: 0 0;
      }
      to {
         -webkit-mask-position: 100% 0;
         mask-position: 100% 0;
      }
   }
   @-webkit-keyframes ani2 {
      from {
         -webkit-mask-position: 100% 0;
         mask-position: 100% 0;
      }
      to {
         -webkit-mask-position: 0 0;
         mask-position: 0 0;
      }
   }
   @keyframes ani2 {
      from {
         -webkit-mask-position: 100% 0;
         mask-position: 100% 0;
      }
      to {
         -webkit-mask-position: 0 0;
         mask-position: 0 0;
      }
   }
`;
