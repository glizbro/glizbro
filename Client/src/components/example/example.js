import ExampleStyled from './exampleStyled';
import React from 'react';

const ButtonExample = ({ props, children, ...rest }) => {
	return (
		<ExampleStyled className='container'>
			<button className='myButt one'>
				<div className='insider' />
				{children}
			</button>
		</ExampleStyled>
	);
};

export default ButtonExample;
