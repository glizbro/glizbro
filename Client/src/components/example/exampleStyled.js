import styled from 'styled-components';
import theme from '../../theme/theme';

export default styled.main`

   background: gray;

   font-family: Helvetica, sans-serif;
   /* color: #f44336; */
   color:black;
   cursor: unset;
   width: 100%;
   height: 100%;
   

   .myButt {
      font-weight: bold;
      width: 100%;
      height: 100%;
      cursor: unset;
      outline: none;
      border: none;
      /* padding: 20px; */
      display: block;
      /* margin: 50px auto; */
      /* font-size: 20px; */
      background-color: transparent;
      position: relative;
      border: 2px solid #fff;
      transition: all 0.5s ease;
      -webkit-transition: all 0.5s ease;
      -moz-transition: all 0.5s ease;
      -o-transition: all 0.5s ease;
      -ms-transition: all 0.5s ease;

      .link {
            color: ${props => props.theme.main.black};
            text-decoration: none;
            text-decoration-line: none;
            font-weight: bold;
            width: 100%;
            height: 100%;

            .selected {
               color: red;
               width: 100%;
               height: 100%;
            }

            &:hover {
               color: ${props => props.theme.main.hover};
               /* text-decoration: underline; */
               text-decoration: none;
            }
         }
   }
   .one {
      border-color: #fff;
      overflow: hidden;
      color: white;
   }
   .one .insider {
      background-color: #fff;
      width: 100%;
      height: 20px;
      position: absolute;
      left: -400px; /* start animation position on hover*/
      transform: rotateZ(45deg);
      -webkit-transform: rotateZ(45deg);
      -moz-transform: rotateZ(45deg);
      -o-transform: rotateZ(45deg);
      -ms-transform: rotateZ(45deg);
   }
   .one:hover {
      background-color: ${props => props.theme.main.gray};
      border-color: #fff;
      color: black;
   }
   .one:hover .insider {
      transition: all 0.3s ease;
      -webkit-transition: all 0.3s ease;
      -moz-transition: all 0.3s ease;
      -o-transition: all 0.3s ease;
      -ms-transition: all 0.3s ease;
      left: 400px; /* end animation position on hover*/
   }
`;
