import ExampleStyled from './exampleStyled2';
import React from 'react';

const ButtonExample2 = ({ children, ...rest }) => {
	return (
		<ExampleStyled className='button-container-1' {...rest}>
			<span className='mas'>{children}</span>
			<button id='work' type='button' name='Hover' disabled={rest.disabled}>
				{children}
			</button>
		</ExampleStyled>
	);
};

export default ButtonExample2;
