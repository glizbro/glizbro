import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

const Root = styled.div`
	position: relative;
	width: 100%;

	input[type='range'] {
		-webkit-appearance: none;
		pointer-events: none;
		position: absolute;
		width: 100%;
	}
	input[type='range']:focus {
		outline: none;
	}
	input[type='range']::-webkit-slider-runnable-track {
		width: 100%;
		height: 8.4px;
		cursor: pointer;
		animate: 0.2s;
		box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
		background: #3071a9;
		border-radius: 1.3px;
		border: 0.2px solid #010101;
	}
	input[type='range']::-webkit-slider-thumb {
		pointer-events: all;
		position: relative;
		z-index: 1;
		box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
		border: 1px solid #000000;
      border-radius: 50%;
		height: 16px;
		width: 16px;
		background: #ffffff;
		cursor: pointer;
		-webkit-appearance: none;
		margin-top: -4px;
	}
	input[type='range']:focus::-webkit-slider-runnable-track {
		background: #367ebd;
	}
	input[type='range']::-moz-range-track {
		width: 100%;
		height: 8.4px;
		cursor: pointer;
		animate: 0.2s;
		box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
		background: #3071a9;
		border-radius: 1.3px;
		border: 0.2px solid #010101;
	}
	input[type='range']::-moz-range-thumb {
		pointer-events: all;
		position: relative;
		z-index: 1;
		box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
		border: 1px solid #000000;
		height: 36px;
		width: 16px;
		border-radius: 3px;
		background: #ffffff;
		cursor: pointer;
	}
	input[type='range']::-ms-track {
		width: 100%;
		height: 8.4px;
		cursor: pointer;
		animate: 0.2s;
		background: transparent;
		border-color: transparent;
		border-width: 16px 0;
		color: transparent;
	}
	input[type='range']::-ms-fill-lower {
		background: #2a6495;
		border: 0.2px solid #010101;
		border-radius: 2.6px;
		box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
	}
	input[type='range']::-ms-fill-upper {
		background: #3071a9;
		border: 0.2px solid #010101;
		border-radius: 2.6px;
		box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
	}
	input[type='range']::-ms-thumb {
		pointer-events: all;
		position: relative;
		z-index: 1;
		box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
		border: 1px solid #000000;
		height: 36px;
		width: 16px;
		border-radius: 3px;
		background: #ffffff;
		cursor: pointer;
	}
	input[type='range']:focus::-ms-fill-lower {
		background: #3071a9;
	}
	input[type='range']:focus::-ms-fill-upper {
		background: #367ebd;
	}
`;

class RangeInput extends Component {
	constructor(props) {
		super(props);

		this.val1 = React.createRef();
		this.val2 = React.createRef();
	}
	onInput = e => {
		const { val1, val2 } = this;
		const x = parseInt(val1.current.value, 10);
		const y = parseInt(val2.current.value, 10);
		const min = x <= y ? x : y;
		const max = x >= y ? x : y;
		const result = {
			min: min,
			max: max,
		};
		this.props.onChange(this.props.name, result);
	};
	render() {
      const { onInput } = this;
      const {min, max, step} = this.props;
		return (
			<Root>
				<input
					ref={this.val1}
					name='min'
					step={step}
					max={max}
					min={min}
					onChange={onInput}
					type='range'
					defaultValue={min}
				/>
				<input
					ref={this.val2}
					name='max'
					step={step}
					min={min}
					max={max}
					onChange={onInput}
               type='range'
               defaultValue={max}
				/>
			</Root>
		);
	}
}

RangeInput.propTypes = {
   // value TODO: value not work yet refactor needed
   onChange: PropTypes.func.isRequired,
   name: PropTypes.string.isRequired,
   min: PropTypes.number.isRequired,
   max: PropTypes.number.isRequired,
   step: PropTypes.number,
}

RangeInput.defaultProps = {
   min: 0,
   max: 120,
   step: 1,
   name: 'input without name',
   onChange: () => console.log('ON CHANGE CALLBACK MISSING')
};

export default RangeInput;
