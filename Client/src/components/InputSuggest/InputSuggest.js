import React, { useState, useCallback } from 'react';

import styled from 'styled-components';

const Container = styled.div`
	position: relative;
`;

const InputStyled = styled.input``;

const Suggestions = styled.div`
	max-height: ${props => (props.open ? '120px' : 0)};

	position: absolute;
	left: 0;
	right: 0;
	top: 100%;
	z-index: 1;

	background: white;

	overflow: ${props => (props.open ? 'auto' : 'hidden')};

	transition: max-height 0.4s;
`;

const Suggestion = styled.div`
	height: 30px;

	display: flex;
	align-items: center;

	padding: 0 5px;

	:hover {
		background: purple;
		color: white;
		cursor: pointer;
	}
`;

const data = ['jerusalim', 'petah tikva', 'tel aviv', 'haifa', 'bnei barak'];

const InputSuggest = props => {
	const { onChange, ...rest } = props;

	const [suggestionIsOpen, setSuggestionIsOpen] = useState(false);

	const handleChange = event => {
		const newVal = event.target.value;
		if (newVal) {
			setSuggestionIsOpen(true);
		} else {
			setSuggestionIsOpen(false);
		}
		onChange(props.name, newVal);
	};

	const onItemSelect = useCallback(text => {
		setSuggestionIsOpen(false);
		onChange(props.name, text);
	});

	const suggestions = data.map(item => {
		if (contain(item, props.value)) {
			return (
				<Suggestion key={item} onClick={() => onItemSelect(item)}>
					{item}
				</Suggestion>
			);
		}
	});
	return (
		<Container className='InputSuggest'>
			<InputStyled className='InputStyled' onChange={handleChange} {...rest} />
			<Suggestions className='Suggestions' open={suggestionIsOpen}>
				{suggestions}
			</Suggestions>
		</Container>
	);
};

// check if string contain value
const contain = (str, value) => {
	value = value.toLocaleLowerCase();
	return str.includes(value);
};

export default InputSuggest;
