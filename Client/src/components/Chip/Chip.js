import React from 'react';

import styled from 'styled-components';
import theme from '../../theme/theme';

const Wrapper = styled.div`
	background: crimson;
	color: ${theme.main.white};;
	font-weight: 600;
	margin: 10px;
	border: 1px solid red;
	border-radius: 15px;
	padding: 5px 8px;
	outline: none;
	display: inline-flex;
	justify-content: space-between;
	align-items: center;
	cursor: pointer;
`;

const DeleteIcon = styled.span`
	font-size: 0.8em;
	border: 1px solid red;
	border-radius: 50%;
	width: 20px;
	height: 20px;
	display: flex;
	justify-content: center;
	align-items: center;
	background: red;
	color: ${theme.main.white};;
`;

const Chip = props => {
	const { onDelete, withIcon } = props;
	return (
		<Wrapper {...props}>
			{props.children}
			{withIcon && <DeleteIcon onClick={onDelete}>X</DeleteIcon>}
		</Wrapper>
	);
};

export default Chip;
