import React from 'react';

import Container from './ModalStyled';
import Card from '../Card/Card';

const Modal = ({ children, onClose, config = {} }) => {
	const { hideBackground } = config;
	return (
		<Container className='Modal' hideBackground={hideBackground}>
			<Card className='ModalContent'>
				<span className='closeModal' onClick={onClose}>
					X
				</span>
				{children}
			</Card>
		</Container>
	);
};

export default Modal;
