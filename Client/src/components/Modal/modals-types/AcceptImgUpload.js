/**
 * modal for accept image upload
 * appert after user choose file to upload
 */

// React
import React, { Fragment, useCallback, useState } from 'react';

// Redux
import { connect } from 'react-redux';
import { setImages } from '../../../appStore/user/actions';

// fetch client
import Api from '../../../api';

// Components
import Modal from '../Modal';
import Button from '../../Button/Button';
import FileReader from '../../FileReader/FileReader';

import styled from 'styled-components';

const Container = styled.div`
	width: 300px;
	height: 200px;
	border: 1px solid black;
	background-color: slateblue;
	color: wheat;

	display: flex;
	flex-direction: column;
	justify-content: space-evenly;
	align-items: center;

	.selectedImage {
		width: 100px;
		height: 100px;
		display: flex;
		justify-content: center;
		align-items: center;
		text-align: center;

		img {
			/* max-width: 100%;
			height: auto; */
			width: 100%;
			height: 100%;
		}
	}

	.actionsBar {
		display: flex;
		justify-content: space-evenly;
		width: 100%;
	}
`;

const AcceptImgUpload = props => {
	const { file,setImages } = props;
	const [finishUpload, setFinishUpload] = useState(false);
	const [isLoading, setIsLaoding] = useState(false);
	const [uploadMsg, setUploadMsg] = useState('');
	const handleUpload = useCallback(async () => {
		try {
			setIsLaoding(true);

			const { images } = await Api.aws.s3.uploadFile(file);
			setUploadMsg('success upload');
			setImages(images);
		} catch (err) {
			setUploadMsg('error was on load image... try again never');
		} finally {
			setIsLaoding(false);
			setFinishUpload(true);
		}
	});
	return (
		<Modal onClose={props.closeModal}>
			<Container>
				<div className='selectedImage'>
					{isLoading && 'loading'}
					{!uploadMsg && !isLoading && <FileReader file={file} />}
					{uploadMsg && <div>{uploadMsg}</div>}
				</div>
				<div className='actionsBar'>
					{finishUpload && <Button onClick={props.closeModal}>close</Button>}
					{!finishUpload && (
						<Fragment>
							<Button onClick={handleUpload}>Upload</Button>
							<Button onClick={props.closeModal}>Cancel</Button>
						</Fragment>
					)}
				</div>
			</Container>
		</Modal>
	);
};

export default connect(
	null,
	{ setImages },
)(AcceptImgUpload);
