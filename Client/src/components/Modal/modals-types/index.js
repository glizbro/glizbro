import UploadImageModal from './UploadImageModal';
import AcceptImgUpload from './AcceptImgUpload';

export default { UploadImageModal, AcceptImgUpload };
