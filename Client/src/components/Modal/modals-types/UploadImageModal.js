import React, { useCallback } from 'react';

// Redux
import { connect } from 'react-redux';
import { setImages, setProfileImage } from '../../../appStore/user/actions';
import { openModal } from '../../../appStore/modals/actions';

// base modal component
import Modal from '../Modal';

import styled from 'styled-components';
import FileUpload from '../../FileUpload/FileUpload';

import Api from '../../../api';

import Icon from '../../Icon/Icon';

const Wrapper = styled.div`
	width: 80vw;
	height: 60vh;

	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	.header {
		background: #f5f6f7;
		width: 100%;
		padding: 8px;
		border-bottom: 1px solid lightsteelblue;
	}

	.imageContainer {
		display: flex;
		flex-wrap: wrap;
		flex-grow: 1;
		justify-content: space-evenly;
		align-items: center;
		align-content: flex-start;
		width: 100%;
		overflow-y: auto;

		.ImageItem {
			width: 20vh;
			height: 20vh;
			border: 1px solid #ddd;
			border-radius: 4px;
			padding: 5px;
			margin: 5px;
			position: relative;
			display: flex;
			justify-content: center;
			align-items: center;

			&:hover {
				box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
			}

			img {
				width: 100%;
				height: 100%;
			}

			:hover {
				.buttons {
					display: flex;
				}
			}

			.buttons {
				position: absolute;
				justify-content: space-evenly;
				align-items: center;
				bottom: 0;
				display: flex;
				width: 100%;
				background: darkorchid;
				padding: 3px;
				height: 25px;
				display: none;

				i {
					border-radius: 50%;
					background: white;
					color: darkmagenta;
					font-size: 0.6em;
					padding: 3px;
					cursor: pointer;
					:hover {
						padding: 4px;
					}
				}
			}
		}
	}

	.imageArea {
		width: 200px;
		height: 200px;

		flex-grow: 1;

		img {
			width: 100%;
			height: 100%;
		}
	}
`;

const ImageItem = ({ image, setImages, setProfileImage }) => {
	const url = 'https://glizbro-images-1.s3.amazonaws.com';

	const deleteImage = () => {
		Api.profile
			.post('/deleteImage', { image: image.src })
			.then(respose => {
				setImages(respose.data.images);
			})
			.catch(err => console.log('err', err.message));
	};

	const setAsProfileImgHandler = () => {
		Api.profile
			.post('/setAsProfile', { image: image })
			.then(respose => {
				setProfileImage(respose.data.profileImg);
			})
			.catch(err => console.log('err', err.message));
	};

	return (
		<div className='ImageItem'>
			<img src={`${url}/${image.src}`} alt='' />
			<div className='buttons'>
				<Icon type='trash-alt' onClick={deleteImage} />
				<Icon type='id-card' onClick={setAsProfileImgHandler} />
			</div>
		</div>
	);
};

const ImageList = ({ images, setImages, setProfileImage }) => {
	return images.map(image => (
		<ImageItem
			setImages={setImages}
			setProfileImage={setProfileImage}
			key={image.src}
			image={image}
		/>
	));
};

const UploadImageModal = props => {
	const { setImages, setProfileImage, images = [], openModal } = props;

	const onFileUpload = useCallback((name, file) => {
		openModal({ modalType: 'AcceptImgUpload', modalProps: { file: file } });
	});

	const ModalConfig = {
		hideBackground: true,
	};

	return (
		<Modal onClose={props.closeModal} config={ModalConfig}>
			<Wrapper className='UploadImageModal'>
				<div className='header'>My Photos </div>
				<div className='imageContainer'>
					<ImageList
						images={images}
						setProfileImage={setProfileImage}
						setImages={setImages}
					/>
					<FileUpload name='addImage' onChange={onFileUpload}>
						<div className='ImageItem'>+</div>
					</FileUpload>
				</div>
			</Wrapper>
		</Modal>
	);
};

const mapState = state => ({
	images: state.user.data.images,
});

export default connect(
	mapState,
	{ setImages, setProfileImage, openModal },
)(UploadImageModal);
