import styled from 'styled-components';
import theme from '../../theme/theme';

export default styled.div`
	position: fixed;
	width: 100%;
	height: 100%;
	z-index: 20;
	background: ${props => (props.hideBackground ? 'rgba(0, 0, 0, 0.8)' : 'inherit')};
	top: 0;
	left: 0;
	display: flex;
	justify-content: center;
	align-items: center;

	.ModalContent {
		/* width: 60vw; */
		/* height: 40vh; */

		position: relative;
	}
	.closeModal {
		color: ${theme.main.primary};
		margin: 4px;
		font-size: 1.2em;
		cursor: pointer;

		position: absolute;
		top: 0;
		right: 0;
	}
`;
