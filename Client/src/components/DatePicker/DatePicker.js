/**
 * TODO: refactor it - choose date picker
 */

import React from 'react';

import Select from '../Select/Select';

import Picker from './DatePickerStyled.js';

class DatePicker extends React.Component {
	render() {
		const { props, onChange } = this;

		return (
			<Picker>
				<Select
					value={props.day}
					options={days}
					onChange={onChange}
					name='day'
					dateFormat='dd/MM/yyyy'
				/>
				<Select
					value={props.month}
					options={months}
					onChange={onChange}
					name='month'
					dateFormat='dd/MM/yyyy'
				/>
				<Select
					value={props.year}
					options={years}
					onChange={onChange}
					name='year'
					dateFormat='dd/MM/yyyy'
				/>
			</Picker>
		);
	}

	onChange = (field, val) => {
		this.setState(
			{
				[field]: val,
			},
			() => {
				const { day, month, year } = this.state;
				const result = `${day}/${month}/${year}`;
				this.props.onChange(this.props.name, result);
			},
		);
	};
}

export default DatePicker;

let days = [];
let months = [];
let years = [];

for (let i = 1; i < 32; i++) {
	let x = i;
	if (i < 10) x = '0' + i;
	days.push(x);
}
for (let i = 1; i < 13; i++) {
	let x = i;
	if (i < 10) x = '0' + i;
	months.push(x);
}
for (let i = 1900; i < new Date().getFullYear() + 1; i++) {
	years.push(i);
}
