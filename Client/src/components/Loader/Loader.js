/**
 *  loader component - display spinner when something loading  from server
 */

import React from 'react';
// import PropTypes from 'prop-types';

const propTypes = {};
const defaultProps = {};

const Loader = () => {
	return <div>Loading...</div>;
};

Loader.defaultProps = defaultProps;
Loader.propTypes = propTypes;

export default Loader;
