import styled from 'styled-components';

export default styled.button`
	position: relative;
	overflow: hidden;
	width: 100%;
	font-weight: bold;
	font-size: ${p => p.theme.typography.fontSize[p.size] || p.theme.typography.fontSize.md};
	background-color: ${props => props.theme.palette[props.backgroundColor] || props.theme.palette.primary};
	color: ${props => props.theme.palette[props.color] || props.theme.palette.primary};
	padding: 0;
	outline: none;
	border: 1px solid ${props => props.theme.palette[props.borderColor] || 'none'};
	border-radius: 4px;
	box-shadow: 0 1px 4px gray;
	cursor: pointer;

	:active {
		box-shadow: 0 0px gray;
		transform: translateY(1px);
	}

	.overlay {
		display: flex;
		width: 100%;
		height: 100%;
		justify-content: center;
		align-items: center;
		background: transparent;
		padding: ${props => props.theme.spacing.md};
	}
	.overlay:hover {
		background: rgba(0, 0, 0, 0.25);
	}

	:disabled {
		opacity: 0.4;
		pointer-events: none;
	}
`;

export let Ripple = styled.span`
	/* anim on click */

	background: rgba(0, 0, 0, 0.3);
	position: absolute;
	top: -50px;
	left: -50px;
	bottom: -50px;
	right: -50px;
	opacity: 0;
	transition: all 0.6s;
	border-radius: 4px;

	&:active {
		left: ${props => props.left + '%'};
		top: ${props => props.top + '%'};
		right: ${props => props.right + '%'};
		bottom: ${props => props.bottom + '%'};
		opacity: 1;
		transition: 0s;
	}
`;
