// React
import React, { useState, useEffect, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';

// styled components
import Container, { Ripple } from './ButtonStyled';

const propTypes = {
	color: PropTypes.oneOf(['primary', 'secondary']),
	size: PropTypes.oneOf(['sm', 'md', 'lg']),
};
const defaultProps = {};

const Button = React.memo(({ children, ...rest }) => {
	const [left, setLeft] = useState(0);
	const [right, setRight] = useState(0);
	const [top, setTop] = useState(0);
	const [bottom, setBottom] = useState(0);

	const [btnRight, setBtnRight] = useState(0);
	const [btnLeft, setBtnLeft] = useState(0);
	const [btnTop, setBtnTop] = useState(0);
	const [btnBottom, setBtnBottom] = useState(0);
	const btnRef = useRef(null);

	const onClick = useCallback(event => {
		event.preventDefault()
		const x = event.clientX;
		const y = event.clientY;

		const width = btnRight - btnLeft;
		const height = btnBottom - btnTop;
		const positionX = x - btnLeft;
		const positionY = y - btnTop;

		const percentTop = (100 / height) * positionY;
		const percentBottom = 100 - percentTop;
		const percentLeft = (100 / width) * positionX;
		const percentRight = 100 - percentLeft;

		setRight(percentRight);
		setLeft(percentLeft);
		setTop(percentTop);
		setBottom(percentBottom);
	});

	useEffect(() => {
		var { top, bottom, right, left } = btnRef.current.getBoundingClientRect();
		setBtnRight(right);
		setBtnLeft(left);
		setBtnBottom(bottom);
		setBtnTop(top);
	}, []);

	return (
		<Container {...rest} onMouseDown={onClick} ref={btnRef}>
			<span className="overlay">{children}</span>
			<Ripple className="Ripple" right={right} left={left} top={top} bottom={bottom} />
		</Container>
	);
});

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default Button;
