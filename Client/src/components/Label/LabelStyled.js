
import styled from "styled-components";
import theme from '../../theme/theme';


const LabelStyled = styled.label`
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
        width: ${props => (props.width ? props.width : '100%')};
        height: ${props => (props.height ? props.height : '30px')};
        float: left;
        font-size: 0.8em;
        padding: ${props => (props.padding ? props.padding : '0')};
        color: ${props => (props.color ? props.color : theme.main.primary)};

        & > i {
                margin-right: 5px;
        }

        .labelIcon {
            width: 10%;
            flex-shrink: 0;
        }

        .labelName {
            width: 35%;
            flex-shrink: 0;
        }

        .labelValue {
            width: 55%;
            flex-shrink: 0;
        }
`;

export default LabelStyled;