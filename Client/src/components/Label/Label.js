import React from 'react';

import LabelStyled from './LabelStyled';
import styled from 'styled-components';

const Label = props => {
	return (
		<LabelStyled
			padding={props.padding}
			width={props.width}
			height={props.height}
			color={props.labelNameColor}
		>
			<div className='labelIcon'>{props.icon ? <props.icon /> : null}</div>
			<div className='labelName'>{props.labelName}</div>
			<div className='labelValue'>
				{props.textComponent ? (
					props.textComponent
				) : (
					<Value className='text' color={props.labelValueColor}>
						{props.text}
					</Value>
				)}
			</div>
		</LabelStyled>
	);
};

const Value = styled.div`
	color: ${props => (props.color ? props.color : 'black')};
`;

export default Label;
