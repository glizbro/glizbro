// react
import React from 'react';
import PropTypes from 'prop-types';

// styled comspanonent
import Root, { Header, Info, Image, About } from './PersonalCardStyled';

// faker delete
import faker from 'faker';
import Button from '../../components/Button/Button';

const PersonalCard = props => {
	const { user = {} , constImg} = props;
	const { firstName } = user;
	return (
		<Root className='ProfileCard'>
			<Header className='Header'>
				<Image className='Image'>
					<img
						src={constImg ? 'https://s3.amazonaws.com/uifaces/faces/twitter/fran_mchamy/128.jpg' : faker.image.avatar()}
						alt='personal card'
						width='100'
						height='125'
					/>
				</Image>
			</Header>
			<About className='About'>
				<span className='name'> {firstName}, 27 </span>
			</About>
			<Info>
				<div className='info'>
					<span className='data'>Profile card views:</span>
					<span className='value'>69</span>
				</div>
				<div className='info'>
					<span className='data'>You have been matched:</span>
					<span className='value'>69</span>
				</div>
				<div className='info'>
					<span className='data'>Likes:</span>
					<span className='value'>69</span>
				</div>
				<div className='info'>
					<span className='data'>Called</span>
					<span className='value'>69</span>
				</div>
				<div className='info'>
					<span className='data'>Invited</span>
					<span className='value'>69</span>
				</div>
				<div className='info'>
					<span className='data'>Joined</span>
					<span className='value'>01.01.2000</span>
				</div>
				{firstName === 'Your Match' ? '' : <Button>Edit</Button>}
			</Info>
		</Root>
	);
};

PersonalCard.propTypes = {
	user: PropTypes.shape({
		firstName: PropTypes.string.isRequired,
		lastName: PropTypes.string.isRequired,
		// age: PropTypes.number.isRequired, //  age is number  NOT string
		// city: PropTypes.string.isRequired,
		// country: PropTypes.string.isRequired,
		// image: PropTypes.string.isRequired, // image url
	}).isRequired,
};

export default PersonalCard;
