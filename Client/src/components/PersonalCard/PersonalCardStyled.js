import styled from 'styled-components';

import Card from '../Card/Card';
import theme from '../../theme/theme';

const Root = styled(Card)`
	padding: 2px;
	width: ${props => (props.width ? props.width : '160px')};
	margin: 10px;
`;

const Header = styled.div`
	width: 100%;
	height: 35px;
	background-color: ${theme.main.primary};
	display: flex;
	justify-content: center;
	align-items: self-start;
	padding-top: 10px;
`;

const Image = styled.div`
	width: 85px;
	height: 85px;
	background: black;
	border-radius: 50%;
	img {
		width: 100%;
		height: 100%;
		border-radius: inherit;
	}
`;

const About = styled.div`
	margin-top: 71px;
	display: flex;
	justify-content: center;
	width: 100%;
	font-weight: bold;
	padding: 5px;
	border-bottom: 1px solid rgba(0, 0, 0, 0.15);
	color: ${theme.main.primary};
`;

const Info = styled.div`
	padding: 2px;
	font-size: 0.8em;
	align-items: center;
	width: 100%;
	display: flex;
	flex-direction: column;
	flex-grow: 1;
	justify-content: space-evenly;

	.info {
		width: 100%;
		display: flex;
		justify-content: space-between;
		padding: 10px;
		font-weight: 600;

		.data {
			color: ${theme.main.gray};
			margin-right: 4px;
		}
		.value {
			color: ${theme.main.hover};
		}
	}
`;

// export
export default Root;
export { Header, Info, Image, About };
