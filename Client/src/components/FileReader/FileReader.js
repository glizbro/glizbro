// React
import React, { useEffect, useState } from 'react';

const FileReader = props => {
	const { file } = props;
	const [src, setSrc] = useState(null);

	useEffect(() => {
		const reader = new window.FileReader();
		reader.onload = function(e) {
			setSrc(e.target.result);
		};
		if (file) {
			reader.readAsDataURL(file);
		}
	}, [file]);

	if (!src) return null;
	return <img src={src} alt='' />;
};

export default FileReader;
