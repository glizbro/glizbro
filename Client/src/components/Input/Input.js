import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

// styled componets
import { InputStyled } from './InputStyled';

const propTypes = {
	value: PropTypes.any.isRequired,
	name: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	size: PropTypes.oneOf(['sm', 'md', 'lg']),
};

const defaultProps = {
	value: '',
	onChange: () => {},
};
const Input = props => {
	const onChange = useCallback(event => {
		const name = event.target.name;
		const value = event.target.value;
		props.onChange(name, value);
	});

	return <InputStyled {...props} onChange={onChange} autoComplete='off' />;
};

Input.propTypes = propTypes;
Input.defaultProps = defaultProps;

export default Input;
