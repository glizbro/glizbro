import styled from 'styled-components';

const sizes = {
	sm: '30px',
	md: '35px',
	lg: '40px',
};

const InputStyled = styled.input`
	width: 100%;
	height: ${props => sizes[props.size] || sizes.md};

	background: ${props => props.theme.palette.primary};
	color: ${props => props.theme.palette.secondary};

	padding: ${props => props.theme.spacing.md};

	border-radius: 4px;
	border: 1px solid ${props => props.theme.palette.secondary};

	font-size: ${p => p.theme.typography.fontSize[p.size] || p.theme.typography.fontSize.md};

	:focus {
		outline: none;
	}

	&[type='number']::-webkit-inner-spin-button,
	&[type='number']::-webkit-outer-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
`;

export { InputStyled };
