import React, { memo } from 'react';

import styled from 'styled-components';

const Root = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	width: 25px;
	height: 25px;
	background: ${p => `url('https://glizbro-images-1.s3.amazonaws.com/icons/${p.type}.svg')`};
	background-size: cover;
	background-position: center;
`;

// https://glizbro-images-1.s3.amazonaws.com/icons/edit.svg

const Icon = memo(({ type, ...rest }) => {
	return <Root type={type} />;
});

export default Icon;
