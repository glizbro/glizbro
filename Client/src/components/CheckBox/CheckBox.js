import React from 'react';

import Wrapper from './CheckBoxStyled';

const CheckBox = ({ ...rest }) => {
	return (
		<Wrapper className='CheckBoxWrapper'>
			<input className='CheckBoxInput' type='checkbox' {...rest} />
		</Wrapper>
	);
};

export default CheckBox;
