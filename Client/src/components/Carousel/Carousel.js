// React
import React, { useState } from 'react';

// Components
import Button from '../Button/Button';

// styled componets
import Container, { CarouselItem } from './CarouselStyled';

const Carousel = ({ children, current }) => {
	const totalItems = React.Children.count(children);

	const [picIndex, setPicIndex] = useState(0);

	return (
		<Container className='Carousel'>
			{React.Children.map(children, (child, i) => {
				const open = picIndex % totalItems === i;

				return <Carousel.Item open={open}>{child}</Carousel.Item>;
			})}

			<Button onClick={() => setPicIndex(picIndex - 1)} className='prev'>
				&#10094;
			</Button>
			<Button onClick={() => setPicIndex(picIndex + 1)} className='next'>
				&#10095;
			</Button>
		</Container>
	);
};

Carousel.Item = props => {
	const { children, open } = props;
	return open && <CarouselItem className='CarouselItem'>{children}</CarouselItem>;
};

export default Carousel;
