import styled from 'styled-components';

// animations
import { fadeIn } from '../../components/animations/';

export default styled.div`
	position: relative;
	width: 100%;
	height: 100%;

	.img {
		height: 100%;
		width: 100%;
		background-size: cover;
		background-position: center;
	}

	.next {
		right: 0;
		border-radius: 3px 0 0 3px;
	}
	.prev,
	.next {
		cursor: pointer;
		position: absolute;
		top: 50%;
		width: auto;
		padding: 16px;
		margin-top: -22px;
		color: white;
		font-weight: bold;
		font-size: 18px;
		transition: 0.6s ease;
		border-radius: 0 3px 3px 0;
		user-select: none;
	}
`;

export const CarouselItem = styled.div`
	width: 100%;
	height: 100%;
	animation: ${fadeIn};

	/* set child full width and height */

	> * {
		width: 100%;
		height: 100%;
	}
`;
