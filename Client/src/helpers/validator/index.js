/**
 *  work with string only!!!
 */
import validator from 'validator';

const isEmail = (key, value) => {
	if (typeof value !== 'string') return `${key} not valid email`;
	const isValid = validator.isEmail(value);
	return isValid ? '' : `${key} not valid email`;
};

const isRequired = (key, value) => {
	if (typeof value !== 'string') return `${key} is required`;
	const isValid = !validator.isEmpty(value);
	return isValid ? '' : `${key} is required`;
};

const Validator = {
	isRequired,
	isEmail,
};

export default Validator;
