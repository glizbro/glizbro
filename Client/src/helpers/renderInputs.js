/**
 *  return component reference by type
 */

import Input from '../components/Input/Input';
import Select from '../components/Select/Select';

const config = {
	range: Input, //TODO: create range input
	input: Input,
	radio: Input,
	select: Select,
};

/**
 *
 * @param {String} type
 */
const getInput = type => {
	if (!config[type]) throw new Error(`getInput: type: ${type} not exits`);
	return config[type];
};

/**
 * 
 * @param {Object} target
 * @param {Array} keys
 */
const selectKeys = (target, keys) => {
	const newTarget = {};
	for (const key of keys) newTarget[key] = target[key];
	return newTarget;
};

export { getInput, selectKeys };
