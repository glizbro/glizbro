import styled from "styled-components";

export default styled.div`
   width: 100%;
   display: flex;
   flex-direction: row;
   align-items: center;
   justify-content: center;
   z-index: 100000;
   .container {
      position: "absolute";
      top: 0;
      left: 0;
      z-index: "99";
      opacity: 0.9;
      display: "flex";
      align-items: "center";
      background: "black";
      width: "100%";
      color: "white";
      font-family: "Lobster";
   }

   .logo {
      margin: "0 auto";
   }

   .body {
      display: "flex";
      flex-direction: "column";
      align-items: "center";
      width: "100vw";
      height: "100vh";
      filter: ${props => (props.menuOpen ? "blur(2px)" : null)};
      transition: "filter 0.5s ease";
   }

   @import url("https://fonts.googleapis.com/css?family=Lobster");
   @import url("https://fonts.googleapis.com/css?family=Open+Sans");

   @keyframes appear {
      0% {
         opacity: 0;
      }
      100% {
         opacity: 1;
      }
   }

   @keyframes slideIn {
      0% {
         transform: translateX(-2%);
      }
      100% {
         transform: translateX(0);
      }
   }

   @keyframes shrink {
      0% {
         width: 95%;
      }
      100% {
         width: 90%;
      }
   }
`;
