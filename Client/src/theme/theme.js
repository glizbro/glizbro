// react
import React from 'react';



const theme = {
	main: {
		// fg: 'rgb(205,167,119)',
		fg: 'rgb(23,28,66)', //to delete
		// primary: 'rgb(23,28,66)',
		// primary: 'cornflowerblue',
		primary: '#242424',
		// secondary: 'orangered', //or coral
		secondary: 'coral', //or coral
		inherit: 'inherit',
		error: 'orangered',
		white: 'white',
		gray: '#e9ebee',
		black: 'black',
		ok: 'limegreen',
		hover: 'dodgerblue',
		online: 'green',
		offline: 'indianred',
		text: 'rgba(143, 155, 174, 1)',
		time: '#848484',
		border: '#d0d0d0',
	},
	icons: {
		activity: {
			watchedme: () => <i className='fas fa-eye' />,
			iwatched: () => <i className='far fa-eye' />,
			favorites: () => <i className='far fa-heart' />,
			invitedme: () => <i className='fas fa-sign-in-alt' />,
			iinvite: () => <i className='fas fa-sign-out-alt' />,
		},
	},
	chat: {
		main: 'rgb(229, 221, 213)',
		me: {
			background: 'rgb(220,248,198)',
			color: '#242424'
		},
		friend: {
			background: 'white',
			color: '#242424'
		},
	},
	staticContent: {
		fg: 'gray',
		gb: 'darkslategray',
	},
	success: {
		fg: '#0abb87',
	},
	inherit: {
		fg: 'inherit',
	},
	dark: {
		bg: 'black',
		fg: 'rgb(205, 167, 119)',
	},
};

export default theme;
