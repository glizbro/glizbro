// React
import React from 'react';
import { createPortal } from 'react-dom';

// redux
import { connect } from 'react-redux';
import { closeModal } from '../../appStore/modals/actions';

// modals types - display modal base on export object from index.js file
import modalTypes from '../../components/Modal/modals-types/';

// modal element
const modalRoot = document.getElementById('modal-root');

const ModalContainer = props => {
	const { closeModal } = props;
	const { items } = props.modals;

	const content = items.map((item, i) => {
		const CurrentModal = modalTypes[item.modalType];
		return (
			<CurrentModal key={i} {...item.modalProps} closeModal={() => closeModal(i)} />
		);
	});
	return createPortal(content, modalRoot);
};

const mapState = state => ({
	modals: state.modals,
});

const mapDispath = dispatch => ({
	closeModal: index => dispatch(closeModal(index)),
});

export default connect(
	mapState,
	mapDispath,
)(ModalContainer);
