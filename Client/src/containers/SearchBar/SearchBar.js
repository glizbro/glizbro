// container for search profiles

// React
import React, { useEffect } from 'react';

// redux
import { connect } from 'react-redux';
import { _onChange } from '../../appStore/fields/actions';
import { fetchMatches } from '../../appStore/matches/actions';

// components
import Input from '../../components/Input/Input';
import InputSuggest from '../../components/InputSuggest/InputSuggest';
import CheckBox from '../../components/CheckBox/CheckBox';
import Button from '../../components/Button/Button';

// styled components
import Container, { InputsWrapper, InputsRow } from './SearchBarStyled';

const SearchBar = props => {
	const { form, onChange } = props;
	console.log('form', form);

	useEffect(() => {
		props.fetchMatches();
	}, []);

	const onAgeChange = (name, value) => {
		props.onChange('age', {
			...form.age,
			[name]: value,
		});
	};


	return (
		<Container className='SearchBar'>
			<InputsWrapper>
				<InputsRow className='Row'>
					<h4 className='fieldName'>Age</h4>
					<label htmlFor='ageMin'>from</label>
					<Input
						value={form.age.min}
						name='min'
						onChange={onAgeChange}
						type='number'
						width='30px'
					/>
					<label htmlFor='ageMin'>to </label>
					<Input
						value={form.age.max}
						name='max'
						onChange={onAgeChange}
						type='number'
						width='30px'
					/>
				</InputsRow>
				<InputsRow className='Row'>
					<h4 className='fieldName'>City</h4>
					<InputSuggest
						value={form.city}
						name='city'
						onChange={onChange}
						type='text'
						width='100px'
					/>
					<label htmlFor='online'>online</label>
					<CheckBox name='online' />
				</InputsRow>
				<InputsRow className='Row'>
					<Button onClick={() => props.fetchMatches()}>Find</Button>
				</InputsRow>
			</InputsWrapper>
		</Container>
	);
};

const mapState = state => ({
	form: state.form.searchProfiles,
});

const mapDispatch = dispatch => ({
	onChange: (name, value) => dispatch(_onChange(name, value, 'searchProfiles')),
	fetchMatches: () => dispatch(fetchMatches()),
});

export default connect(
	mapState,
	mapDispatch,
)(SearchBar);
