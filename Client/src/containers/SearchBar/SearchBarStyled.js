import styled from 'styled-components';

import Card from '../../components/Card/Card';
import theme from '../../theme/theme';
// main container
export default styled(Card)`
	height: 10vw;
	width: 30vw;
	background: ${theme.main.primary};
	color: ${theme.main.white};
	margin-bottom: 10px;
	padding: 6px;
	font-weight: initial;
`;

export const InputsWrapper = styled.div`
	display: flex;
	flex-direction: column;
`;

export const InputsRow = styled.div`
	display: flex;
   align-items: center;

   margin: 5px 0;
   .fieldName {
      margin-right: 5px;
   }
	label {
		margin: 0 5px;
	}
`;

export const InputItem = styled.div``;
