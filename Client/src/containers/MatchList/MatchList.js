/**
 * container for display profiles of search result
 */

//React
import React, { useEffect } from 'react';

// Redux
import { connect } from 'react-redux';
import { fetchMatches } from '../../appStore/matches/actions';

// Sub Components
import MatchesCard from './MatchesCard/MatchesCard';

// styled components
import Container from './MatchListStyled';

const MatchList = ({ matches, fetchMatches }) => {
	const matchesList = matches.items;

	useEffect(() => {
		// fetchMatches();
	}, []);
	return (
		<Container>
			{matchesList.map(match => (
				<MatchesCard key={match._id} user={match} />
			))}
		</Container>
	);
};

const mapState = state => ({
	matches: state.matches,
});

const mapDispatch = dispatch => ({
	fetchMatches: () => dispatch(fetchMatches()),
});
export default connect(
	mapState,
	mapDispatch,
)(MatchList);
