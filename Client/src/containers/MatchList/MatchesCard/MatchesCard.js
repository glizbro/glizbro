// react
import React, { useEffect, useState, useCallback } from "react";
import ReactSVG from "react-svg";

// redux
import { connect } from "react-redux";
import { sendInvite } from "../../../appStore/relationship/actions";
import { statuses } from "../../../appStore/relationship/reducer";
import { cancelRelationship } from "../../../appStore/relationship/actions";

//  componets
import Button from "../../../components/Button/Button";
import ButtonExample from "../../../components/example/example";
import ButtonExample2 from "../../../components/example/example2";

// styled components
import Root from "./MatchesCardStyled";

// faker delete
import faker from "faker";

import { NavLink } from "react-router-dom";
import relationship from "../../../sockets/relationship/relationship";

import { ioAddListener, ioEvents, ioSendEvent, clientEvent, ioRemoveListener } from "../../../sockets";

import Icon from "../../../components/Icon/Icon";

const MatchesCard = props => {
   const { user = {}, sendInvite } = props;
   const [isOnline, setIsOnline] = useState(false);

   const onUserConnect = useCallback(newUser => {
      if (user._id === newUser._id) setIsOnline(true);
   });

   const onUserDisconnect = useCallback(newUser => {
      if (user._id === newUser._id) setIsOnline(false);
   });
   useEffect(() => {
      window.socket.on("USER_DISCONECT", id => {
         if (user._id === id) setIsOnline(false);
      });

      window.socket.on("NEW_CONNECT", data => {
         const id = data.id;
         if (user._id === id) {
            setIsOnline(true);
         }
      });

      // get user status
      ioSendEvent(clientEvent.GET_USER_STATUS, user, setIsOnline);
      // add listener for current user connect, after render
      ioAddListener(ioEvents.USER_CONNECT, onUserConnect);
      ioAddListener(ioEvents.USER_DISCONNECT, onUserDisconnect);
      return () => {
         // add listener for current user connect, after unmount
         ioRemoveListener(ioEvents.USER_CONNECT, onUserConnect);
         ioRemoveListener(ioEvents.USER_DISCONNECT, onUserDisconnect);
      };
   }, []);
   const { firstName, lastName, age, city, country, profession, tall } = user;
   const { relationship } = props;
   const { _id, status } = user;
   return (
      <Root
         className="MatchesCard"
         theme={props.theme.main}
         isOnline={isOnline}
         profileImage={"https://cdn2.stylecraze.com/wp-content/uploads/2013/07/Simple-Braid.jpg"}
      >
         <div className="header" />
         <div className="about">
            <div className="topAbout">
               <div className="userDetails">
                  <div className="userStatus">
                     <div className="theUser">
                        <div className="userFlag">
                           <Icon type="flags/il" />
                        </div>
                        <div className="userName">
                           <b>{firstName.toUpperCase()} </b>
                           {lastName.toUpperCase()}
                        </div>
                        <div className="userProffesion">{profession}</div>
                        <div className="userHaracteristics">
                           {arrangeAgeCityTall(age, city, tall).map((item, index, array) =>  {
                              const shouldAddComma = (array.length - index - 1) > 0;
                              console.log('shouldAddComma', shouldAddComma, array.length, index);
                              
                              return (<span>{item}{shouldAddComma ? ', ' : ''}</span>)
                           })}
                        </div>
                     </div>
                  </div>
                  <div className="styledStar">
                     <div className="left" />
                     <div className="mid">
                        <ReactSVG
                           src="https://glizbro-images-1.s3.amazonaws.com/icons/star.svg?type=svg"
                           afterInjection={(error, svg) => {
                              if (error) {
                                 console.error(error);
                                 return;
                              }
                              console.log(svg);
                           }}
                           beforeInjection={svg => {
                              svg.setAttribute("style", "width: 25px;height: 15px; fill: #F34336;");
                           }}
                           evalScripts="always"
                           fallback={() => <span>Error!</span>}
                           loading={() => <span>Loading</span>}
                           renumerateIRIElements={false}
                        />
                     </div>
                     <div className="right" />
                  </div>
               </div>
            </div>
           
         </div>
         <div className="actions">
               {!relationship || relationship.status === "single" ? (
                  <Button color = 'white' backgroundColor = 'hover' borderColor = 'hover'
                     onClick={() => {
                        sendInvite(user);
                     }}
                  >
                     {/* <i className="fa fa-user-plus" /> */}
                     Invite
                  </Button>
               ) : (
                  <Button color = 'fifth' backgroundColor = 'white' borderColor = 'fifth' disabled>
                     {/* <i className="fas fa-user-slash" /> */}
                     Invite
                  </Button>
               )}

               <Button color = 'hover' backgroundColor = 'white' borderColor = 'hover'>
                  {/* <i className="fa fa-star-o" /> */}
                  Favorites
               </Button>
            </div>
      </Root>
   );
};

// function calculateAge(birthday) {
// 	// birthday is a date
// 	try {
// 		const date = new Date(birthday);
// 		var ageDifMs = Date.now() - date.getTime();
// 		var ageDate = new Date(ageDifMs); // miliseconds from epoch
// 		return Math.abs(ageDate.getUTCFullYear() - 1970);
// 	} catch (e) {
// 		return 0;
// 	}
// }

MatchesCard.propTypes = {};

MatchesCard.defaultProps = {};

const getAge = date => {
   const currentYear = new Date().getFullYear();
   const personYear = date.split("/")[2];
   const age = currentYear - personYear;
   return age;
};

const arrangeAgeCityTall = (age, city, tall) => {

   let array = [];
   console.log(age, city, tall);
   if(!isNaN(getAge(age))) {
      array.push(getAge(age))
   }
   if(!(city === '' || city === undefined)) {
      array.push(city)
   }
   if(!(tall === '' || tall === undefined)) {
      array.push(tall);
   }
   console.log('arrayLength:', array.length);
   return array;
};

const mapState = state => ({
   relationship: state.relationship,
   theme: state.theme
});

const mapDispatch = dispatch => ({
   sendInvite: id => dispatch(sendInvite(id)),
   cancelRelationship: () => dispatch(cancelRelationship())
   // inviteUser: id => dispatch(inviteUser(id)),
   // cancelInviteUser: id => dispatch(cancelInviteUser(id)),
});

export default connect(
   mapState,
   mapDispatch
)(MatchesCard);
