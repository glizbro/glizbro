import styled from "styled-components";
import Card from "../../../components/Card/Card";
export default styled(Card)`
	width: 100%;
	min-width: 350px;
	max-width: 30vw;
	height: 60vh;
	padding: 5px;
	min-height: 600px;
	display: flex;
	margin: 20px;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	/* border: 1px ${props => props.theme.border} solid; */
	background: ${props => props.theme.white};
	border-radius: 4px;
	font-family: 'Open Sans Condensed', sans-serif, sans-serif;
	

		.userStatus {
			width: 100%;
			height: 80%;
			display: flex;
			flex-direction: row;
			align-items: center;
			justify-content: space-between;
			/* padding: 20px; */

			.theUser {
				width: 100%;
				color: #333;
				font-size: 1em;
				padding-bottom: 50px;
				padding-top: 30px;
				.userFlag {
					display: flex;
					flex-direction: row;
					align-items: center;
					justify-content: center;
					padding: 3px;
				}
				.userName {
					font-size: 1.5em;
					padding: 5px;
				}
				.userProfession {
					padding: 5px;
				}
			}
		}

	.header {
		width: 100%;
		height: 60%;
		/* padding: 15px; */
		background-size: cover;
		background-position: center;
		/* background-image: url('https://images.pexels.com/photos/462146/pexels-photo-462146.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'); */
		background-image: url(${props => props.profileImage});
		color: ${props => props.theme.ok};
		display: flex;
		flex-direction: row;
		justify-content: space-between;
	}

	.about {
		width: 100%;
		height: 30%;
		padding-top: 5px;
		display: flex;
		flex-direction: column;
		align-items: flex-start;
    	justify-content: flex-start;
		color: ${props => props.theme.text};

		& > div {
			/* margin: 5px 0px; */
			text-align: center;
		}
		

		.topAbout {
			height: 100%;
			width: 100%;
			display: flex;
			flex-direction: column;
			align-items: flex-start;
			justify-content: start;

			.userDetails {
				width: 100%;
				height: 100%;
				display: flex;
				flex-direction: column;
				align-items: center;
				justify-content: flex-start;
				color: ${props => props.theme.text};
				text-decoration: none;

				.styledStar {
					display: flex;
					flex-direction: row;
					align-items: center;
					justify-content: center;
					width: 100%;
					padding: 5px;
					height: 20%;
					
					.left {
						width: 40%;
						border-bottom: 1px solid gray;

					}

					.mid {
						padding-left: 10px;
						padding-right: 10px;
						color: coral;
					}

					.right {
						width: 40%;
						border-bottom: 1px solid gray;
					}
				}
			}
		}
	}
	.actions {
		width: 100%;
		height: 10%;
		display: flex;
		flex-direction: row;
		align-items: flex-end;
		justify-content: space-evenly;
		/* border-top: 1px ${props => props.theme.border} solid; */

		.buttonDisabled {
			position: relative;
			width: 100px;
			height: 50px;
			margin-left: auto;
			margin-right: auto;
			overflow: hidden;
			font-family: "Lato",sans-serif;
			font-weight: 300;
			font-size: 20px;
			-webkit-transition: 0.5s;
			transition: 0.5s;
			-webkit-letter-spacing: 1px;
			-moz-letter-spacing: 1px;
			-ms-letter-spacing: 1px;
			letter-spacing: 1px;
			background: white;
		}
	}

	
`;
