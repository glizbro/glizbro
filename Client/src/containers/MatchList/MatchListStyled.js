/**
 * container for show matches list
 */

import styled from 'styled-components';

// main container for match cards
export default styled.div`
	width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
	 
`;
