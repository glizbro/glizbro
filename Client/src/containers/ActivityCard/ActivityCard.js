/**
 *  Card for display parnter
 */

import React from "react";

import ActivityCardMain from "./ActivityCardStyled";
import ButtonExample2 from "../../components/example/example2";
import faker from "faker";

// React Router
import { withRouter, Route, Switch, NavLink } from "react-router-dom";

// Redux
import { connect } from "react-redux";

const ActivityCard = props => {
   const { firstName, lastName, age, country, education, eventDate, defaultContent, _id } = props;

   {
      defaultContent == true ? "" : "";
   }
   return (
      <NavLink className="activityNavLink" to={"/profile/" + _id}>
         <ActivityCardMain className="ActivityCardMain">
            <div className="Details">
               <div className="Name">{firstName + " " + lastName}</div>
               <div className="About">
                  <div className="UserData">
                     <div className="Age">{age}</div>
                     <div className="Country">{country}</div>
                     <div className="Education">{education}</div>
                  </div>
                  <div className="EventDate">{eventDate}</div>
               </div>
            </div>

            <div className="MatchPicture">
               <div className="thePerson">
                  <img src={faker.image.avatar()} alt="personal card in profile1" />
               </div>
            </div>
         </ActivityCardMain>
      </NavLink>
   );
};

export default ActivityCard;
