import styled from "styled-components";
import Card from "../../components/Card/Card";
import MediaQuery from "../../config/media-query";

export default styled(Card)`
   height: 20vh;
    width: 100%;
    min-width: 320px;
    max-width: 50vw;
	padding: 5px;
	min-height: 200px;
	display: flex;
	margin: 20px;
	flex-direction: row;
	align-items: center;
	justify-content: center;
	/* border: 1px ${props => props.theme.palette.primary} solid; */
	background: ${props => props.theme.palette.white};
   border-radius: 4px;

   &:hover {
         border: 1px solid ${props => props.theme.palette.hover};
		}
   
   @media (max-width: ${MediaQuery.mobile}) {
      min-width: 295px;
      min-height: 150px;
      margin-left: 0px;
      margin-right: 0px;
   }
	/* box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14),
		0px 2px 1px -1px rgba(0, 0, 0, 0.12); */
	/* box-shadow: ${props =>
      props.isOnline
         ? "0px 0px 20px 11px rgba(45, 204, 89,0.2), 0px 1px 0px 0px rgba(45, 204, 89,0.14), 0px 2px 0px 0px rgba(45, 204, 89,0.12)"
         : "0px 0px 20px 11px rgba(255,0,8,0.2), 0px 1px 0px 0px rgba(249,0,8,0.14), 0px 2px 0px 0px rgba(255,0,8,0.12)"}; */

	.Details {
      width: 60vw;
      height: 20vh;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;

      
      
      
      .Name {
         width: 100%;
         height: 30%;
         display: flex;
         flex-direction: column;
         align-items: center;
         justify-content: center;
         padding: 5px;

         @media (max-width: ${MediaQuery.mobile}) {
            font-size: 2.5vh;
         }
      }
      .About {
         height: 70%;
         width: 100%;
         display: flex;
         flex-direction: column;
         align-items: flex-end;
         justify-content: center;
         padding: 5px;

         @media (max-width: ${MediaQuery.mobile}) {
            font-size: 2vh;
         }

         .UserData {
            padding: 10px;
            width: 50%;
            height: 100%;
            display: flex;
            flex-direction: column;
            align-items: flex-end;
            justify-content: space-between;

            @media (max-width: ${MediaQuery.mobile}) {
               font-size: 2vh;
            }

            .Age {
               height: 25%;
            }

            .Country {
               height: 25%;
            }

            .Education {
               height: 25%;
            }
         }

         .EventDate {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            justify-content: center;
            color: ${props => props.theme.palette.primary};
            padding: 3px;
            font-size: 1.5vh;
         }
      }
   }

   .MatchPicture {
      display: flex;
      flex-direction: row;
      align-items: flex-end;
      justify-content: flex-end;
      width: 150px;
      height: 150px;

      @media (max-width: ${MediaQuery.mobile}) {
         width: 30vw;
         height: 30vw;
      }

      .thePerson {
         display: flex;
         flex-direction: row;
         align-items: center;
         justify-content: flex-end;
         width: 150px;
         height: 150px;

         @media (max-width: ${MediaQuery.mobile}) {
            width: 30vw;
            height: 30vw;
         }

         & > img {
            width: 150px;
            height: 150px;
            @media (max-width: ${MediaQuery.mobile}) {
               width: 30vw;
               height: 30vw;
            }
         }
      }
   }
`;
