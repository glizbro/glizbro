// React
import React from 'react';

// Redux
import { connect } from 'react-redux';
import { uploadImgRequest } from '../../appStore/user/actions';
import { openModal } from '../../appStore/modals/actions';

// Components
import Carousel from '../../components/Carousel/Carousel';
import Button from '../../components/Button/Button';
import Icon from '../../components/Icon/Icon';

// styled components
import { Wrapper, Img } from './ImageViewerStyled';

const ImageViewer = props => {
	const { openModal, images = [] } = props;
	const url = 'https://glizbro-images-1.s3.amazonaws.com';

	return (
		<Wrapper className="ImageViewer">
			<div className="actionsBar">
				<Button onClick={openModal}>upload image</Button>
			</div>
			<div className="content">
				<Carousel>
					{images.map(image => {
						return <Img alt="" key={image.src} src={`${url}/${image.src}`} />;
					})}
				</Carousel>
			</div>
		</Wrapper>
	);
};

const mapState = state => ({
	images: state.user.data.images,
});
const mapDispatch = dispatch => ({
	uploadImgRequest: file => dispatch(uploadImgRequest(file)),
	openModal: () => dispatch(openModal({ modalType: 'UploadImageModal' })),
});

export default connect(
	mapState,
	mapDispatch,
)(ImageViewer);
