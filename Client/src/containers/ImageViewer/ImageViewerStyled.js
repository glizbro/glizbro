import styled from 'styled-components';

import Card from '../../components/Card/Card';

export const Img = styled.div`
	width: 100%;
	height: 100%;
	background: ${p => `url(${p.src})`};
   background-size: cover;
   background-position: center;
`;

export const Wrapper = styled.div`
	width: 100%;
	height: 100%;

	display: flex;
	flex-direction: column;

	.actionsBar {
		height: 50px;

		display: flex;
		align-items: center;
		flex-shrink: 0;

		padding: 0 20px;

		box-shadow: inset -1px -2px 5px darkslategrey;
	}

	.content {
		flex-grow: 1;
	}
`;
