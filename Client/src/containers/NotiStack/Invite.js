// react
import React, { useCallback } from 'react';

// redux
import { connect } from 'react-redux';
import { pop } from '../../appStore/notifications/actions';
import { acceptInvite, rejectInvite } from '../../appStore/relationship/actions';

// styled componets
import styled from 'styled-components';

// components
import Icon from '../../components/Icon/Icon';

const Invite = props => {
	const { user, acceptInvite, rejectInvite, pop } = props;

	const onAccept = useCallback(() => {
		acceptInvite(user);
		pop();
	});
	const onReject = useCallback(() => {
		rejectInvite();
		pop();
	});

	return (
		<Container>
			<div className='avatar' />
			<div className='user'>{user.firstName} Invite You</div>
			<div className='actions'>
				<Icon type='check' className='check' onClick={onAccept} />
				<Icon type='close' className='close' onClick={onReject} />
			</div>
		</Container>
	);
};

const mapDispatch = dispatch => ({
	acceptInvite: () => dispatch(acceptInvite()),
	rejectInvite: () => dispatch(rejectInvite()),
	pop: () => dispatch(pop()),
});
export default connect(
	null,
	mapDispatch,
)(Invite);

const Container = styled.div`
	display: flex;
	width: 100%;
	justify-content: space-around;
	align-items: center;

	.avatar {
		background: url('oleg.jpg');
		background-size: cover;
		background-position: center;
		width: 35px;
		height: 35px;
		border-radius: 50%;
	}
	.actions {
		display: flex;

		.check {
			padding: 4px;
			color: green;
			border: 1px solid;
			border-radius: 50%;
			margin: 2px;
		}
		.close {
			padding: 4px;
			color: red;
			border: 1px solid;
			border-radius: 50%;
			margin: 2px;
		}
	}
`;
