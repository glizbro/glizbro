/**
 *  Container for manage notifications
 */
// react
import React, { useReducer, useEffect, useCallback, useRef, useState } from 'react';
import { createPortal } from 'react-dom';

// redux
import { connect } from 'react-redux';
import { pop, push } from '../../appStore/notifications/actions';

// notification types components
import Invite from './Invite';

// styled
import styled from 'styled-components';
import { slideIn, slideOut, downLine } from '../../components/animations/';

const NotiStack = props => {
	const {
		notifications: { messages, doPop, pushed },
		pop,
		push,
	} = props;

	const [isNew, setIsNew] = useState(false);
	return createPortal(
		<Render items={messages} pop={pop} push={push} doPop={doPop} pushed={pushed} />,
		document.querySelector('#notifications'),
	);
};

const Render = ({ items, pop, push, pushed, doPop }) => {
	const r = items.map((item, index) => {
		return (
			<Notification
				key={index}
				className='sda'
				onClick={pop}
				pos={index}
				text={item.text + index}
				remove={index === 0 ? true : false}
				pushed={pushed}
				doPop={doPop}
				isLast={items.length - 1 === index}
				type={item.type}
				user={item.user}
			/>
		);
	});

	return <div>{r}</div>;
};

const Notification = ({ user, pos, doPop, isLast, pushed, onClick, type, ...rest }) => {

	switch (type) {
		case 'invite':
			return (
				<Wrapper
					pos={pos}
					isLast={isLast}
					doPop={doPop}
					pushed={pushed}
				>
					<Invite user={user} />
				</Wrapper>
			);
		default:
			return <Invite />;
	}
	return (
		<Wrapper pos={pos} isLast={isLast} doPop={doPop} pushed={pushed} >
		</Wrapper>
	);
};

const Wrapper = styled.div`
	position: fixed;
	border: 1px solid black;
	bottom: ${props => (props.pos + 1) * 40 + 10 * props.pos + 'px'};
	left: 30px;
	background: rgb(49, 49, 49);
	color: white;
	width: 255px;
	height: 40px;
	border-radius: 4px;
	display: flex;
	align-items: center;
	justify-content: center;
	box-shadow: -1px 4px 3px 1px grey;

	animation: ${props =>
		props.doPop
			? props.pos !== 0
				? downLine(
						(props.pos + 1) * 40 + 10 * props.pos + 'px',
						(props.pos + 1) * 40 + 10 * props.pos - 50 + 'px',
				  )
				: slideOut
			: props.pushed && props.isLast
			? slideIn
			: null};
`;

const mapState = state => ({
	notifications: state.notifications,
});
const mapdispatch = dispatch => ({
	pop: () => dispatch(pop()),
	push: payload => dispatch(push(payload)),
});

export default connect(
	mapState,
	mapdispatch,
)(NotiStack);
