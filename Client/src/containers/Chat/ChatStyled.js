import styled from 'styled-components';
import theme from '../../theme/theme';

export default styled.div`
	width: 100%;
	height: 80vh;
	border: 1px solid;
	display: flex;
	flex-direction: column;
	align-items: stretch;
	min-height: 300px;
	max-height: 1200px;
	overflow: auto;
	background-color: ${theme.chat.main};
	/* position: relative; */
	.header {
		width: 100%;
		min-height: 7vh;
		background-color: ${theme.main.primary};
		color: ${theme.main.white};
		border-bottom: 1px solid;
		display: flex;
		flex-direction: row;
		align-items: stretch;
		top:0px;
		position:sticky;
		.otherUserImage {
			width: 10%;
			height: 100%;
			
			& > img {
				border-radius: 50%;
				height: 40px;
    			width: 40px;
			}
		}

		.personal {
			padding-left: 10px;
			width: 80%;
			height: 100%;
			display: flex;
			flex-direction: column;
			align-items: stretch;
			font-size: 10px;

			.otherUserName {
				font-weight: bold;
				font-size: 15px;
			}
		}

		
		
	}
	.messages {
		background-image: url(/whatsap.png);
		flex-grow: 1;
		display: flex;
		flex-direction: column;
		justify-content: flex-start;
		align-items: flex-end;
		padding: 10px;

		& > div {
			padding: 7px;
			border: 1px solid black;
			background: white;
			border-radius: 5px;
		}

		& > div.me {
			align-self: flex-start;
			background: ${theme.chat.me.background};
			color: ${theme.chat.me.color};
			max-width: 45%;
			margin: 10px;
			

			.content {
				white-space: normal;
    			word-break: break-all;
				
			}
			.currentDate {
				font-size: 10px;
				padding-top: 10px;
			}
		}



		& > div.other {
			background: ${theme.chat.friend.background};
			color: ${theme.chat.friend.color};
			margin: 10px;
			max-width: 45%;
			.content {
				white-space: normal;
    			word-break: break-all;
				 
			}
			.currentDate {
				font-size: 10px;
				padding-top: 10px;
			}
		}
		
	}
	.inputs {
		min-height: 4vh;
		width:100%;
		background-color: ${theme.main.primary};
		color: ${theme.main.white};
		
		display: flex;
		/* position: fixed; */
		bottom:0;
		
		position:sticky;
		/* flex-direction: column-reverse; */
		.messageInput {
			flex-grow: 1;
		}
		.sendBtn {
			
		}
	}
	.actions {
	}
`;
