/**
 * main container for chat
 */

import React, { useState, useRef, useCallback, useEffect } from 'react';
import faker from 'faker';

// redux
import { connect } from 'react-redux';
import { sendMessage, getAllMessages } from '../../appStore/chat/actions';

// styled components
import Wrapper from './ChatStyled';

//components
import Button from '../../components/Button/Button';

const Chat = props => {
	const { messages, sendMessage, user, relationship, getAllMessages } = props;
	const containerRef = useRef(null);
	const [text, setText] = useState('');
	const onSendMessage = useCallback(() => {
		setText('');
		sendMessage(text);
	});

	useEffect(() => {
		getAllMessages(user.data._id);
	}, []);

	useEffect(() => {
		containerRef.current.scrollTop = containerRef.current.scrollHeight;
	});

	if (!relationship.partner) {
		return null;
	}

	return (
		<Wrapper className='ChatWrapper' ref={containerRef}>
			<div className='header'>
				<div className='otherUserImage'>
					<img
						src={
							'https://s3.amazonaws.com/uifaces/faces/twitter/fran_mchamy/128.jpg'
						}
						alt='personal card'
					/>
				</div>
				<div className='personal'>
					<div className='otherUserName'>
						{relationship.partner.firstName + ' ' + relationship.partner.lastName}
					</div>
					<div className='clickForMore'>click here for contact details</div>
				</div>
			</div>
			<div className='messages' key='messages' >
				{messages.map((message, index) => {
					if (message.me)
						return (
							<div className='me' key={index}>
								<div className='content'>{message.content}</div>
								<div className='currentDate'>{message.time}</div>
							</div>
						);
					return (
						<div className='other' key={index}>
							<div className='content'>{message.content}</div>
							<div className='currentDate'>{message.time}</div>
						</div>
					);
				})}
			</div>

			<div className='inputs'>
				<Button className='smileys' type='basic' disabled>
					<i className='fa fa-smile-o' />
				</Button>
				<input
					value={text}
					onChange={e => {
						setText(e.target.value);
					}}
					type='text'
					className='messageInput'
				/>
				<Button
					className='sendBtn'
					type='basic'
					onClick={onSendMessage}
					disabled={text === '' ? true : false}
				>
					<i className='fa fa-paper-plane' />
				</Button>
			</div>
		</Wrapper>
	);
};

const mapState = state => ({
	messages: state.chat.messages,
	user: state.user,
	relationship: state.relationship,
});

const mapDispatch = dispatch => ({
	getAllMessages: userId => dispatch(getAllMessages(userId)),
	sendMessage: data => dispatch(sendMessage(data)),
});
export default connect(
	mapState,
	mapDispatch,
)(Chat);
