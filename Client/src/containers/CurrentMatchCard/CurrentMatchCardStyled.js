import styled from "styled-components";
import Card from "../../components/Card/Card";
import faker from "faker";
import theme from '../../theme/theme';

export default styled(Card)`
   display: flex;
   flex-direction: column;
   align-items:center;
   justify-content: flex-start;
	height: 80vh;
   width: 20%;
   background: ${theme.main.primary};
   color: ${theme.main.white};
   padding-top: 25px;
   position: fixed;

   .Title {
      font-family: 'Didot, serif';
      font-size: 2vw;
      color: white;
      height: 10%;
      width: 100%;
      display: flex;
      flex-direction: column;
      align-items: center;
      
   }

   .SubTitle {
      height: 10%;
      width: 100%;
      display: flex;
      flex-direction: column;
      align-items: center;
      font-size: 1.0vw;
      white-space: normal;
      word-break: normal;
      padding: 20px;
   }

   .MatchPictures {
      height: 30%;
      width: 100%;
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: flex-end;

      .firstPerson {
         padding: 10px;
      }
      .secondPerson {
         padding: 10px;
      }
      img {
         height: 7.5vw;
         width: 7.5vw;
         border-radius: 50%;
      }

      .innerImageTxt {
         font-weight: bold;
         display: flex;
         flex-direction: row;
         align-items: center;
         justify-content: center;
      }
   }

   .Buttons {
      height: 40%;
      width: 100%;
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: flex-end;
   }
`;
