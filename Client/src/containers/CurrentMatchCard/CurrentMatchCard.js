/**
 *  Card for display parnter
 */

import React from "react";

import styled from "styled-components";
import MainCurrentMatch from "./CurrentMatchCardStyled";
import ButtonExample2 from "../../components/example/example2";
import faker from "faker";

import { cancelRelationship } from "../../appStore/relationship/actions";

// Redux
import { connect } from "react-redux";

const CurrentMatchCard = props => {

   const { user, cancelRelationship } = props;
   const { partner } = props.relationship;
   const { profileImg } = props.user.data;
   // const { profileImgPartner } = partner.profileImg;
   //to do add image to partner
   return (
      <MainCurrentMatch>
         <div className="Title">Your Current Match</div>

         <div className="MatchPictures">
            <div className="firstPerson">
               <img
                  src={ profileImg ? `https://glizbro-images-1.s3.amazonaws.com/${profileImg.src}`: "/man.jpg"}
                  alt="personal card in profile1"
               />
            </div>
            <div className="secondPerson">
               <img src={partner ? faker.image.avatar() : "/man.jpg"} alt="personal card in profile2" />
            </div>
         </div>
         <div className="SubTitle">{partner ? `You now chatting with ${partner.firstName}.` : `Not matched yet`}</div>
         <div className="Buttons">
            {partner ? (
               <ButtonExample2
                  onClick={() => {
                     cancelRelationship(user.data);
                  }}
               >
                  Unmatch
               </ButtonExample2>
            ) : (
               ""
            )}
         </div>
      </MainCurrentMatch>
   );
};

const mapState = state => ({
   user: state.user,
   relationship: state.relationship
});

const mapDispatch = dispatch => ({
   cancelRelationship: () => dispatch(cancelRelationship())
});
export default connect(
   mapState,
   mapDispatch
)(CurrentMatchCard);
