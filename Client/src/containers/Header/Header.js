// React
import React from 'react';

//redux
import { connect } from 'react-redux';

// components
import HeaderItem from './HeaderItem';

// styled components
import { Container } from './HeaderStyled';

const Header = props => {
	const { user } = props;

	// header for logedIn users
	const isLogedin = Object.keys(user).length > 0;
	if (!isLogedin) {
		return <Container className="Header" />;
	}
	return <Container className="Header">Glizbro</Container>;
	// return (
	// 	<Container className='Header'>
	// 		<HeaderItem to='/' exact text='Home' />
	// 		<HeaderItem to='/matches' text='Matches' />
	// 		<HeaderItem to='/activity' text='Activity' />
	// 		<HeaderItem to='/profile' text='Profile' />
	// 	</Container>
	// );
};

const mapState = state => ({
	user: state.user.data,
});

const mapDispatch = dispatch => ({});

export default connect(
	mapState,
	mapDispatch,
)(Header);
