import styled from "styled-components";
import theme from "../../theme/theme";
import media from "../../config/media-query";

export const Container = styled.div`
   width: 100%;
   height: 5vh;
   position: fixed;
   z-index: 9999;
   border: 1px solid black;
   overflow: hidden;
   background: ${theme.main.primary};
   color: ${theme.main.white};
   display: flex;
   align-items: center;
   justify-content: center;
   z-index: 100001;
`;

