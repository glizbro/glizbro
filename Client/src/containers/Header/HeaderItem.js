import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

const propTypes = {
	to: PropTypes.string.isRequired,
	text: PropTypes.string.isRequired,
	exact: PropTypes.bool.isRequired,
};

const defaultProps = {
	to: '/',
	text: 'NavLink',
	exact: false,
};
const HeaderItem = ({ to, exact, text }) => {
	return (
		<Container className='HeaderItem'>
			<NavLink to={to} className='link' exact={exact}>
				<span className='hover'>{text}</span>
			</NavLink>
		</Container>
	);
};

HeaderItem.propTypes = propTypes;
HeaderItem.defaultProps = defaultProps;

export default HeaderItem;

export const Container = styled.div`
	margin: 0 10px;

	.link {
		display: flex;
		justify-content: center;
		align-items: center;
		padding: 8px;
		color: #c7d1d8;
		text-align: center;
		text-decoration: none;

		:hover {
			color: #fff;
		}
	}
	.link.active {
		color: #fff;
		border-bottom: 1px solid #fff;
	}
`;
