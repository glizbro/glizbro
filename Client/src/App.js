// react
import React, { Component, Suspense } from 'react';
// react router
import { withRouter, Route, Switch, NavLink } from 'react-router-dom';
// redux
import { connect } from 'react-redux';
import { initAvailableProps } from './appStore/filter/actions';
import { fetchAppConfig } from './appStore/appConfig/actions';

import NotiStack from './containers/NotiStack/NotiStack';
import ModalsContainer from './containers/ModalsContainer/ModalsContainer';
// APP THEME
import { ThemeProvider } from 'styled-components';

// layout component
import { Main, UnauthorizedPages } from './layout';

// containers
import Header from './containers/Header/Header';

// css
import './App.css';

//Helper
import WithUser from './util/WithUser';

// pages
const ProfilePrivate = React.lazy(() => import('./pages/Profile/private/ProfilePrivate'));
const Home = React.lazy(() => import('./pages/Home/Home'));
const Matches = React.lazy(() => import('./pages/Matches/Matches'));
const Activity = React.lazy(() => import('./pages/Activity/Activity'));
const SignUp = React.lazy(() => import('./pages/SignUp/SignUp'));
const Login = React.lazy(() => import('./pages/Login/Login'));
const ProfilePublic = React.lazy(() => import('./pages/Profile/public/ProfilePublic'));

let HomeRoute = props => (
	<WithUser urlToRedirect="/login" shouldUserExist>
		<Home />
	</WithUser>
);
let MatchesRoute = props => (
	<WithUser urlToRedirect="/matches" shouldUserExist>
		<Matches />
	</WithUser>
);
let ActivityRoute = props => (
	<WithUser urlToRedirect="/activity" shouldUserExist>
		<Activity />
	</WithUser>
);
let LoginRoute = props => (
	<WithUser urlToRedirect="/home">
		<Login />
	</WithUser>
);
let SignupRoute = props => (
	<WithUser urlToRedirect="/login">
		<SignUp />
	</WithUser>
);
let ProfilePrivateRoute = props => (
	<WithUser urlToRedirect="/login" shouldUserExist>
		<ProfilePrivate />
	</WithUser>
);

let ProfilePublicRoute = props => (
	<WithUser urlToRedirect="/login" shouldUserExist>
		<ProfilePublic {...props} />
	</WithUser>
);

class App extends Component {
	state = {
		loaded: false,
	};

	render() {
		const { user } = this.props;

		if (user.isLoading) return 'loader';
		return (
			<ThemeProvider theme={this.props.theme}>
				<div className="App">
					<Header />
					<Main className="Main">
						<Suspense fallback={'hi'}>
							<Switch>
								<Route path="/login" render={LoginRoute} />
								<Route path="/signup" render={SignupRoute} />
								{/* <Main className="Main"> */}
								{/* <Route path="/profile" render={ProfilePrivateRoute} exact />
								<Route path="/profile/:id" render={ProfilePublicRoute} exact />
								<Route path="/matches" render={MatchesRoute} exact />
								<Route path="/activity" component={ActivityRoute} /> */}
								{/* <Route path="(/|/home)" render={HomeRoute} exact /> */}
								<Route path="/" render={MatchesRoute} exact />
							</Switch>
						</Suspense>
					</Main>
					<ModalsContainer />
					<NotiStack />
				</div>
			</ThemeProvider>
		);
	}
	componentDidMount() {
		this.props.fetchAppConfig();
	}

	countriesToArray(countriesRes) {
		var countries = [];
		countriesRes.map(countryObject => {
			return countries.push(countryObject.name);
		});
		return countries;
	}
}

const Page404 = ({ location }) => (
	<div id="error">
		<h1 className="notFoundTitle">Oops! That page can’t be found.</h1>
		<p className="notFoundDesc">
			It looks like nothing was found at this location. Maybe try one of the links in the menu or
			press back to go to the previous page.
			<code>{location.pathname}</code>
		</p>
	</div>
);

const mapDispatchToProps = dispatch => {
	return {
		fetchAppConfig: () => dispatch(fetchAppConfig()),
		initAvailableProps: props => dispatch(initAvailableProps(props)),
	};
};

const mapStateToProps = state => {
	return {
		theme: state.theme,
		user: state.user,
	};
};

export default withRouter(
	connect(
		mapStateToProps,
		mapDispatchToProps,
	)(App),
);
