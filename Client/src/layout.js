import styled from 'styled-components';
import theme from './theme/theme';

const Header = styled.div`
	position: fixed;
	width: 100%;
	border: 1px solid black;
	height: 8vh;
	background: #000;
	color: ${theme.main.primary};
	display: flex;
	flex-direction: row;
	justify-content: center;
	padding-top: 1%;
	padding-bottom: 1%;

	.nav {
		display: flex;
		flex-direction: row;
		justify-content: space-evenly;
		width: 50%;
	}
`;

const Main = styled.div`
	margin-top: 5vh;
	min-height: 100vh;
	width: 100%;

	display: flex;
	flex-direction: column;

`;

const UnauthorizedPages = styled.div`
	min-height: ${this.props ? '100vh' : '0'};
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	width: 100%;
	height: 100%;
`;

const GlobalContainer = styled.div``;

export { Header, Main, UnauthorizedPages, GlobalContainer };
