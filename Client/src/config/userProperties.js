/**
 * config input -> all app should relay on this config
 * TODO: optional move it to server
 * TODO: optional store in redux
 */

export default {
	firstName: {
		label: 'First Name',
		minVal: 18,
		maxVal: 120,
		inputType: 'input',
		inputProps: {
			placeholder: 'enter your first name',
			label: 'First Name',
			name: 'firstName',
		},
		validations: ['isRequired'],
	},
	lastName: {
		label: 'Last Name',
		minVal: 18,
		maxVal: 120,
		inputType: 'range',
		inputProps: {
			placeholder: 'enter your last name',
			label: 'Last Name',
			name: 'lastName',
		},
		validations: ['isRequired'],
	},
	email: {
		label: 'Email',
		minVal: 18,
		maxVal: 120,
		inputType: 'input',
		inputProps: {
			placeholder: 'enter your email',
			label: 'Email',
			name: 'email',
		},
		validations: ['isRequired', 'isEmail'],
	},
	password: {
		label: 'Password',
		minVal: 18,
		maxVal: 120,
		inputType: 'input',
		inputProps: {
			placeholder: 'enter your password',
			label: 'Password',
			name: 'password',
			type: 'password',
		},
		validations: ['isRequired'],
	},
	age: {
		name: 'age',
		minVal: 18,
		maxVal: 120,
		option: 'age',
		inputType: 'range',
		inputProps: {
			placeholder: 'enter your age',
			label: 'Age',
			name: 'age',
		},
	},

	tall: {
		name: 'tall',
		minVal: 50,
		maxVal: 250,
		step: 5,
		inputType: 'range',
		inputProps: {
			placeholder: 'enter your tall',
			label: 'Tall',
			name: 'tall',
		},
	},

	weight: {
		name: 'weight',
		minVal: 30,
		maxVal: 300,
		step: 5,
		inputType: 'range',
		inputProps: {
			placeholder: 'enter your weight',
			label: 'Weight',
			name: 'weight',
		},
	},

	gender: {
		name: 'gender',
		option: 'genders',
		inputType: 'radio',
		inputProps: {
			placeholder: 'enter your gender',
			label: 'Gender',
			name: 'gender',
		},
	},

	country: {
		name: 'country',
		option: 'countries',
		inputType: 'select',
		inputProps: {
			placeholder: 'enter your country',
			label: 'country',
			name: 'country',
		},
	},

	religion: {
		name: 'religion',
		option: 'religions',
		inputType: 'select',
		inputProps: {
			placeholder: 'enter your religion',
			label: 'Religion',
			name: 'religion',
		},
	},

	language: {
		name: 'language',
		option: 'languages',
		inputType: 'select',
		inputProps: {
			placeholder: 'enter your language',
			label: 'Language',
			name: 'language',
		},
	},

	education: {
		name: 'education',
		option: 'educations',
		inputType: 'select',
		inputProps: {
			placeholder: 'enter your education',
			label: 'Education',
			name: 'education',
		},
	},
};
