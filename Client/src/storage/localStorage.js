



const LocalStorage = {

    set(key, value) {
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem(key, value);
        } 
        else {
            
        }
    },

    get(key) {
        return localStorage.getItem(key);
    }
}


export default LocalStorage;

