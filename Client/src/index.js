// react
import React from 'react';
import ReactDOM from 'react-dom';

// redux
import { Provider } from 'react-redux';
import store from './appStore/Store';
import { fetchUser } from './appStore/user/actions';

// react router
import { BrowserRouter as Router } from 'react-router-dom';

// component
import App from './App';

// socket connec to server
import io from 'socket.io-client';
import registerEvents from './sockets';

// TODO: Find better solution
// swap it when build to heroku
const socket = io('http://localhost:3001'); // dev
// const socket = io(); // prod

registerEvents(socket);

window.socket = socket;

store.dispatch(fetchUser());


ReactDOM.render(
	<Provider store={store}>
		<Router>
			<App />
		</Router>
	</Provider>,
	document.getElementById('root'),
);
