// models
const users = require('./models/users');
const chats = require('./models/chats');
const activity = require('./models/activity');
const appConfiguration = require('./models/appConfiguration');

const db = {
	users,
	chats,
	activity,
	appConfiguration,
};

module.exports.db = db;
