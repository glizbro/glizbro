const { getCollection } = require("../../common/mongoClient");
const { ObjectId } = require("mongodb");
//utils
const  time  = require("../../utils").currentDate;
const  stringTools  = require("../../utils").stringTools;

const users = require("./users");
const collName = "Activity"; //todo: move to config

const activity = {
   getActivities: async (userId, action) => {
      const filter = {
         _id: ObjectId(userId)
      };
      try {
         console.log('camelized:',getCollectionNameByAction(action));
         const coll = await getCollection(getCollectionNameByAction(action));
			const activities = await coll.findOne(filter);
			
         let finalReuslt = [];
         
			if(activities) {
				 for(let i of activities["users"]){
					let userData = await users.getUserById(i._id);
					delete userData.private;
					delete userData.verified;
					finalReuslt.push({ userData: userData, eventDate: i.eventDate });
				 }
			}
			return finalReuslt;
      } catch (err) {
         console.log('Activity:',err);
         return [];
      }
   },
   saveActivity: async (userId, partnerId, action) => {
      console.log("in save activity", userId, partnerId, action);
      const filter = {
         "_id": ObjectId(userId)
      };
      const setData = {
         $push: {
            users: { _id: partnerId, eventDate: time.getCurrentTimeFull() }
         }
      };
      const options = {
         upsert: true
      };
      const coll = await getCollection(getCollectionNameByAction(action));
      return await coll.updateOne(filter, setData, options);
   }
};

const getCollectionNameByAction = (action) => {
   return `${collName}${stringTools.toCamelCase(action)}`
}



module.exports = activity;
