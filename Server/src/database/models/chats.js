const { getCollection } = require("../../common/mongoClient");
const { ObjectId } = require("mongodb");

const collName = "Chats"; //todo: move to config

const chats = {
   createChat: async chat => {
      const coll = await getCollection(collName);
      return await coll.insertOne(chat);
   },
   getChat: async (firstUserId, secondUserId) => {
      const coll = await getCollection(collName);
      console.log(firstUserId, secondUserId);
      let res = await coll.findOne({
         firstUserId: new ObjectId(firstUserId),
         secondUserId: new ObjectId(secondUserId)
      });
      if (res == null)
         res = await coll.findOne({
            firstUserId: new ObjectId(secondUserId),
            secondUserId: new ObjectId(firstUserId)
         });
         console.log('is null?!',res == null);
      return res;
   },
   saveMessageInChat: async (chatId, messageData) => {
      const filter = {
         _id: ObjectId(chatId)
      };
      const setData = {
         $push: {
            messages: messageData
         }
      };

      const coll = await getCollection(collName);
      await coll.updateOne(filter, setData);
   }
};

module.exports = chats;
