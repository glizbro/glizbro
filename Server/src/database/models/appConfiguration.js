const { getCollection } = require('../../common/mongoClient');
const { ObjectId } = require('mongodb');

const collName = 'AppConfiguration'; //todo: move to config

const appConfiguration = {
	get: async () => {
		const coll = await getCollection(collName);
		return await coll.findOne();
	},
};

module.exports = appConfiguration;
