const { getCollection } = require('../../common/mongoClient');
const { ObjectId } = require('mongodb');

const collName = 'Users'; //todo: move to config

//contract
const { statuses } = require('../../contract');

const users = {
	getUserById: async value => {
		const filter = {
			_id: ObjectId(value),
		};
		const coll = await getCollection(collName);
		return await coll.findOne(filter);
	},
	getUserByEmail: async email => {
		const filter = {
			'private.email': email,
		};
		const coll = await getCollection(collName);
		return await coll.findOne(filter);
	},
	setInviter: async (userId, partner) => {
		const filter = {
			_id: ObjectId(userId),
		};
		const setData = {
			$set: {
				'relationship.status': statuses.inviter,
				'relationship.partner': partner,
			},
		};
		const coll = await getCollection(collName);
		return await coll.updateOne(filter, setData);
	},
	setInvitee: async (userId, partner) => {
		const filter = {
			_id: ObjectId(userId.toString()),
		};
		const setData = {
			$set: {
				'relationship.status': statuses.invitee,
				'relationship.partner': partner,
			},
		};
		const coll = await getCollection(collName);
		return await coll.updateOne(filter, setData);
	},
	rejectInvite: async userId => {
		const filter = {
			_id: ObjectId(userId.toString()),
		};
		const setData = {
			$set: {
				'relationship.status': statuses.single,
				'relationship.partner': null,
			},
		};
		const coll = await getCollection(collName);
		return await coll.updateOne(filter, setData);
	},
	cancelRelationship: async userId => {
		const filter = {
			_id: ObjectId(userId),
		};
		const setData = {
			$set: {
				'relationship.status': statuses.single,
				'relationship.partner': null,
			},
		};
		const coll = await getCollection(collName);
		return await coll.updateOne(filter, setData);
	},
	acceptInvite: async userId => {
		const filter = {
			_id: ObjectId(userId.toString()),
		};
		const setData = {
			$set: {
				'relationship.status': statuses.inRelationship,
			},
		};
		const coll = await getCollection(collName);
		return await coll.updateOne(filter, setData);
	},
	addImage: async (userId, image) => {
		const filter = {
			_id: ObjectId(userId.toString()),
		};
		const coll = await getCollection(collName);
		const update = {
			$push: { images: image },
		};
		const options = {
			returnOriginal: false,
		};
		const result = await coll.findOneAndUpdate(filter, update, options);
		return result.value.images;
	},
	deleteImage: async (userId, imageSrc) => {
		console.log('userId.toString()', userId.toString());
		const filter = {
			_id: ObjectId(userId.toString()),
		};
		const update = {
			$pull: { images: { src: imageSrc } },
		};
		const options = {
			returnOriginal: false,
		};
		const coll = await getCollection(collName);
		const result = await coll.findOneAndUpdate(filter, update, options);
		return result.value;
	},
	setProfileImage: async (userId, image) => {
		const filter = {
			_id: ObjectId(userId.toString()),
		};
		const coll = await getCollection(collName);
		const update = {
			$set: { profileImg: image },
		};
		const options = {
			returnOriginal: false,
		};
		const result = await coll.findOneAndUpdate(filter, update, options);
		return result.value;
	},
};

module.exports = users;
