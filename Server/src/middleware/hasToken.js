
const verifyToken = require('../common/verifyToken');

/**
 * @description Middleware verify token and add decoded on req object
 */
const hasToken = async (req, res, next) => {
	try {
		const token = extractToken(req);
		const decoded = await verifyToken(token);
		req.user = decoded;
		next();
	} catch (err) {
		console.log(`hasToken: auth failed ${err.message}`);
		res.status(401).json({
			err: 'UNAUTHORIZED',
		});
	}
};

module.exports = hasToken;

/**
 * @description extract token from request
 * @dependencies {cookieParser}
 * @param {req} @type {Object}
 * @returns {token}  @type {String}
 */
const extractToken = req => {
	if (req.cookies && req.cookies.token) return req.cookies.token;
	if (req.query.token) return req.query.token;
};
