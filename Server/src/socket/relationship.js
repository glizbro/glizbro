/* server listener for relationship events */

// mongo
const mongoClient = require('../common/mongoClient');
const { ObjectID } = require('mongodb');

const { db } = require('../database');

// config


//utils
const  currentDate  = require('../utils').currentDate;

// contract
const { statuses, activities } = require('../contract');

module.exports = (socket, users) => {
	// check if current user can invite ❌
	// find invited user ✔️
	// check if he can be invited ❌
	// update both user relantionship prop ❌
	// emit event to invited user if online ✔️

	socket.on('INVITE', async partner => {
		try {
			const user = socket.user;
			const invitedUser = await db.users.getUserById(partner._id);
			const partnerData = { ...invitedUser.public, _id: invitedUser._id };

			const userData = await db.users.getUserById(socket.user._id);
			const userDataFiltered = { ...userData.public, _id: userData._id };

			await db.users.setInviter(user._id, partnerData);
         await db.users.setInvitee(invitedUser._id, userDataFiltered);
         

			await db.activity.saveActivity(user._id, partner._id, activities.iinvite);
			await db.activity.saveActivity(partner._id, user._id , activities.invitedme);

			console.log(
				`IO:INVITE - ${user.public.firstName} invite ${invitedUser.public.firstName}`,
			);
			if (!!users[partner._id]) {
				// is online
				users[partner._id].emit('INVITE', { ...user.public, _id: user._id });
			}
		} catch (err) {
			console.log(`IO:INVITE ERROR - ${err}`);
		}
	});

	socket.on('REJECT_INVITE', async () => {
		try {
			console.log('REJECT_INVITE');
			const { user } = socket;
			const userData = await db.users.getUserById(user._id);
			const partnerId = userData.relationship.partner._id;

			await db.users.rejectInvite(user._id);
			await db.users.rejectInvite(partnerId);

			console.log(`IO:REJECT_INVITE - ${user.public.firstName} reject invite `);

			if (!!users[partnerId]) {
				// is online
				users[partnerId].emit('REJECT_INVITE');
			}
		} catch (err) {
			console.log(`IO:REJECT_INVITE ERROR - ${err.message}`);
		}
	});

	// get invited user data ✔️
	// validate if user can accept this request ❌
	// update db of both user relansthion to true ✔️
	socket.on('ACCEPT_INVITE', async () => {
		try {
			const { user } = socket;
			const userData = await db.users.getUserById(user._id);
			const partner = await db.users.getUserById(userData.relationship.partner._id);

			await db.users.acceptInvite(userData._id);
			await db.users.acceptInvite(partner._id);
			console.log('currentDate',currentDate);
			await db.chats.createChat({
				creationDate: currentDate.getCurrentTimeFull(),
				messages: [],
				firstUserId: userData._id,
				secondUserId: partner._id,
			});

			console.log(`IO:ACCEPT_INVITE - ${user.public.firstName} accept  invite`);

			if (!!users[partner._id]) {
				// is online
				users[partner._id].emit('ACCEPT_INVITE');
			}
		} catch (err) {
			console.log(`IO:ACCEPT_INVITE ERROR - ${err.message}`);
		}
	});

	socket.on('CANCEL_RELATIONSHIP', async () => {
		try {
			const { user } = socket;
			const userData = await db.users.getUserById(user._id);
			const partner = await db.users.getUserById(userData.relationship.partner._id);
			console.log(
				`IO:CANCEL_RELATIONSHIP - ${user.public.firstName} cancel ${
					partner.public.firstName
				}`,
			);
			await db.users.cancelRelationship(userData._id);
			await db.users.cancelRelationship(partner._id);
			if (!!users[partner._id]) {
				// is online
				users[partner._id].emit('CANCEL_RELATIONSHIP');
			}
		} catch (err) {
			console.log(`IO:CANCEL_RELATIONSHIP ERROR: ${err.message}`);
		}
	});
};
