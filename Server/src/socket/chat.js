const { db } = require('../database');

const chatEvents = {
	SEND_MESSAGE: 'SEND_MESSAGE',
   GET_ALL_MESSAGES: 'GET_ALL_MESSAGES',
   DELETE_MESSAGE: 'DELETE_MESSAGE',
};

module.exports = (socket, users) => {

   //verify the user and the partner in the same chat id ❌
   //store message in db (add to message to existing messages) ❌
   //send(emit) message to the target user ❌
	socket.on(chatEvents.SEND_MESSAGE, async payload => {
      const sendingUserId = socket.user._id;
      const sendingUserData = await db.users.getUserById(sendingUserId);
      const receivingUserData = { ...sendingUserData.relationship.partner};
      const receivingUserId = receivingUserData._id;
      const chatData = await db.chats.getChat(sendingUserId, receivingUserId)
      //save message to db Chats collection
      let messageData = {
         from: {...sendingUserData.public, _id: sendingUserData._id},
         to: receivingUserData,
         message: payload.content,
         time: payload.time
      }

      console.log('chatData._id,',chatData._id)
      await db.chats.saveMessageInChat(chatData._id, messageData);
      //send the message to client
      console.log(chatEvents.SEND_MESSAGE + ' from user: ' + sendingUserData.public.firstName + '(' + sendingUserId + ')' + ' to user: ' + receivingUserData.firstName + '(' + receivingUserId + ')');
      users[receivingUserId].emit(chatEvents.SEND_MESSAGE, { message: payload });
   });

   //check user allowed to pull from chat id ❌
   //send(emit) messages to the user ❌
   socket.on(chatEvents.GET_ALL_MESSAGES, async userId => {
      console.log(chatEvents.GET_ALL_MESSAGES + ' ' + userId);
   });

   //delete message from chat (at both users) ❌
   socket.on(chatEvents.DELETE_MESSAGE, async userId => {
      console.log(chatEvents.DELETE_MESSAGE + ' ' + userId);
   });

};