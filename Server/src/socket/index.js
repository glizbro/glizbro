const cookieParser = require('cookie-parse');
const verifyToken = require('../common/verifyToken');

/* register listeners by category */
const relationshipListener = require('./relationship');
const chatListener = require('./chat');

// track online users
const users = {};

module.exports = io => {
	io.use(socketAuth);

	// on user connect

	io.on('connection', socket => {
		
		const {
			_id,
			public: { firstName },
		} = socket.user;
		// console.log('conect', _id);
		users[_id] = socket;
		const onlines = Object.keys(users).length;
		
		socket.broadcast.emit('USER_CONNECT', socket.user);
		// console.log(`IO[connection]: ${firstName} connected, online now: ${onlines}`);

		/* register events here */
		relationshipListener(socket, users);
		chatListener(socket, users);

		// on disconnect
		socket.on('disconnect', reason => {
			// console.log('all users', Object.keys(users));
			// console.log('delete', socket.user._id);
			delete users[socket.user._id];
			const onlines = Object.keys(users).length;
			io.emit('USER_DISCONNECT', socket.user);
		});
		// on login
		socket.on('GET_USER_STATUS', (user, callback) => {
			// console.log('Object.keys(users)', Object.keys(users));
			// console.log('users._id', user._id);
			callback(!!users[user._id]);
		});

		socket.on('CHAT_MESSAGE', payload => {
			console.log('CHAT_MESSAGE', JSON.stringify(payload, null, 2));

			socket.broadcast.emit('GET_MESSAGE', {
				content: payload.content,
			});
		});
	});
};

// verify token - create connection only if user has token
async function socketAuth(socket, next) {
	try {
		const { token } = cookieParser.parse(socket.request.headers.cookie);
		const decoded = await verifyToken(token);

		//add user data to socket
		socket.user = decoded;

		next();
	} catch (err) {
		console.log('socketAuth: authentication fail');
		next(new Error('io: unauthorized request'));
	}
}
