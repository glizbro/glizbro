// load variables to process.env, from .env file
const env = require('dotenv').config();

const http = require('http');
const app = require('./app');
const mongoDb = require('./common/mongoClient');

let server;

mongoDb()
	.then(() => {
		server = http.createServer(app);
		// register  socket
		const io = require('socket.io')(server, {});
		require('./socket')(io);

		server.listen(process.env.PORT, () => {
			console.log('server run on port:', process.env.PORT);
		});
	})
	.catch(err => {
		console.log('failed to connect to mongodb:', err, err.message);
	});
