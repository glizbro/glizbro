const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
let addRequestId = require('express-request-id')();
var cookieParser = require('cookie-parser');
const path = require('path');

const helmet = require('helmet');

const app = express();
// static files
app.use(cors());
app.use(helmet());
app.use(bodyParser.text({ type: 'text/html' }));
app.use('/', bodyParser.json()); // to support JSON-encoded bodies
app.use(cookieParser());

app.use(
	bodyParser.urlencoded({
		// to support URL-encoded bodies
		extended: false,
	}),
);
app.use(addRequestId);

// midlleware
const hasToken = require('./middleware/hasToken');
// server static
app.use(express.static('public'));

//  register routes
const appConfigApi = require('./api/appConfig/appConfig.route');
const signUpApi = require('./api/signup/signup.route');
const loginApi = require('./api/login/login.route');
const mailServiceApi = require('./api/mailService/mailService.route');
const matchesApi = require('./api/matches/matches.route');
const profileApi = require('./api/profile/profile.route');
const chatApi = require('./api/chat/chat.route');
const activityServiceApi = require('./api/activity/activity.route');

// public api
app.use(`${process.env.BASE_API}/appConfiguration`, appConfigApi);
app.use(`${process.env.BASE_API}/login`, loginApi);
app.use(`${process.env.BASE_API}/signup`, signUpApi);
app.use(`${process.env.BASE_API}/matches`, matchesApi);
app.use(`${process.env.BASE_API}/mail`, mailServiceApi);
// private api
app.use(`${process.env.BASE_API}/profile`, hasToken, profileApi);
app.use(`${process.env.BASE_API}/chat`, hasToken, chatApi);
app.use(`${process.env.BASE_API}/activity`, hasToken, activityServiceApi);

app.get('*', (req, res) => {
	res.sendFile(path.join(__dirname + '/public/index.html'));
});

module.exports = app;
