const JWT = require('jsonwebtoken');


module.exports = token => {
	return new Promise((resolve, rejects) => {
		JWT.verify(token, process.env.JWT_SECRET, (err, decoded) => {
			if (!err) {
				resolve(decoded);
			} else {
				rejects(err);
			}
		});
	});
};
