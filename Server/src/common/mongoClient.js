var MongoClient = require('mongodb').MongoClient;


let connection;

const options = {
	useNewUrlParser: true,
	poolSize: 10,
};

const getConnection = async () => {
	return new Promise((resolve, reject) => {
		if (connection) {
			return resolve(connection);
		}

		MongoClient.connect(process.env.DB_URL, options, (err, client) => {
			if (err) {
				return reject(err);
			}
			connection = client;
			resolve(client);
		});
	});
};

module.exports = getConnection;

module.exports.getCollection = async collName => {
	const client = await getConnection();
	return client.db(process.env.DB_NAME).collection(collName);
};
