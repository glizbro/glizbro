const JWT = require('jsonwebtoken');


function verifyToken(token) {
	return JWT.verify(token, process.env.JWT_SECRET);
}

module.exports.verifyToken = verifyToken;
