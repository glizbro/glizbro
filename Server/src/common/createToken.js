const JWT = require('jsonwebtoken');


module.exports = (payload, expire) => {
	const options = {
		expiresIn: expire || '30d',
	};
	return new Promise((resolve, rejects) => {
		JWT.sign(payload, process.env.JWT_SECRET, options, (err, token) => {
			if (err) return rejects(err);
			return resolve(token);
		});
	});
};
