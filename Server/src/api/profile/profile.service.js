const dal = require('./profile.dal');

const { db } = require('../../database');

const uuid = require('uuid');

module.exports.deleteImage = async (userId, fileName) => {
	// delete from s3
	const data = await dal.deteleImage(fileName);
	// delete from mongo
	const user = await db.users.deleteImage(userId, fileName);
	return user;
};

module.exports.setProfileImage = async (userId, fileName) => {
	// delete from mongo
	const user = await db.users.setProfileImage(userId, fileName);
	return user;
};

module.exports.uploadImage = async (user, type) => {
	try {
		const folder = user._id;
		const file = uuid();
		const key = `${folder}/${file}.png`;

		console.log('key', key);
		const s3Result = await dal.getSignedUrl({
			operation: 'putObject',
			params: { Bucket: 'glizbro-images-1', Key: key, ContentType: type },
		});

		const image = {
			src: key,
		};

		const updatedUser = await db.users.addImage(user._id, image);
		return {
			url: s3Result,
			images: updatedUser,
		};
	} catch (err) {
		console.log('uploadImage service error', err.message);
	}
};

module.exports.saveProfileData = (req, res) => {
	try {
		dal.saveProfileData(req, res);
	} catch (err) {
		res.status(403).send('cant saveProfileData ');
	}
};

module.exports.getPublicUser = async userId => {
	try {
		console.log(`SUCCESS: Get public userid: ${userId}`);
		return await dal.getUserData(userId);
	} catch (err) {
		console.log(`Failed: Get public userid: ${userId} Error msg ${err.message}`);
		throw err;
	}
};
