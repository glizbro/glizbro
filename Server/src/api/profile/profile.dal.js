const mongoDb = require('../../common/mongoClient');
var ObjectId = require('mongodb').ObjectID;

// AWS
const AWS = require('aws-sdk');

// s3 instance
s3 = new AWS.S3({
	accessKeyId: process.env.AMAZON_S3_KEY,
	secretAccessKey: process.env.AMAZON_S3_SECRET,
});

module.exports.deteleImage = fileName => {
	return new Promise((resolve, reject) => {
		var params = {
			Bucket: 'glizbro-images-1',
			Key: fileName,
		};
		s3.deleteObject(params, function(err, data) {
         if (err) return reject(err);
         console.log('data',data)
			return resolve(data);
		});
	});
};

/**
 * @description create url for operation on s3 bucket
 * @param { operation } => one of valid s3 operations
 * @param { params.Bucket } => bucket name on s3
 * @param { params.key } => file name in bucket
 * @returns {url} => url for operation
 */
module.exports.getSignedUrl = ({ operation, params }) => {
	return new Promise((resolve, reject) => {
		s3.getSignedUrl(operation, params, function(err, data) {
			if (err) return reject(err);
			resolve(data);
		});
	});
};

module.exports.getUserData = async userId => {
	try {
		const client = await mongoDb();
		const user = await client
			.db('dnaze')
			.collection('Users')
			.findOne(
				{ _id: new ObjectId(userId) },
				{ projection: { private: 0, verified: 0 } },
			);
		return user;
	} catch (err) {
		console.log(
			`Cant retreive user ${userId} from the DB. Something very bad happend.`,
		);
		throw err;
	}
};

module.exports.saveProfileData = async function(req, res) {
	try {
		const client = await mongoDb();
		//find user
		let user = await client
			.db('dnaze')
			.collection('Users')
			.findOne({ _id: new ObjectId(req.user._id) });

		//prepare user public object to update
		let newUserPublic = userPublicUpdatedWithReqItems(
			user.public,
			req.body.fields.public,
		);
		console.log(1);
		//find the user and update it with the prepared fields
		let userUpdated = await client
			.db('dnaze')
			.collection('Users')
			.findOneAndUpdate(
				{
					_id: new ObjectId(req.user._id),
				},
				{
					$set: {
						public: newUserPublic,
					},
				},
				{
					projection: {
						verified: 0,
						private: 0,
					},
					returnOriginal: false,
				},
			);

		console.log(2);
		// userUpdatedwithoutPrivate = deletePrivateFieldFromObj(userUpdated.value); //delete private object for response

		res.send(userUpdated.value);
		console.log('response sent:', userUpdated.value);
	} catch (e) {
		console.log(e.message, ' cant update user data, from profile edit request.');
		res.sendStatus(400).send('cant update user data, from profile edit request.');
	}
};

const userPublicUpdatedWithReqItems = (originalUser, reqPublicFields) => {
	let newUserPublic = {};
	Object.keys(originalUser).map(field => {
		if (field in reqPublicFields) {
			newUserPublic[field] = reqPublicFields[field];
		} else {
			newUserPublic[field] = originalUser[field];
		}
	});
	return newUserPublic;
};

const deletePrivateFieldFromObj = obj => {
	var userUpdatedwithoutPrivate = Object.assign({}, obj); //delete private object for response
	delete userUpdatedwithoutPrivate.private;
	return userUpdatedwithoutPrivate;
};
