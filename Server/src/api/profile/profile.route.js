const router = require('express').Router();
const mongoClinet = require('../../common/mongoClient');
const ObjectID = require('mongodb').ObjectID;

const createToken = require('../../common/createToken');

const { db } = require('../../database');
const profileService = require('./profile.service');

// contract
const { activities } = require('../../contract');

// set image as profile
router.post('/setAsProfile', async (req, res) => {
	try {
		console.log('setAsProfile', req.user._id, req.body.image);
		const _id = req.user._id;
		const serverResult = await profileService.setProfileImage(_id, req.body.image);

		res.json(serverResult);
	} catch (err) {
		console.log('setProfileImage ERR', err.message);
		res.status(500).send(err.message);
	}
});

//delete image user
router.post('/deleteImage', async (req, res) => {
	try {
		console.log('deleteImage', req.user._id, req.body.image);
		const _id = req.user._id;
		const serverResult = await profileService.deleteImage(_id, req.body.image);
		res.json(serverResult);
	} catch (err) {
		console.log('ERR', err.message);
		res.status(500).send(err.message);
	}
});

// get current user by id ✔️
// refresh token ✔️
router.get('/current', async (req, res) => {
	try {
		const _id = req.user._id;

		const user = await db.users.getUserById(_id);
		const token = await createToken(user);

		res.cookie('token', token, {
			httpOnly: true,
		});
		res.json(user);
	} catch (err) {
		console.log('ERR', err.message);
		res.status(500).send(err.message);
	}
});

//save profile, after edit
router.post('/save', profileService.saveProfileData);

router.get('/:id', async (req, res) => {
	try {
		console.log('user watched me, req.params.id:',req.params.id, 'user i watched, req.user._id:', req.user._id );
		let user = await profileService.getPublicUser(req.params.id);
		res.json(user);
		await db.activity.saveActivity(req.user._id, req.params.id, activities.iwatched);
		await db.activity.saveActivity(req.params.id, req.user._id, activities.watchedme);
		console.log(`SUCCESS: get user ${req.params.id}`);
	} catch (err) {
		console.log(`Cant get user, Error msg: ${err.message}`);
		res.status(400).send('User does not exist in our system.');
	}
});

router.post('/uploadImage', async (req, res) => {
	try {
		console.log('try upload file', req.get('Content-Type'));
		console.log(req.body);
		const serviceResult = await profileService.uploadImage(req.user, req.body.type);
		res.json(serviceResult);
	} catch (err) {
		console.log(`uploadImage error ${err.message}`);
		res.status(500).send(err.message);
	}
});

module.exports = router;
