const router = require("express").Router();

const { db } = require("../../database");

// contract
const { activities } = require("../../contract");

router.get("/:activityType", async (req, res) => {
   try {
      const userIdThatAskedForMessages = req.user._id;
      const finalResult = db.activity.getActivities(userIdThatAskedForMessages, req.params.activityType);
      Promise.all([
         finalResult
      ]).then(results => {
         console.log("Activity results:", results);
         const [activityResponse] = results
         const allUserActivities = {
            [req.params.activityType]: activityResponse
         };
         res.json(allUserActivities);
      });
   } catch (err) {
      console.log("ERR", err);
      res.status(500).send(err.message);
   }
});

module.exports = router;
