const router = require('express').Router();
const { db } = require('../../database');
const createRouter = require('../../utils/functionCreator/createRouter');

createRouter({
	path: '/',
	method: 'get',
	name: 'get app config',
	description: 'load base config for client app',
	service: appConfigurationService,
	router: router,
});

module.exports = router;

// TODO: move to service folder
async function appConfigurationService() {
	try {
		const config = await db.appConfiguration.get();

		return {
			contentConfig: config['contentConfig'],
			homeConfig: config['homeConfig'],
		};
	} catch (err) {}
}
