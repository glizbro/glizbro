
const { validator } = require('express-validator');

//db validations
module.exports.validateCountries = ( requestCounty, appConfigCountries, updateMessageOnError) => {
    var isValid = arrayOfobjectsToArray(appConfigCountries).includes(requestCounty);
    if(isValid == false) {
        updateMessageOnError(`The Country ${requestCounty} does not exist.`);
    }
    return isValid;
}


module.exports.validateGenders = ( requestGender, signupConfigGenders, updateMessageOnError) => {
    var isValid = signupConfigGenders.includes(requestGender);
    if(isValid == false) {
        updateMessageOnError(`The Gender ${requestGender} does not exist.`);
    }
    return isValid;
}

module.exports.validateCharacters = ( requestCharacters, signupConfigCharacters, updateMessageOnError ) => {
    var isValid = includesAll(requestCharacters, signupConfigCharacters);
    if(isValid == false) {
        updateMessageOnError(`One of the Characters ${requestCharacters} does not exist.`);
    }
    return isValid;
}

module.exports.validateInterests = ( requestInterests, signupConfigInterests, updateMessageOnError ) => {
    var isValid = includesAll(requestInterests, signupConfigInterests);
    if(isValid == false) {
        updateMessageOnError(`One of the Interests ${requestInterests} does not exist.`);
    }
    return isValid;
}

module.exports.validateEducations = ( requestEducation, signupConfigEucations, updateMessageOnError ) => {
    var isValid = signupConfigEucations.includes(requestEducation);
    if(isValid == false) {
        updateMessageOnError(`The Educations ${requestEducation} does not exist.`);
    }
    return isValid;
}

module.exports.validateLanguages = ( requestLanguage, signupConfigLanguages, updateMessageOnError ) => {
    var isValid = signupConfigLanguages.includes(requestLanguage);
    if(isValid == false) {
        updateMessageOnError(`The Language ${requestLanguage} does not exist.`);
    }
    return isValid;
}

module.exports.validatePets = ( requestPets, signupConfigPets, updateMessageOnError ) => {
    var isValid = includesAll(requestPets, signupConfigPets);
    if(isValid == false) {
        updateMessageOnError(`One of the Pets ${requestPets} does not exist.`);
    }
    return isValid;
}

module.exports.validateRelationshipPurposes = ( requestRelationshipPurpose, signupConfigRelationshipPurposes, updateMessageOnError ) => {
    var isValid = signupConfigRelationshipPurposes.includes(requestRelationshipPurpose);
    if(isValid == false) {
        updateMessageOnError(`The RelationshipPurposes ${requestRelationshipPurpose} does not exist.`);
    }
    return isValid;
}

module.exports.validateReligions = ( requestReligion, signupConfigReligions, updateMessageOnError ) => {
    var isValid = signupConfigReligions.includes(requestReligion);
    if(isValid == false) {
        updateMessageOnError(`The Religion ${requestReligion} does not exist.`);
    }
    return isValid;
}

module.exports.validateDrinking = ( requestDrinking, updateMessageOnError ) => {
    var isValid = isBoolean(requestDrinking);
    if(isValid == false) {
        updateMessageOnError(`The Drinking ${requestDrinking} is not boolean.`);
    }
    return isValid;
}

module.exports.validateSmoking = ( requestSmoking, updateMessageOnError ) => {
    var isValid = isBoolean(requestSmoking);
    if(isValid == false) {
        updateMessageOnError(`The Smoking ${requestSmoking} is not boolean.`);
    }
    return isValid;
}

module.exports.validateReligions = ( requestReligion, signupConfigReligion, updateMessageOnError ) => {
    var isValid = signupConfigReligion.includes(requestReligion);
    if(isValid == false) {
        updateMessageOnError(`The Religion ${requestReligion} does not exist.`);
    }
    return isValid;
}

module.exports.validateTransports = ( requestTransport, signupConfigTransports, updateMessageOnError ) => {
    var isValid = signupConfigTransports.includes(requestTransport);
    if(isValid == false) {
        updateMessageOnError(`The Transports ${requestTransport} does not exist.`);
    }
    return isValid;
}

module.exports.validateStatuses = ( requestStatus, signupConfigStatuses, updateMessageOnError ) => {
    var isValid = signupConfigStatuses.includes(requestStatus);
    if(isValid == false) {
        updateMessageOnError(`The Status ${requestStatus} does not exist.`);
    }
    return isValid;
}

function includesAll(arr1, arr2) {
    let arr1IncludesAll = true;
    if(arr1.length == 0)
        return false;
     
    arr1.forEach(element => {
        if(!arr2.includes(element)) {
            arr1IncludesAll = false;
            return;
        }
    });
    return arr1IncludesAll;
}

function isBoolean(value) {
    return typeof value === "boolean";
      
}

function arrayOfobjectsToArray(arrayOfObjects) {
    var arr = arrayOfObjects.map( obj => obj.name);
    return arr;
}



//validator checks
const email = ( prop, value ) => {
    if(!!validator.isEmail(value)) return;
    throw new Error(`${prop}: not valid email`);
}





