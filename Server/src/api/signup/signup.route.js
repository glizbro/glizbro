const sv = require('./signup.validations');
const router = require('express').Router();
const signupDal = require('./signup.dal');
const { checkSchema, validationResult } = require('express-validator/check');
const mongoClinet = require('../../common/mongoClient');
const ObjectID = require('mongodb').ObjectID;

const { registerUser } = require('./signup.service');

// common
const verifyToken = require('../../common/verifyToken');

//Validations objects
//Req validation template
const signupFailures = ({ location, msg, param, value, nestedErrors }) => {
	return {
		type: 'Error',
		name: 'Signup Failure',
		location: location,
		message: msg,
		param: param,
		value: value,
		nestedErrors: nestedErrors,
	};
};

//Db validations error
let errorsDbValidations = [];

//signup token route
router.get('/confirmation', async (req, res) => {
	try {
      console.log('confirmation api')
		const token = req.query.token;
		const decoded = await verifyToken(token);
		console.log('decoded', decoded);
		// EmailVerification collection in dnaze
      const _id = decoded._id;
		const client = await mongoClinet();
		const result = await client
			.db(process.env.DB_NAME)
			.collection(process.env.USER_COLLECTION)
			.updateOne({ _id: ObjectID(_id) }, { $set: { verified: true } });
		res.redirect(`${process.env.HOST}/login`);
	} catch (err) {
		res.status(400).send(err.message);
	}
});

const validateRegisterRequest = req => {
	let error = null;
	if (!req.body.data.firstName) error = new Error('req.body.data firstName');
	if (!req.body.data.lastName) error = new Error('req.body.data lastName');
	if (!req.body.data.email) error = new Error('req.body.data email');
	if (!req.body.data.password) error = new Error('req.body.data password');
	if (error) throw error;
};
//signup route
router.post('/', async (req, res) => {
	try {
		console.log('SIGNUP ROUTE', req.body.data);
		validateRegisterRequest(req);
      const serviceResult = await registerUser(req.body.data);
		return res.json(serviceResult);
	} catch (err) {
		console.log('sign up error:', err.message);
		res.status(500).json({
			err: err.message,
		});
	}
});

function updateDBMessageOnError(message) {
	errorsDbValidations.push(message);
}

function validateDBData(reqBody, appConfig) {
	let { countries } = appConfig;
	let {
		characters,
		genders,
		educations,
		languages,
		pets,
		relationshipPurposes,
		religions,
		statuses,
		transports,
		interests,
	} = appConfig.signupConfig;
	let {
		country,
		gender,
		education,
		language,
		relationshipPurpose,
		religion,
		alcohol,
		smoking,
		status,
		transport,
	} = reqBody;
	sv.validateCountries(country, countries, updateDBMessageOnError);
	sv.validateCharacters(req.body.character, characters, updateDBMessageOnError);
	sv.validateGenders(gender, genders, updateDBMessageOnError);
	sv.validateEducations(education, educations, updateDBMessageOnError);
	sv.validateLanguages(language, languages, updateDBMessageOnError);
	sv.validatePets(req.body.pets, pets, updateDBMessageOnError);
	sv.validateRelationshipPurposes(
		relationshipPurpose,
		relationshipPurposes,
		updateDBMessageOnError,
	);
	sv.validateReligions(religion, religions, updateDBMessageOnError);
	sv.validateDrinking(alcohol, updateDBMessageOnError);
	sv.validateSmoking(smoking, updateDBMessageOnError);
	sv.validateStatuses(status, statuses, updateDBMessageOnError);
	sv.validateTransports(transport, transports, updateDBMessageOnError);
	sv.validateInterests(req.body.interests, interests, updateDBMessageOnError);
}

function signupSchema() {
	return {
		email: {
			isEmail: true,
			errorMessage: 'Wrong email format.',
			isLength: {
				errorMessage: 'email must be at least 5 and up to 40 characters in length.',
				options: { min: 5, max: 40 },
			},
		},
		password: {
			isString: true,
			errorMessage: 'password not string.',
			matches: {
				options: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
				errorMessage:
					'Password must be: Minimum eight characters, at least one letter, one number and one special character:',
			},
		},
		firstName: {
			isString: true,
			errorMessage: 'firstName not string.',
			isLength: {
				errorMessage:
					'firstName must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
			isAlpha: {
				errorMessage: 'firstName must contain only small or big letters.',
			},
		},
		lastName: {
			isString: true,
			errorMessage: 'lastName not string.',
			isLength: {
				errorMessage:
					'lastName must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
			isAlpha: {
				errorMessage: 'lastName must contain only small or big letters.',
			},
		},
		age: {
			matches: {
				options: /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/,
				errorMessage: 'Wrong age format.',
			},
		},
		country: {
			isString: true,
			errorMessage: 'Country not string.',
			isLength: {
				errorMessage: 'Country must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
		},
		city: {
			isAlpha: true,
			errorMessage: 'City must contain only small or big letters.',
			isLength: {
				errorMessage: 'City must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
		},
		gender: {
			isString: true,
			errorMessage: 'Gender not string.',
			isLength: {
				errorMessage: 'Gender must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
		},
		tall: {
			isString: true,
			errorMessage: 'tall not string.',
			isLength: {
				errorMessage: 'tall must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
		},
		weight: {
			isString: true,
			errorMessage: 'weight not string.',
			isLength: {
				errorMessage: 'weight must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
		},
		status: {
			isString: true,
			errorMessage: 'status not string.',
			isLength: {
				errorMessage: 'status must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
		},
		transport: {
			isString: true,
			errorMessage: 'transport not string.',
			isLength: {
				errorMessage:
					'transport must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
		},
		religion: {
			isString: true,
			errorMessage: 'religion not string.',
			isLength: {
				errorMessage:
					'religion must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
		},
		smoking: {
			isBoolean: true,
			errorMessage: 'smoking not boolean.',
		},
		alcohol: {
			isBoolean: true,
			errorMessage: 'alcohol not boolean.',
		},
		interests: {
			isArray: true,
			errorMessage: 'interests not array.',
		},
		character: {
			isArray: true,
			errorMessage: 'character not array.',
		},
		language: {
			isString: true,
			errorMessage: 'language not string.',
			isLength: {
				errorMessage:
					'language must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
		},
		pets: {
			isArray: true,
			errorMessage: 'pets not array.',
		},
		education: {
			isString: true,
			errorMessage: 'education not string.',
			isLength: {
				errorMessage:
					'education must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
		},
		relationshipPurpose: {
			isString: true,
			errorMessage: 'relationshipPurpose not string.',
			isLength: {
				errorMessage:
					'relationshipPurpose must be at least 2 and up to 30 characters in length.',
				options: { min: 2, max: 30 },
			},
		},
	};
}

module.exports = router;
