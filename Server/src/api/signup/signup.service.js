// contract
const { statuses } = require('../../contract');

// dal
const { findUser, saveUser, sendVerifyAcc } = require('./signup.dal');

// utils
const { hash } = require('../../utils/crypto');

// common
const createToken = require('../../common/createToken');

module.exports.registerUser = async user => {
	try {
		console.log(`SERVICE.registerUser: try register new user  ${JSON.stringify(user, null, 2)}`);

		const userShcema = {
			public: {
				firstName: user.firstName,
				lastName: user.lastName,
				age: '',
				gender: '',
				country: '',
				education: '',
				weight: '',
				tall: '',
				religion: '',
				profession: '',
				city: ''
			},
			images: [],
			profileImg: '',
			private: {
				email: user.email,
				password: user.password,
			},
			relationship: {
				status: statuses.single,
				partner: null,
			},
			verified: false,
			createDate: new Date(),
		};
		const existsUser = await findUser(user.email);
		if (existsUser) throw new Error('user with same email exists');
		// hash password
		const hashedPassword = await hash(userShcema.private.password);
		userShcema.private.password = hashedPassword;

		const newUser = await saveUser(userShcema);
		const token = await createToken({ id: newUser._id }, '10d');
		await sendVerifyAcc(newUser.private.email, token);

		console.log(`SERVICE.registerUser: success register new user`);
		return newUser;
	} catch (err) {
		console.log(`SERVICE.registerUser: faild register new user, ${err.message}`);
		throw err;
	}
};
