// mongo client
const mongoClinet = require('../../common/mongoClient');



// other
const axios = require('axios');

module.exports.findUser = async email => {
	try {
		console.log(`Find User TRY: by email ${email}`);
		const filter = {
			'private.email': email,
		};
		const client = await mongoClinet();
		const user = await client
			.db(process.env.DB_NAME)
			.collection(process.env.USER_COLLECTION)
			.findOne(filter);

		console.log(`Find User SUCCESS:`);
		return user;
	} catch (err) {
		console.log(`Find User ERROR: ${err.message}`);
		throw err;
	}
};
module.exports.saveUser = async user => {
	try {
		console.log(`Save User TRY:`);

		const client = await mongoClinet();
		const result = await client
			.db(process.env.DB_NAME)
			.collection(process.env.USER_COLLECTION)
			.insertOne(user);

		if (!result.ops || !result.ops[0]) throw new Error('unknow mongo insert err');

		console.log(`Save User SUCCESS:`);
		return result.ops[0];
	} catch (err) {
		console.log(`Save User ERROR: ${err.message}`);
		throw err;
	}
};

module.exports.sendVerifyAcc = async (email, token) => {
	try {
		console.log(`sendVerifyAcc: TRY send email`);
		const emailResponse = await axios({
			url: process.env.EMAIL_SERVICE_URL,
			method: 'POST',
			data: {
				receiver: email,
				subject: 'Account Verification Token',
				text: `Hello ${email}, \nPlease verify ${process.env.HOST}${process.env.BASE_API}/signup/confirmation?token=${token}`,
			},
		});
		if (emailResponse.data.err) throw new Error(emailResponse.data.err);
		console.log(`sendVerifyAcc: SUCCESS send email`);
	} catch (err) {
		console.log(`sendVerifyAcc ERROR send email: ${err.message}`);
		throw err;
	}
};
