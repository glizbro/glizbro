'use strict';

const router = require('express').Router();

const nodemailer = require('nodemailer');


// send email
router.post('/', async (req, res) => {
	try {
		const { receiver, subject, text } = req.body;
		// create reusable transporter object using the default SMTP transport
		let transporter = nodemailer.createTransport({
			service: 'gmail',
			auth: {
				user: process.env.EMAIL_USERNAME, // generated ethereal user
				pass: process.env.EMAIL_PASSWORD, // generated ethereal password
			},
		});
		// setup email data with unicode symbols
		let mailOptions = {
			from: process.env.EMAIL_USERNAME, // sender address
			to: receiver, // list of receivers
			subject: subject, // Subject line
			text: text, // plain text body
			// html: "<b>Hello world?</b>" // html body
		};

		let info = await transporter.sendMail(mailOptions);

		res.json({
			err: null,
			data: 'send email success',
		});
	} catch (e) {
		console.log('SendMail Service Err:', e.message);
		res.status(500).json({
			err: e.message,
		});
	}
});

module.exports = router;
