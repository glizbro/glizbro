/**
 * this api should return all users that match for by accepted filter
 * ** return only Users whose gender matches the one you're interested in.
 *
 */

const router = require('express').Router();
const mathcesService = require('./matches.service');

// return matches by filter
router.post('/', async (req, res) => {
	try {
		// logger
		console.log('match api', req.body);
		// TODO: validate request
		const filter = req.body.filter || {};
		const user = req.body.user;
		const data = await mathcesService({ filter, user });
		res.json({
			err: null,
			matches: data,
		});
	} catch (err) {
		console.log('error in match api', err.message);
		res.status(500).json({
			err: err.message,
			matches: null,
		});
	}
});

module.exports = router;
