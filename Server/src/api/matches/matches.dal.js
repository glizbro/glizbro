

const MongoClient = require("../../common/mongoClient");

module.exports = async ({ filter }) => {
	try {
      let client = await MongoClient();
      const db = client.db('dnaze');
      const usersCol = db.collection('Users');
      // console.log('filter in dal', filter)
      const users = await usersCol.find(filter).toArray();
      console.log('users;', users.length);
      return users;
	} catch (err) {
		console.log("matches dal error:", err.message);
		return null;
	}
};
