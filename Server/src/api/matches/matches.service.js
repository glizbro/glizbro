const matchesDal = require('./matches.dal');
var ObjectId = require('mongodb').ObjectID;

module.exports = async ({ filter, user }) => {
	const keys = Object.keys(filter);

	const dbFilter = createFilter(filter);
	// remove current user from matches list
	dbFilter._id = { $ne: ObjectId(user.data._id) };
	console.log('X', dbFilter);
	// TODO: add age filter

	// {tall: {max: val, min: val} => tall:{ $gte: 175,$lte: 180} (gte = >= lte =  <=)
	// interests [val, val] =>  interests:{$all: [val, val]}

	const dbResult = await matchesDal({ filter: dbFilter });
	const result = dbResult.map(user => ({
		...user.public,
		status: user.relationship.status,
		images: user.images,
		profileImg: user.profileImg,
		_id: user._id,
	}));
	if (result) {
		return result;
	} else {
		return null;
	}
};

function addFilter(key, value, type) {
	switch (type) {
		case 'string':
			return value;
		case 'array':
			return { $all: value };
		case 'number':
			return { $gte: value.min, $lte: value.max };
		case 'date':
			return { $gte: value.min, $lte: value.max };
		case 'boolean':
			return value;
		default:
			console.log('BUG: INVALID KEY TYPE', key, type);
			throw new Error(`key - ${key} not match to type - ${type}`);
	}
}

function createFilter(filter) {
	const result = {};
	for (let key in filter) {
	}
	return result;
}
