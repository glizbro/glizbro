'use strict';

const router = require('express').Router();

const mongoClient = require('../../common/mongoClient');

const createToken = require('../../common/createToken');

const { db } = require('../../database');

// utils
const { compare } = require('../../utils/crypto');

router.post('/', validateLoginRequest, async (req, res) => {
	try {
		console.log(`LOGIN_ROUTE: TRY LOGIN`);
		console.log(req.body.password);
		const { email, password } = req.body;
		const user = await db.users.getUserByEmail(email);

		if (!user) throw new Error(`user with ${email} email not exists`);
      const validPassword = await compare(password, user.private.password);
		if (!validPassword) throw new Error(`invalid password`);

		const token = await createToken(user);

		// set cookie
		res.cookie('token', token, {
			httpOnly: true,
		});

		var userWithoutPrivate = Object.assign({}, user); //delete private object for response
		delete userWithoutPrivate.private;

		console.log(`LOGIN_ROUTE: SUCCESS LOGIN`);
		res.json({ ...userWithoutPrivate });
	} catch (err) {
		console.log(`LOGIN_ROUTE: FAIL LOGIN: ${err.message}`);
		res.status(500).json({
			err: err.message,
		});
	}
});

function validateLoginRequest(req, res, next) {
	let error = null;
	if (!req.body.email) error = new Error('req.body.email missing');
	if (!req.body.password) error = new Error('req.body.password missing');
	if (error) return res.sendStatus(400);
	next();
}

module.exports = router;
