const router = require("express").Router();

const { db } = require("../../database");

//get all messages for chat
router.get("/", async (req, res) => {
   try {
      const userIdThatAskedForMessages = req.user._id;

      const userData = await db.users.getUserById(userIdThatAskedForMessages);
      const chatData = await db.chats.getChat(userIdThatAskedForMessages, userData.relationship.partner._id);

      const organizedChatMessages = organizeChatMessages(chatData.messages, userIdThatAskedForMessages);

      res.json(organizedChatMessages);
   } catch (err) {
      console.log("ERR", err.message);
      res.status(500).send(err.message);
   }
});


const organizeChatMessages = (messages, userIdThatAskedForMessages) => {
   const organizedChatMessages = messages.map((chat) => {
      if(chat.to._id == userIdThatAskedForMessages) {
         return {me: false, content: chat.message, time: chat.time}
      }
      else if(chat.from._id == userIdThatAskedForMessages){
         return {me: true, content: chat.message, time: chat.time}
      }
   });
   return organizedChatMessages;
}

module.exports = router;
