const bcrypt = require('bcryptjs');

module.exports.compare = async (value, hash) => {
	try {
		const result = await bcrypt.compare(value, hash);
		return result;
	} catch (err) {
		throw err;
	}
};

module.exports.hash = async value => {
	try {
		return await bcrypt.hash(value, 8);
	} catch (err) {
		throw err;
	}
};
