//time for chat
module.exports = {
	getCurrentTime: getCurrentTime = () => {
		var date = new Date;
		var seconds = date.getSeconds();
		var minutes = date.getMinutes();
		var hour = date.getHours();
		var ampm = hour >= 12 ? 'PM' : 'AM';
		return hour + ':' + minutes + ' ' + ampm;
	},
	getCurrentTimeFull: getCurrentTimeFull = () => {
		let date = new Date()
		var hour = date.getHours();
		var ampm = hour >= 12 ? 'PM' : 'AM';
		let formattedDate = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + ampm
		return formattedDate;
	}
};



