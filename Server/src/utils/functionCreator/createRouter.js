function createServiceParams(variables) {
	return {};
}

function validateReq({ variables, req }) {
	try {
		return false;
	} catch (err) {}
}
function validateConfig(config) {}

async function createRouter({ router, name, description, method, path, service, variables }) {
	return router[method](path, baseRoute({ description, name, service, variables }));
}

function baseRoute({ description, name, service, variables }) {
	return async function(req, res) {
		try {
			console.log(`api call ${name} -> try ${description}`);

			const reqError = validateReq({ variables, req });
			if (reqError) throw new Error(reqError);

			const serviceResult = await service(createServiceParams(variables));

			console.log(`api call ${name} -> success ${description}`);
			res.json(serviceResult);
		} catch (err) {
			console.log(`api call ${name} -> error on ${description} reason: ${err.message}`);
			res.status(500).json({ err: 'server error' });
		}
	};
}

function typeValidate(value, type) {
	return typeof value === type.name.toLowerCase();
}

function reqValidator(keys) {
	for (const key in keys) {
		const fields = keys[key];
		// loop over fields  and validate
		for (const field in fields) {
			const type = fields[field];
			console.log('type', type.name.toLowerCase());
			console.log('field', field);
			console.log('is valid', typeValidate(field, type));
		}
	}
}

module.exports = createRouter;
