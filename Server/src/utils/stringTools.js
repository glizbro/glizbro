module.exports = {
   toCamelCase: (string) => {
      string = string.toLowerCase();
      const firstLetter = string.charAt(0).toUpperCase()
      return firstLetter + string.substring(1);
  }
};
